package wtf.skruffl.roobot.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WebRequestUtils {

    public class RequestTypes {
        public static final String POST = "POST";
        public static final String GET = "GET";
    }

    private WebRequestUtils() {
        // implicit constructor override.
    }

    public static JSONObject performRequest(String requestType, Map<String, String> requestProperties, String url)
            throws IOException {
        try {
            return performRequest(requestType, requestProperties, new URL(url));
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public static JSONObject performRequest(String requestType, Map<String, String> requestProperties, URL url)
            throws IOException {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpRequestBase request = null;
            if (requestType.equalsIgnoreCase("GET")) {
                request = new HttpGet(url.toURI());
            } else if (requestType.equalsIgnoreCase("POST")) {
                request = new HttpPost(url.toURI());
            }

            for (Map.Entry<String, String> entry : requestProperties.entrySet()) {
                request.addHeader(entry.getKey(), entry.getValue());
            }
            String response = readInput(client.execute(request).getEntity().getContent());
            log.debug("Got json result: {}", response);
            return new JSONObject(response);
        } catch (IOException | URISyntaxException e) {
            log.error("Couldn't perform web request", e);
            return null;
        }
    }

    public static String readInput(InputStream content) {
        try (InputStream is = content; ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int buffer = 0;
            while ((buffer = is.read()) != -1) {
                baos.write(buffer);
            }
            String string = new String(baos.toByteArray());
            log.debug("Read input from input stream: {}", string);
            return string;
        } catch (IOException e) {
            log.error("couldn't read content input stream", e);
        }
        return "";
    }

    public static String getPublicIp() {
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            try (BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));) {
                return in.readLine(); // you get the IP as a String
            }
        } catch (IOException e) {
            log.error("Couldn't check public ip.", e);
            return "";
        }
    }
}