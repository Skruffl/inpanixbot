package wtf.skruffl.roobot.util;

import java.util.List;

public class GeneralUtils {
    private GeneralUtils() {
        // Hide implicit
    }

    /**
     * checks if the string is numeric (only holds numbers)
     * @param string
     * @return
     */
    public static boolean isNumeric(String string) {
        try {
            Double.parseDouble(string); // NOSONAR
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * returns the first element of the list or null
     * @param <T> type of the list
     * @param list the list
     * @return the first element of the list or null
     */
    public static <T> T firstOrNull(List<T> list) {
        return list.stream().findFirst().orElse(null);
    }

    /**
     * returns a string of all strings in the array except the first one (0)
     * @param strings the array or collection of string
     * @return the joined string
     */
    public static String shiftJoin(String... strings) {
        StringBuilder builder = new StringBuilder();
        for(int i = 1; i < strings.length; i++) {
            builder.append(strings[i]);
        }

        return builder.toString();
    }
}