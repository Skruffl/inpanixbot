package wtf.skruffl.roobot.util;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.main.Bot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.RestAction;

public class MessageUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageUtils.class);

	private MessageUtils() {
		// Empty private Constructor for Util-Class
	}

	/**
	 * Checks if the message in the event starts with the prefix
	 * 
	 * @param e the event
	 * @return true if the message starts with the currently defined prefix
	 */
	public static Boolean validPrefix(MessageReceivedEvent e, Bot bot) {
		LOGGER.debug("Called MessageUtils::validPrefix");
		if (e.getMessage().getContentRaw().length() > bot.getConfig().getBotPrefix().length()) {
			Boolean rtnValue = e.getMessage().getContentRaw().substring(0, bot.getConfig().getBotPrefix().length())
					.contentEquals(bot.getConfig().getBotPrefix());
			LOGGER.debug("MessageUtils::validPrefix | returning {}", rtnValue);
			return rtnValue;
		}
		LOGGER.debug("Empty message (probably image-only message); returning.");
		return false;
	}

	public static Boolean hasAdminPerms(MessageReceivedEvent event, Bot bot) {
		return hasAdminPerms(event.getMember(), bot);
	}

	public static boolean hasAdminPerms(Member member, Bot bot) {
		ConnectionManager manager = bot.getConnectionManager();
		List<GuildSettings> result = manager.getGuildSetting(Setting.ADMIN_ROLE_ID, member.getGuild().getIdLong());

		// if no admin role is set and user isn't owner
		if (result.isEmpty() && !member.isOwner()) {
			return false;
		}

		// if an admin role is set
		if (!result.isEmpty() && !member.isOwner()) {
			Role adminRole = member.getGuild().getRoleById(result.get(0).getValue());
			if (!member.getRoles().contains(adminRole)) {
				LOGGER.info("unprivileged user {} (name: {}) tried using privileged commands.",
						member.getUser().getId(), member.getUser().getName());
				return false;
			}
		}

		return true;
	}

	/**
	 * splits the message into the args[] array
	 * 
	 * @param e the event of the message in question arriving at the bot
	 * @return the split String[]
	 */
	public static String[] splitMessage(MessageReceivedEvent e, Bot bot) {
		LOGGER.debug("Called MessageUtils::splitMessage");

		// Remove the prefix
		String messageContent = e.getMessage().getContentRaw();
		messageContent = messageContent.substring(bot.getConfig().getBotPrefix().length(), messageContent.length());
		String[] rtnValue = messageContent.split("\\s");

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("MessageUtils::validPrefix | returning {}", String.join(";", rtnValue));
		}

		return rtnValue;
	}

	/**
	 * returns the last message in the channel the event was called in
	 * 
	 * @param event The event of the message to get the channel
	 * @return the previous message
	 */
	public static Message getPreviousMessage(MessageReceivedEvent event) {
		MessageHistory history = event.getChannel().getHistory();
		RestAction<List<Message>> action = history.retrievePast(2);
		Future<List<Message>> fut = action.submit();
		try {
			fut.get();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOGGER.error("Error while executing future call", e);
		}
		Message msg = history.getRetrievedHistory().get(1);
		if (LOGGER.isDebugEnabled() && msg != null) {
			LOGGER.debug("Last message found was ID: {} Content: {}", msg.getIdLong(), msg.getContentRaw());
		}
		return msg;
	}

	public static String getStreamerList(Long guildId, Bot bot) {
		List<TwitchNotifs> table = bot.getManager().getVisibleStreamers(guildId);
		StringBuilder streamerList = new StringBuilder();
		for (TwitchNotifs row : table) {
			streamerList.append("<:Twitch:534038401108934657> https://www.twitch.tv/");
			streamerList.append(row.getChannelName());
			streamerList.append("\n");
		}
		return streamerList.toString();
	}

	public static String getMessageTextFor(Member member, MessageType type, Bot bot) {
		try {
			Language english = bot.getManager().getLanguage("English");
			Player player = Player.build(member, bot);
			Language language;
			// Use players language first
			if (player.getLanguage() != null && !player.getLanguage().isEmpty()) {
				language = bot.getManager().getLanguage(player.getLanguage());
			} else {
				List<GuildSettings> settings = bot.getManager().getGuildSetting(Setting.LANGUAGE,
						member.getGuild().getIdLong());
				// use guilds language
				if (!settings.isEmpty()) {
					language = bot.getManager().getLanguage(settings.get(0).getValue());
				} else {
					// Fall back to english if guild has no language set
					language = english;
				}
			}
			Field field = Language.class.getDeclaredField(type.name());
			field.setAccessible(true);
			String returnValue = (String) field.get(language);

			// Fall back to english
			if (returnValue == null) {
				returnValue = (String) field.get(english);
			}

			returnValue = returnValue.replace("PREFIX", bot.getConfig().getBotPrefix());
			return returnValue;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error("Reflect exception while getting language", e);
			StringBuilder builder = new StringBuilder();
			for (StackTraceElement element : e.getStackTrace()) {
				builder.append(element.toString()).append("\n");
			}
			return "An error occurred while fetching your response. Maybe a stack trace is helpful? ```"
					+ builder.toString() + "```";
		}
	}

	public static MessageEmbed buildSupportEmbed() {
		EmbedBuilder builder = new EmbedBuilder();
		builder.addField("Here you can support us", "[Twitter](https://twitter.com/InPanixClan)", false);
		builder.addField("Our Players", "", false);
		builder.addField("SvNyy",
				"[Instagram](https://www.instagram.com/xzsven/)\n[Twitch](https://www.twitch.tv/svnyy)\n[Twitter](https://twitter.com/InPanixSvNyy)\n[YouTube](https://www.youtube.com/channel/UCQPxhN_QalO_r5-T4j7cJyg)",
				true);
		builder.addField("Wuuudi",
				"[Twitch](https://www.twitch.tv/Wuuudi)\n[Twitter](https://twitter.com/InPanixWuuudi)\n[YouTube](https://www.youtube.com/channel/UCHpVb3Yk34v6XoDagfK2pTA)",
				true);
		builder.addField("AeadR",
				"[Twitch](https://www.twitch.tv/aeadrfn)\n[Twitter](https://twitter.com/InPanixAeadR)\n[YouTube](https://www.youtube.com/channel/UC2XJHrR8HxM1frM938UDimA)",
				true);
		builder.addField("Gardner",
				"[Twitter](https://twitter.com/fat_gardner)\n[YouTube](https://www.youtube.com/user/TheMushroomWg)",
				true);
		builder.addField("Kromb",
				"[Twitch](https://www.twitch.tv/inpanix_kromb)\n[Twitter](https://twitter.com/kr0mb)\n[YouTube](https://www.youtube.com/channel/UCAfRTtgjB4sD9zDFEODOdBg)",
				true);
		builder.addField("Kyota",
				"[Instagram](https://www.instagram.com/altxnn/)\n[Twitch](https://www.twitch.tv/kyotafn)\n[Twitter](https://twitter.com/KyotaFN)\n[YouTube](https://www.youtube.com/channel/UCsuMIsl8pR8O4rBU3typO3A)",
				true);
		builder.addField("Scorpex",
				"[Twitch](https://www.twitch.tv/scorpex_)\n[Twitter](https://twitter.com/InPanixScorpex)\n[YouTube](https://www.youtube.com/channel/UCrGxzMZspUbC-CMt-_MqNBQ)",
				true);
		builder.addField("Scxf",
				"[Twitter](https://twitter.com/InPanixScxf)\n[YouTube](https://www.youtube.com/channel/UCp3Ny3Gxg4SXSQa_bdzXkLg)",
				true);
		builder.addField("xeason", "[Twitch](https://www.twitch.tv/xeason)\n[Twitter](https://twitter.com/Xeasonn)",
				true);

		builder.setColor(new Color(0x565656))
				.addField("Invite your friends!", "<:Discord:534039266901360657> https://discord.gg/q73aGDy", false)
				.addField("**Make sure you read the rules. To get to them, click on the reaction.**", "Step (1/3)",
						false);
		return builder.build();
	}

	public static Language getGuildLanguage(Guild guild, Bot bot) {
		List<GuildSettings> settings = bot.getManager().getGuildSetting(Setting.LANGUAGE, guild.getIdLong());
		if (settings.isEmpty()) {
			return bot.getManager().getLanguage("English");
		}
		return bot.getManager().getLanguage(settings.get(0).getValue());
	}
}
