package wtf.skruffl.roobot.util;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.TwitchOAuthBearer;

public class Streamer {
    private static final Map<String, JSONArray> streamerInfos = new HashMap<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(Streamer.class);
    private String clientId = "Client-ID";
    private String twitchToken;

    private Integer timeout;
    private Long guildId;
    private String channelName;
    private Boolean isLive;
    private String id;
    private JSONObject json;
    private String profileImageUrl;
    private String bannerUrl;
    private String viewCount;
    private String liveDate = "";
    private Bot bot;

    public Streamer(Long guildId, String channelName, Bot bot) {
        this.bot = bot;
        twitchToken = bot.getConfig().getTwitchToken();

        this.timeout = Integer.MAX_VALUE;
        this.guildId = guildId;
        this.channelName = channelName;
        this.isLive = Boolean.FALSE;
        load();
    }

    private Map<String, String> getRequestProps(boolean isKraken) {
        Map<String, String> reqProps = new HashMap<>();
        reqProps.put(clientId, twitchToken);
        String token = TwitchOAuthBearer.getInstance().getToken();
        if (isKraken) {
            reqProps.put("Accept", "application/vnd.twitchtv.v5+json");
            reqProps.put("Authorization", "OAuth " + token);
        } else {
            reqProps.put("Authorization", "Bearer " + token);
        }
        return reqProps;
    }

    public void incrementTimeout() {
        this.timeout++;
    }

    public void setTimeout(Integer value) {
        this.timeout = value;
    }

    public Long getGuildId() {
        return this.guildId;
    }

    public String getChannelName() {
        return this.channelName;
    }

    public Integer getTimeout() {
        return this.timeout;
    }

    public Boolean isLive() {
        return isLive;
    }

    public void setIsLive(Boolean value) {
        this.isLive = value;
    }

    public void setLiveDate(String liveDate) {
        this.liveDate = liveDate;
    }

    public String getLiveDate() {
        return this.liveDate;
    }

    public String getId() {
        return this.id;
    }

    public String getViewCount() {
        return this.viewCount;
    }

    public String getProfileImage() {
        return this.profileImageUrl;
    }

    private void load() {
        try {
            if (!streamerInfos.containsKey(channelName)) {
                JSONArray obj = WebRequestUtils.performRequest("GET", getRequestProps(false),
                        new URL("https://api.twitch.tv/helix/users?login=" + channelName)).getJSONArray("data");
                streamerInfos.put(channelName, obj);
            }
            JSONArray obj = streamerInfos.get(channelName);
            this.json = obj.getJSONObject(0);
            this.id = json.getString("id");
            this.profileImageUrl = json.getString("profile_image_url");
            this.viewCount = ((Integer) json.getInt("view_count")).toString();
        } catch (IOException e) {
            LOGGER.error("Couldn't query streamer id.", e);
        }
    }

    public String getBannerUrl() {
        if (bannerUrl == null) {
            try {
                JSONObject obj = WebRequestUtils.performRequest("GET", getRequestProps(true),
                        new URL("https://api.twitch.tv/kraken/channels/" + id));
                Object o = obj.get("profile_banner");
                if (o == null || (o instanceof String
                        && (o.toString().contentEquals("") || o.toString().contentEquals("null")))) {
                    bannerUrl = "";
                } else {
                    bannerUrl = o.toString();
                }
            } catch (IOException e) {
                LOGGER.error("Couldn't load banner url", e);
            }
        }
        return bannerUrl;
    }

    public String getGame(Long gameId) {
        try {
            JSONArray obj = WebRequestUtils.performRequest("GET", getRequestProps(false),
                    new URL("https://api.twitch.tv/helix/games?id=" + gameId)).getJSONArray("data");
            return obj.isNull(0) ? null : obj.getJSONObject(0).getString("name");
        } catch (IOException e) {
            LOGGER.error("Couldn't query game name.", e);
            return "";
        }
    }

    public String getGame() {
        try {
            JSONObject stream = getStream();
            if (stream == null) {
                return "";
            }
            JSONArray obj = WebRequestUtils
                    .performRequest("GET", getRequestProps(false),
                            new URL("https://api.twitch.tv/helix/games?id=" + stream.getString("game_id")))
                    .getJSONArray("data");
            return obj.isNull(0) ? null : obj.getJSONObject(0).getString("name");
        } catch (IOException e) {
            LOGGER.error("Couldn't query game name.", e);
            return "";
        }
    }

    public String getFollowers() {
        try {
            return ((Integer) WebRequestUtils.performRequest("GET", getRequestProps(false),
                    new URL("https://api.twitch.tv/helix/users/follows?to_id=" + id)).getInt("total")).toString();
        } catch (IOException e) {
            LOGGER.error("Couldn't query follower count.", e);
            return "";
        }
    }

    public JSONObject getStream() {
        try {
            return WebRequestUtils
                    .performRequest("GET", getRequestProps(false),
                            new URL("https://api.twitch.tv/helix/streams?first=1&user_login=" + channelName))
                    .getJSONArray("data").getJSONObject(0);
        } catch (IOException e) {
            LOGGER.error("Error while querying stream info", e);
            return null;
        }
    }

}
