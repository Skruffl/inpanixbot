package wtf.skruffl.roobot.util;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;

/**
 * MemberMessageCache
 */
public class MemberMessageCache {

    /**
     * the member this instance is caching messages for
     */
    @Getter
    private Member member;

    /**
     * the saved messages. we're only saving the contents to save ram.
     */
    @Getter
    private List<Message> lastMessages;

    private final int maxCacheSize = 5;

    public MemberMessageCache(Member member) {
        this.member = member;
        lastMessages = new ArrayList<>(5);
    }

    /**
     * add a message to the cache. will delete the oldest stored message if there
     * are more messages than MemberMessageCache.maxCacheSize (default: 5)
     * 
     * @param message the message to store
     */
    public void addMessage(Message message) {
        lastMessages.add(message);
        if(lastMessages.size() > maxCacheSize) {
            lastMessages.remove(0);
        }
    }

    /**
     * empties the cache
     */
    public void clear() {
        lastMessages = new ArrayList<>();
    }
}