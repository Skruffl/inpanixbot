package wtf.skruffl.roobot.util;

public class DBUtils {
    private DBUtils() {
        // hide implicit constructor
    }

    public static String buildSettingsQuery(String settingsType, Long guildId) {
        /*
         * { "selector": { "group": { "$eq": "GuildSettings" }, "settingsType": { "$eq":    //NOSONAR
         * "${settingsType}" }, "guildId": { "$eq": "${guildId}" } } }                      //NOSONAR
         */
        StringBuilder builder = new StringBuilder();
        builder.append("{\"selector\": {\"group\": {\"$eq\": \"GuildSettings\"},\"type\": {\"$eq\": \"");
        builder.append(settingsType);
        builder.append("\"},\"guildId\": {\"$eq\": \"");
        builder.append(guildId);
        builder.append("\"}}}");
        return builder.toString();
    }
}