package wtf.skruffl.roobot.modules;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.constants.GeneralConstants;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class CommandListener extends ListenerAdapter {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandListener.class);
	private Map<String, MessageHandler> handlers = new HashMap<>();
	private Bot bot;

	public CommandListener(Bot bot) {
		this.bot = bot;
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		Runnable messageCall = () -> messageCall(event);

		new Thread(messageCall).start();
	}

	public void messageCall(MessageReceivedEvent event) {
		String[] args;

		// Ignore bots (including self)
		if (event.getAuthor().isBot()) {
			LOGGER.debug("Bot message, ignoring.");
			return;
		}

		// Check if message starts with the prefix
		if (!MessageUtils.validPrefix(event, bot)) {
			LOGGER.debug("Message didn't start with the defined prefix, ignoring.");
			return;
		}

		args = MessageUtils.splitMessage(event, bot);

		MessageHandler msgHandler = null;
		try {
			// Find MessageHandler to execute by name
			String commandName = (GeneralConstants.CLASSPATH + "admin.")
					.concat(args[0].substring(0, 1).toUpperCase() + args[0].substring(1).toLowerCase());

			msgHandler = handlers.computeIfAbsent(commandName, name -> {
				try {
					Class<?> clazz = Class.forName(commandName);
					return (MessageHandler) clazz.newInstance();
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					LOGGER.error("No MessageHandler found for {}, skipping.", args[0]);
					return null;
				}
			});

			if(msgHandler == null) {
				return;
			}

			LOGGER.info("MessageHandler {} was called!", msgHandler.getClass().getSimpleName());
			msgHandler = (MessageHandler) msgHandler;
			event.getChannel().sendTyping().queue();
			pause();
			msgHandler.handleMessage(event, args, bot);
		} catch (InvalidMessageException e) { // NOSONAR
			if (msgHandler != null) {
				msgHandler.invalidSyntax(event, args, bot);
			}
		} catch (Exception e) {
			LOGGER.error("Catching uncaught error in MessageHandler", e);
		}
	}

	private void pause() {
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}
