package wtf.skruffl.roobot.modules;

import java.util.List;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.db.pojo.Starboard;
import wtf.skruffl.roobot.db.pojo.StarboardThreshold;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;

public class StarboardListener extends ListenerAdapter {
    private Integer starboardCount;
    private ConnectionManager manager;
    private Bot bot;

    public StarboardListener(Bot bot) {
        this.bot = bot;
        this.manager = bot.getConnectionManager();
        this.starboardCount = bot.getConfig().getBotStarCount();
    }

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        List<GuildSettings> result = manager.getGuildSetting(Setting.STAR_BOARD_EMOJI, event.getGuild().getIdLong());
        if (result.isEmpty()) {
            return;
        }

        List<GuildSettings> starBoardChannel = manager.getGuildSetting(Setting.STAR_BOARD,
                event.getGuild().getIdLong());
        if (starBoardChannel.isEmpty()) {
            return;
        }
        TextChannel starBoard = event.getGuild().getTextChannelById(starBoardChannel.get(0).getValue());
        event.getChannel().getHistoryAround(event.getMessageId(), 5).queue(msgHistory -> {
            if (getReactionCount(result.get(0).getValue(),
                    msgHistory.getMessageById(event.getMessageId())) >= starboardCount) {
                updateCount(msgHistory, result.get(0).getValue(), event, starBoard);
            } else {
                sendNew(msgHistory, result.get(0).getValue(), event, starBoard);
            }
        });
    }

    private Integer getReactionCount(String emoji, Message msg) {
        Integer reactionCount = 0;
        for (MessageReaction react : msg.getReactions()) {
            if (react.getReactionEmote().getName().contentEquals(emoji)) {
                reactionCount = react.getCount();
            }
        }
        return reactionCount;
    }

    private void updateCount(MessageHistory msgHistory, String emoji, GuildMessageReactionAddEvent event, // NOSONAR
            TextChannel starBoard) {
        Message msg = msgHistory.getMessageById(event.getMessageId());
        Integer reactionCount = getReactionCount(emoji, msg);
        if (reactionCount >= starboardCount && event.getReactionEmote().getName().contentEquals(emoji)) {
            MessageEmbed embed = getEmbed(msg, reactionCount, emoji);
            List<Starboard> docs = manager.getStarboards(event.getGuild().getIdLong(), msg.getIdLong());
            Player player = Player.build(msg.getMember(), bot);
            player.addStar(reactionCount);
            checkForRoles(player);
            if (docs.isEmpty()) {
                sendNew(msgHistory, emoji, event, starBoard);
            } else {
                Long oldId = docs.get(0).getStarboard();
                starBoard.getHistoryAround(oldId, 5).queue(starboardMessageHistory -> {
                    Message starboardMessage = starboardMessageHistory.getMessageById(oldId);
                    starboardMessage.editMessage(embed).queue();
                });
            }
        }
    }

    private void checkForRoles(Player player) {
        List<StarboardThreshold> thresholds = manager.getThresholds(player.getGuildId());
        Guild guild = bot.getJDA().getGuildById(player.getGuildId());

        for (StarboardThreshold threshold : thresholds) {
            if (player.getStars() >= threshold.getThreshold()) {
                guild.addRoleToMember(guild.getMemberById(player.getMemberId()),
                        guild.getRoleById(threshold.getGroupId())).queue();
            }
        }
    }

    private void sendNew(MessageHistory msgHistory, String emoji, GuildMessageReactionAddEvent event,
            TextChannel starBoard) {
        Message msg = msgHistory.getMessageById(event.getMessageId());
        Integer reactionCount = getReactionCount(emoji, msg);
        if (reactionCount >= starboardCount && event.getReactionEmote().getName().contentEquals(emoji)) {
            MessageEmbed embed = getEmbed(msg, reactionCount, emoji);
            starBoard.sendMessage(embed).queue(message -> {
                Starboard strbd = new Starboard(message.getIdLong(), msg.getIdLong(), starBoard);
                manager.persistStarboard(strbd);
            });
            updatePlayer(msg);
        }
    }

    public MessageEmbed getEmbed(Message msg, Integer reactionCount, String emoji) {
        Language language = MessageUtils.getGuildLanguage(msg.getGuild(), bot);
        EmbedBuilder embed = new EmbedBuilder();
        embed.setAuthor(msg.getAuthor().getName(), null, msg.getAuthor().getAvatarUrl());
        embed.addField(language.getSTARBOARD_MESSAGE_LINK(), "[Click](" + msg.getJumpUrl() + ")", true);
        embed.addField(language.getSTARBOARD_MESSAGE_CHANNEL(), "<#" + msg.getChannel().getId() + ">", true);
        embed.addField(language.getSTARBOARD_MESSAGE_REACTIONS(), emoji + reactionCount, true);
        embed.setTimestamp(msg.getTimeCreated());

        if (msg.getAttachments().size() == 1) {
            embed.setImage(msg.getAttachments().get(0).getUrl());
        }

        if (msg.getAttachments().size() > 1) {
            for (Attachment att : msg.getAttachments()) {
                embed.addField(language.getSTARBOARD_IMAGE_ATTACHMENT(), att.getUrl(), false);
            }
        }

        if (!msg.getContentRaw().contentEquals("")) {
            embed.setDescription(msg.getContentRaw());
        }

        return embed.build();
    }

    public void updatePlayer(Message msg) {
        Player player = Player.build(msg.getMember(), bot);
        player.newStarboardMessage();
    }
}