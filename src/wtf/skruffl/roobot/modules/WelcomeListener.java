package wtf.skruffl.roobot.modules;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.LoginResponse;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.Bots;
import wtf.skruffl.roobot.util.MessageUtils;

public class WelcomeListener extends ListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeListener.class);
    private Bot bot;

    public WelcomeListener(Bot bot) {
        this.bot = bot;
        startDeletionWatcher();
    }

    private void startDeletionWatcher() {
        try {
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(this::deleteChannels, 0, 30, TimeUnit.SECONDS);
        } catch (Exception e) {
            LOGGER.error("Error while creating scheduled task to remove signin channels", e);
        }
    }

    private void deleteChannels() {
        try {
            for (Guild guild : bot.getJDA().getGuilds()) {
                checkGuild(guild);
            }
        } catch (Exception e) {
            LOGGER.error("uncaught exception while deleting channels", e);
        }
    }

    private void checkGuild(Guild guild) {
        OffsetDateTime deletionDate = OffsetDateTime.now().minusHours(2l);
        List<GuildSettings> settings = bot.getManager().getGuildSetting(Setting.JOIN_CATEGORY, guild.getIdLong());
        if (settings.isEmpty()) {
            return;
        }
        Category joinCategory = guild.getCategoryById(settings.get(0).getValue());
        if(joinCategory == null) {
            return;
        }
        for (TextChannel channel : joinCategory.getTextChannels()) {
            if (channel.getTimeCreated().isBefore(deletionDate)) {
                delete(channel);
            }
        }
    }

    private void delete(TextChannel channel) {
        List<LoginResponse> response = bot.getManager().getLoginResponseByChannel(channel.getGuild().getIdLong(),
                channel.getIdLong());
        channel.delete().queue();
        if (!response.isEmpty()) {
            for (LoginResponse rsp : response) {
                bot.getManager().deleteLoginResponse(rsp);
                Member member = channel.getGuild().getMemberById(rsp.getUserId());
                channel.getGuild().kick(member).queue();
            }
        }
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        TextChannel channel = openChannel(event.getMember());
        if (channel == null) {
            return;
        }
        String text = MessageUtils.getMessageTextFor(event.getMember(), MessageType.WELCOME_INITIAL_MESSAGE, bot);
        text = text.replace("GUILD_NAME", event.getGuild().getName());
        text = text.replace("USER_MENTION", event.getMember().getAsMention());
        Message msg = channel
                .sendMessage(new MessageBuilder(new EmbedBuilder().addField(event.getGuild().getName(), text, true))
                        .append(event.getMember().getAsMention()).build())
                .complete();
        bot.getManager().persistLoginResponse(generateReponse(event, msg));
        msg.addReaction("👍").queue();
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        List<LoginResponse> response = bot.getManager().getLoginResponseByPlayer(event.getGuild().getIdLong(),
                event.getAuthor().getIdLong());
        if (!response.isEmpty()) {
            for (LoginResponse resp : response) {
                TextChannel expectedChannel = event.getGuild().getTextChannelById(resp.getChannelId());
                // Check channel
                if (event.getChannel().getIdLong() != expectedChannel.getIdLong()) {
                    return;
                }

                // Check message
                if (event.getMessage().getContentRaw().startsWith(resp.getExpectedResponse())) {
                    giveMemberRole(resp.getGuildId(), event.getAuthor());
                    String msg = MessageUtils.getMessageTextFor(event.getMember(), MessageType.WELCOME_FINAL_MESSAGE,
                            bot);
                    msg = msg.replace("GUILD_NAME", event.getGuild().getName());
                    event.getChannel().sendMessage(msg).queue();
                    bot.getManager().deleteLoginResponse(resp);

                    // Delete the temporary channel in one minute
                    deleteInOneMinute(expectedChannel);
                    return;
                }
            }

            event.getChannel()
                    .sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.WELCOME_ERROR, bot))
                    .queue();
        }
    }

    private void deleteInOneMinute(TextChannel expectedChannel) {
        Bots.getInstance().getScheduler().execute(() -> {
            try {
                Thread.sleep(60000);
                expectedChannel.delete().queue();
            } catch (InterruptedException e) { // NOSONAR
                // Nuffin.
            }
        });
    }

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (!event.getReactionEmote().getName().contentEquals("👍")) {
            return;
        }
        List<LoginResponse> response = bot.getManager().getLoginResponseByPlayer(event.getGuild().getIdLong(),
                event.getMember().getUser().getIdLong());
        if (response.isEmpty()) {
            return;
        }
        for (LoginResponse resp : response) {
            if (!resp.getMessageId().equals(event.getMessageIdLong())) {
                continue;
            }
            event.getChannel().sendMessage(buildEmbed(resp)).queue();
        }

    }

    private TextChannel openChannel(Member member) {
        TextChannel channel = createChannel(member);
        linkChannel(channel, member);
        return channel;
    }

    private TextChannel createChannel(Member member) {
        List<GuildSettings> categories = bot.getManager().getGuildSetting(Setting.JOIN_CATEGORY,
                member.getGuild().getIdLong());
        if (!categories.isEmpty()) {
            Category category = member.getGuild().getCategoryById(categories.get(0).getValue());
            return (TextChannel) category.createTextChannel(member.getEffectiveName() + member.getTimeJoined())
                    .complete();
        }
        return null;
    }

    private void linkChannel(TextChannel channel, Member member) {
        if (channel == null) {
            return;
        }
        channel.createPermissionOverride(member).queue(permOverride -> permOverride.getManager()
                .grant(Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MESSAGE_ADD_REACTION).queue());
    }

    private void giveMemberRole(Long guildId, User user) { // NOSONAR
        List<GuildSettings> response = bot.getManager().getGuildSetting(Setting.JOIN_ROLE_ID, guildId);
        if (!response.isEmpty()) {
            Guild guild = bot.getJDA().getGuildById(guildId);
            List<Role> joinRole = new ArrayList<>(1);
            Role role = guild.getRoleById(response.get(0).getValue());
            if (role == null) {
                return;
            }
            joinRole.add(role);
            guild.addRoleToMember(guild.getMember(user), role).queue();
        }
    }

    private MessageEmbed buildEmbed(LoginResponse resp) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setThumbnail(bot.getJDA().getGuildById(resp.getGuildId()).getIconUrl());
        builder.addField(MessageUtils.getMessageTextFor(
                bot.getJDA().getGuildById(resp.getGuildId()).getMemberById(resp.getUserId()),
                MessageType.WELCOME_SOLVE_CAPTCHA, bot), resp.getCaptcha(), false);
        return builder.build();
    }

    private LoginResponse generateReponse(GuildMemberJoinEvent event, Message msg) {
        String[] operators = { " + ", " - ", " * " };

        Integer random1 = ThreadLocalRandom.current().nextInt(1, 20);
        Integer random2 = ThreadLocalRandom.current().nextInt(1, 20);
        String operator = operators[ThreadLocalRandom.current().nextInt(0, operators.length - 1)];

        // Prevent negative results
        while (operator.trim().contentEquals("-") && (random2 > random1)) {
            random2 = ThreadLocalRandom.current().nextInt(1, 20);
        }

        String result = "";
        switch (operator.trim()) {
        case "+":
            result = ((Integer) (random1 + random2)).toString();
            break;
        case "-":
            result = ((Integer) (random1 - random2)).toString();
            break;
        case "*":
            result = ((Integer) (random1 * random2)).toString();
            break;
        default:
            // what
            break;
        }
        return new LoginResponse(event.getMember(), result, random1.toString() + operator + random2.toString(), msg);
    }

}