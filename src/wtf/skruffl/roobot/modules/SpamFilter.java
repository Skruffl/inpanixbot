package wtf.skruffl.roobot.modules;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MemberMessageCache;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/**
 * SpamFilter
 */
public class SpamFilter extends ListenerAdapter {
    private Bot bot;
    private List<MemberMessageCache> cache = new ArrayList<>();

    private final int equalMessages = 3;

    public SpamFilter(Bot bot) {
        this.bot = bot;
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Member member = event.getMember();
        if (member.getUser().isBot()) {
            return;
        }
        Optional<MemberMessageCache> memberPresent = isMemberPresent(member);
        MemberMessageCache cacheEntry = null;
        if (memberPresent.isPresent()) {
            cacheEntry = memberPresent.get();
            cacheEntry.addMessage(event.getMessage());
        } else {
            MemberMessageCache newCache = new MemberMessageCache(member);
            newCache.addMessage(event.getMessage());
            cache.add(newCache);
            cacheEntry = newCache;
        }

        if (checkMessages(cacheEntry)) {
            Player player = Player.build(member, bot);
            EmbedBuilder builder = new EmbedBuilder().setTimestamp(Instant.now());
            User user = member.getUser();

            builder.setAuthor(member.getEffectiveName(), null, user.getAvatarUrl());
            builder.setDescription("**Member " + member.getAsMention() + " was warned for spam!**");
            builder.addField("Old Warnings", ((Integer) player.getWarns()).toString(), false);
            player.warn();
            builder.addField("New Warnings", ((Integer) player.getWarns()).toString(), false);
            builder.setFooter("Member ID " + user.getIdLong(), null);
            bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
        }
    }

    private boolean checkMessages(MemberMessageCache cacheEntry) {
        List<Message> messages = cacheEntry.getLastMessages();
        for (Message outer : messages) {
            Set<Message> matches = new HashSet<>();
            for (Message inner : messages) {
                if (inner.getContentRaw().isEmpty()) {
                    if (attachmentCheck(outer, inner)) {
                        matches.add(inner);
                    }
                } else {
                    if (attachmentCheck(outer, inner) || contentCheck(outer, inner) || timeCheck(outer, inner)) {
                        matches.add(inner);
                    }
                }

                if (matches.size() >= equalMessages) {
                    matches.forEach(msg -> msg.delete().queue());
                    cache.clear();
                    return true;
                }
            }
        }
        return false;
    }

    private boolean timeCheck(Message outer, Message inner) {
        return outer.getTimeCreated().isEqual(inner.getTimeCreated());
    }

    private boolean contentCheck(Message outer, Message inner) {
        return outer.getContentRaw().equalsIgnoreCase(inner.getContentRaw());
    }

    private boolean attachmentCheck(Message outer, Message inner) {
        if (outer.getAttachments().isEmpty() && inner.getAttachments().isEmpty()) {
            return false;
        }
        if (outer.getAttachments().size() != inner.getAttachments().size()) {
            return false;
        }
        for (int i = 0; i < outer.getAttachments().size(); i++) {
            Attachment outAttach = outer.getAttachments().get(i);
            Attachment inAttach = inner.getAttachments().get(i);

            if (outAttach.getFileName().contentEquals(inAttach.getFileName())
                    && outAttach.getSize() == inAttach.getSize()) {
                return true;
            }
        }
        return outer.getAttachments().equals(inner.getAttachments());
    }

    /**
     * checks if the member already exists
     * 
     * @param member the member to check for
     * @return an optional {@link MemberMessageCache}
     */
    private Optional<MemberMessageCache> isMemberPresent(Member member) {
        return cache.stream().filter(entry -> entry.getMember().getUser().getIdLong() == member.getUser().getIdLong())
                .findFirst();
    }

}