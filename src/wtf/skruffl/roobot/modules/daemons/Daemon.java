package wtf.skruffl.roobot.modules.daemons;

@FunctionalInterface
public interface Daemon {
    public abstract Boolean startDaemon();
}
