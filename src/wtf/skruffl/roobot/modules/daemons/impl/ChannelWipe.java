package wtf.skruffl.roobot.modules.daemons.impl;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.Bots;
import wtf.skruffl.roobot.modules.daemons.Daemon;
import wtf.skruffl.roobot.util.GeneralUtils;

/**
 * ChannelWipe
 */
public class ChannelWipe implements Daemon {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelWipe.class);

    @Override
    public Boolean startDaemon() {
        try {
            Runnable run = () -> {
                for (Bot bot : Bots.getInstance().getBots()) {
                    for (Guild guild : bot.getJDA().getGuilds()) {
                        GuildSettings setting = GeneralUtils.firstOrNull(
                                bot.getConnectionManager().getGuildSetting(Setting.DAILY_WIPE, guild.getIdLong()));
                        if (setting == null) {
                            continue;
                        }
                        TextChannel channel = guild.getTextChannelById(setting.getValue());
                        channel.getIterableHistory().stream().forEach(msg -> msg.delete().queue());
                    }
                }
            };
            Bots.getInstance().getScheduler().scheduleAtFixedRate(run, 0, 1, TimeUnit.DAYS);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error while setting up channel wiper daemon", e);
            return false;
        }
    }

}