package wtf.skruffl.roobot.modules.daemons.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.GuildStats;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.Bots;
import wtf.skruffl.roobot.modules.daemons.Daemon;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.VoiceChannel;

/**
 * PlayerCountDaemon
 */
public class PlayerCountDaemon implements Daemon {

    private Bot bot;
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerCountDaemon.class);

    public PlayerCountDaemon(Bot bot) {
        this.bot = bot;
    }

    @Override
    public Boolean startDaemon() {
        try {
            Bots.getInstance().getScheduler().scheduleAtFixedRate(this::run, 0l, 1l, TimeUnit.MINUTES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * represents 1 full run
     */
    private void run() {
        for (Guild guild : bot.getJDA().getGuilds()) {
            List<GuildSettings> result = bot.getManager().getGuildSetting(Setting.PLAYER_COUNT_ID, guild.getIdLong());
            if (!result.isEmpty()) {
                VoiceChannel vc = guild.getVoiceChannelById(result.get(0).getValue());
                doCheck(vc, guild);
            }
        }
    }

    /**
     * runs a check for a single guild.
     * 
     * @param vc    the voice channel to edit
     * @param guild the guild
     */
    private void doCheck(VoiceChannel vc, Guild guild) {
        try {
            LOGGER.info("Checking member counts.");
            Language lang = MessageUtils.getGuildLanguage(vc.getGuild(), bot);
            String text = lang.getMEMBER_COUNT().replace("MEMBER_COUNT", Integer.toString(guild.getMembers().size()));
            vc.getManager().setName(text).queue();
            GuildStats stats = GuildStats.build(guild, bot);
            stats.poll();
            bot.getManager().persistGuildStats(stats);
            LOGGER.info("Updating player count for guild {}. New player Count: {}", guild.getIdLong(),
                    guild.getMembers().size());
        } catch (Exception e) {
            LOGGER.error("Error while checking member counts", e);
        }
    }

}