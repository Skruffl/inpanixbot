package wtf.skruffl.roobot.modules.daemons.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;

import fi.iki.elonen.NanoHTTPD;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.exceptions.DaemonException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.Bots;
import wtf.skruffl.roobot.main.TwitchOAuthBearer;
import wtf.skruffl.roobot.modules.daemons.Daemon;
import wtf.skruffl.roobot.util.MessageUtils;
import wtf.skruffl.roobot.util.Streamer;
import wtf.skruffl.roobot.util.WebRequestUtils;

@Slf4j
public class TwitchDaemon implements Daemon {
    private static final String APPKEY = Bots.getInstance().getTwitchToken();
    private static final String CLIENT_ID = "Client-ID";
    private static final String AUTH = "Authorization";

    private static TwitchDaemon instance = null;

    protected List<Streamer> streamers = new ArrayList<>();

    private TwitchDaemon() {
        startDaemon();
    }

    /**
     * goes through the list of all streamers and finds all streamers with the user
     * id, so that we can notify all guilds that have this streamer connected
     * 
     * @param userId the user id of the streamer
     * @return a list of all streamers with this user id
     */
    protected List<Streamer> getStreamers(Integer userId) {
        List<Streamer> streamerList = new ArrayList<>();
        for (Streamer streamer : streamers) {
            if (Integer.parseInt(streamer.getId()) == userId) {
                streamerList.add(streamer);
            }
        }
        return streamerList;
    }

    public static TwitchDaemon getInstance() {
        if (instance == null) {
            instance = new TwitchDaemon();
        }
        return instance;
    }

    @Override
    public Boolean startDaemon() {
        try {
            log.info("Running twitch daemon!");
            new WebhookListener();
            runDaemon();
            return true;
        } catch (DaemonException | IOException e) {
            log.error("Error while creating daemon thread", e);
            return false;
        }
    }

    /**
     * refreshes the leases for the twitch hook every 30 minutes.
     */
    protected void runDaemon() throws DaemonException {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                doTwitchRequests(false);
            }
        });

        Bots.getInstance().getScheduler().scheduleAtFixedRate(() -> doTwitchRequests(true), 0, 30, TimeUnit.MINUTES);
    }

    private void doTwitchRequests(Boolean subscribe) {
        log.info("Resubscribing to all streamers!");
        Set<String> subscribedStreamers = new HashSet<>();
        for (Bot bot : Bots.getInstance().getBots()) {
            for (TwitchNotifs row : bot.getConnectionManager().getAllStreamers()) {
                log.info("{}subscribing streamer: {}", subscribe ? "" : "un", row.getChannelName()); // NOSONAR
                // Creates request to subscribe to streamer
                // We only want to do this if we're not already subscribed to the streamer
                if (!subscribedStreamers.contains(row.getChannelName())) {
                    subscribedStreamers.add(row.getChannelName());
                    subscribeToStreamer(row, subscribe, bot);
                    try { // NOSONAR
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    }

    private boolean containsStreamer(String channelName) {
        for (Streamer streamer : streamers) {
            if (streamer.getChannelName().equalsIgnoreCase(channelName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * subscribes the bot to a streamer
     * 
     * @param row       the database row for this streamer
     * @param subscribe should we subscribe or unsubscribe?
     */
    public void subscribeToStreamer(TwitchNotifs row, Boolean subscribe, Bot bot) {
        try {
            Streamer streamerPojo;
            try {
                streamerPojo = new Streamer(row.getGuildId(), row.getChannelName(), bot);
            } catch (Exception e) {
                log.error("Streamer {} doesn't exist! Deleting data.", row.getChannelName(), e);
                bot.action(bot.getJDA().getGuildById(row.getGuildId()),
                        "Streamer {} doesn't exist! Please update them.", row.getChannelName());
                bot.getConnectionManager().deleteTwitchNotifs(row);
                return;
            }
            streamerPojo.setIsLive(false);
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("https://api.twitch.tv/helix/webhooks/hub");
            String publicUrl = "http://" + WebRequestUtils.getPublicIp() + ":6969";
            JSONObject obj = new JSONObject();
            obj.put("hub.mode", subscribe ? "subscribe" : "unsubscribe");
            obj.put("hub.topic", "https://api.twitch.tv/helix/streams?user_id=" + streamerPojo.getId());
            obj.put("hub.callback", publicUrl);
            obj.put("hub.lease_seconds", 1800);
            String jsonString = obj.toString();
            HttpEntity stringEntity = new StringEntity(jsonString, ContentType.APPLICATION_JSON);
            httpPost.addHeader(CLIENT_ID, APPKEY);
            httpPost.addHeader(AUTH, "Bearer " + TwitchOAuthBearer.getInstance().getToken());
            httpPost.setEntity(stringEntity);
            httpclient.execute(httpPost);
            if (subscribe && !containsStreamer(streamerPojo.getChannelName())) {
                streamers.add(streamerPojo);
            }

            log.debug("Successfully {} streamer {}", subscribe ? "added" : "removed", streamerPojo.getChannelName());
        } catch (IOException e) {
            log.error("Error while creating twitch hook.", e);
        }
    }

    public void subscribeToStreamer(String channelName, Long guildId, Bot bot) {
        subscribeToStreamer(new TwitchNotifs(channelName, guildId, false), true, bot);
    }

    private class WebhookListener extends NanoHTTPD {

        public WebhookListener() throws IOException {
            super(6969);
            start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
            log.info("Webhook Listener online!");
        }

        private void newStreamGoesLive(Streamer streamerObj) {
            log.info("Sending new stream message for streamer {} on guild {}", streamerObj.getChannelName(),
                    streamerObj.getGuildId());
            for (Bot bot : Bots.getInstance().getBots()) {
                for (TwitchNotifs notifs : bot.getConnectionManager().getAllStreamers()) {
                    if (notifs.getChannelName().contentEquals(streamerObj.getChannelName())) {
                        sendStreamer(streamerObj, bot, notifs);
                    }
                }
            }
        }

        private void sendStreamer(Streamer streamerObj, Bot bot, TwitchNotifs notifs) {
            try {
                Role announcementRole = null;
                List<GuildSettings> groupResults = bot.getManager().getGuildSetting(Setting.STREAM_GROUP_ID,
                        streamerObj.getGuildId());
                Guild guildById = bot.getJDA().getGuildById(streamerObj.getGuildId());
                if (guildById == null) {
                    // checks if the guild exists for the given bot
                    // this would null pointer if a streamer goes live but the streamer entry
                    // belongs to a guild the bot is not on
                    return;
                }
                if (!groupResults.isEmpty()) {
                    String objStreamGroup = groupResults.get(0).getValue();
                    Long streamGroupId = Long.parseLong(objStreamGroup);
                    announcementRole = guildById.getRoleById(streamGroupId);
                }

                List<GuildSettings> channelResults = bot.getManager()
                        .getGuildSetting(Setting.STREAM_ANNOUNCEMENT_CHANNEL, streamerObj.getGuildId());
                if (channelResults.isEmpty()) {
                    log.info("There's no notification channel set for this guild; returning.");
                    return;
                }
                Long announcementChannelId = Long.parseLong(channelResults.get(0).getValue().toString());

                TextChannel announcementChannel = guildById.getTextChannelById(announcementChannelId);

                String link = "https://twitch.tv/" + streamerObj.getChannelName();
                String pictureUrl = streamerObj.getBannerUrl();
                JSONObject stream = streamerObj.getStream();
                // never use the banner as the thumbnail image, always use something from the
                // stream c:

                // if (pictureUrl == null || pictureUrl.contentEquals("null") ||
                // pictureUrl.isEmpty()) {

                pictureUrl = stream.getString("thumbnail_url").replace("{width}", "1280").replace("{height}", "720");
                // }
                String game = streamerObj.getGame();
                if (game == null) {
                    game = "Unbekannt";
                }

                Language language = MessageUtils.getGuildLanguage(announcementChannel.getGuild(), bot);

                String live = language.getTWITCH_LIVE();
                if (notifs.isPingable()) {
                    live = live.replace("ANNOUNCEMENT_ROLE",
                            announcementRole == null ? "@here" : announcementRole.getAsMention());
                } else {
                    live = live.replace("ANNOUNCEMENT_ROLE", "");
                }
                live = live.replace("LINK", link);
                live = live.replace("STREAMER_NAME",
                        streamerObj.getChannelName().replace("_", "\\_").replace("*", "\\*"));

                Message msg = new MessageBuilder().append(live)
                        .setEmbed(new EmbedBuilder().setTitle(link, link).setColor(0x565656)
                                .setThumbnail(streamerObj.getProfileImage()).setImage(pictureUrl)
                                .setAuthor(streamerObj.getChannelName(), link, streamerObj.getProfileImage())
                                .addField(language.getTWITCH_PLAYING(), game, false)
                                .addField(language.getTWITCH_STREAM_TITLE(),
                                        stream.isNull("title") ? "---" : stream.getString("title"), false)
                                .addField(language.getTWITCH_FOLLOWERS(), streamerObj.getFollowers(), true)
                                .addField(language.getTWITCH_TOTAL_VIEWS(), streamerObj.getViewCount(), true).build())
                        .build();

                announcementChannel.sendMessage(msg).queue();
            } catch (Exception e) {
                log.error("Error while querying data about the streamer", e);
            }
        }

        /**
         * this is called by an outside source requesting something from the app. this
         * is where twitch will send the GET request to notify us about a streamer going
         * live.
         */
        @Override
        public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> parameters, // NOSONAR
                Map<String, String> files) {
            log.debug("uri {} was called.", uri);
            String challenge = "";
            String streamerName = uri.replace("http://", "").replace(WebRequestUtils.getPublicIp() + "/", "");
            log.info("detected streamer name from uri: {}", streamerName);
            // live checker
            if (files.containsKey("postData")) {
                log.debug("contains postData");
                JSONObject streamAnnouncement = new JSONObject(files.get("postData"));
                JSONArray data = streamAnnouncement.getJSONArray("data");
                log.debug("json array data length is {}", data.length());
                log.debug("json array: {}", data.toString());
                if (!data.isEmpty()) {
                    log.debug("json array isn't empty");
                    JSONObject stream = data.getJSONObject(0);
                    for (Streamer streamer : getStreamers(stream.getInt("user_id"))) {
                        // ignore the update if streamer was already live (if there's just a stream
                        // update)
                        String postLiveDate = stream.getString("started_at");
                        log.info("post live date == {}", postLiveDate);
                        log.info("streamer live date == {}", streamer.getLiveDate());
                        if (!streamer.getLiveDate().contentEquals(postLiveDate)) { // NOSONAR
                            log.info("Sending stream notification for this streamer.");
                            streamer.setLiveDate(postLiveDate);
                            newStreamGoesLive(streamer);
                        } else {
                            log.info("NOT sending stream notification for this streamer.");
                        }
                    }
                }
            }

            // check if challenge is present
            if (method.equals(Method.GET) && parameters.containsKey("hub.challenge")) {
                log.info("challenge present");
                challenge = parameters.get("hub.challenge");
            }

            // send response
            if (!challenge.contentEquals("")) {
                log.info("sending challenge response: {}", challenge);
                return newFixedLengthResponse(challenge);
            } else {
                log.info("sending regular response.");
                return newFixedLengthResponse(
                        "follow <a href=\"http://twitter.com/skruffl\">@skruffl</a> on twitter owo");
            }
        }
    }
}