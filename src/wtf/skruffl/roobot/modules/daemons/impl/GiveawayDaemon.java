package wtf.skruffl.roobot.modules.daemons.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.Giveaway;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.modules.daemons.Daemon;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GiveawayDaemon extends ListenerAdapter implements Daemon {

    private static final Logger LOGGER = LoggerFactory.getLogger(GiveawayDaemon.class);
    private List<Giveaway> runningGiveaways = new ArrayList<>();

    private Bot bot;

    public GiveawayDaemon(Bot bot) {
        this.bot = bot;
    }

    @Override
    public Boolean startDaemon() {
        try {
            loadGiveaways();
            runDaemon();
            return true;
        } catch (Exception e) {
            LOGGER.error("Coudln't start giveaway daemon", e);
            return false;
        }
    }

    private void loadGiveaways() {
        runningGiveaways = bot.getManager().getAllGiveaways();
    }

    public void addGiveaway(Giveaway giveaway) {
        runningGiveaways.add(giveaway);
    }

    private void runDaemon() {
        try {
            Runnable run = this::checkForGiveaways;
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(run, 0, 30, TimeUnit.SECONDS);
        } catch (Exception e) {
            LOGGER.error("Exception while finalizing giveaways", e);
        }
    }

    private void checkForGiveaways() {
        LOGGER.info("Checking for finished giveaways.");
        loadGiveaways();
        if (runningGiveaways.isEmpty()) {
            LOGGER.info("No giveaways currently present.");
        } else {
            LOGGER.info("There are {} giveaways running.", runningGiveaways.size());
        }
        for (int i = 0; i < runningGiveaways.size(); i++) {
            Giveaway ga = runningGiveaways.get(i);
            if (System.currentTimeMillis() >= ga.getEndTs()) {
                try {
                    finalizeGiveaway(ga);
                    bot.getManager().deleteGiveaway(ga);
                    runningGiveaways.remove(i);
                } catch (Exception e) {
                    LOGGER.error("error while finalizing giveaway", e);
                }
            }
        }
    }

    private void finalizeGiveaway(Giveaway ga) {
        LOGGER.info("Finalizing giveaway {} for guild {} on message id {}", ga.getId(), ga.getGuild(), ga.getMessage());
        Guild guild = bot.getJDA().getGuildById(ga.getGuild());
        TextChannel giveawayChannel = guild.getTextChannelById(ga.getChannel());

        // Find a random user and send the message
        MessageHistory hist = giveawayChannel.getHistoryAround(ga.getMessage(), 5).complete();
        Message giveawayMsg = hist.getMessageById(ga.getMessage());
        Long xpReward = ga.getXpReward();
        User randomUser = getRandomUser(giveawayMsg, ga.getEmoji(), xpReward);
        if (randomUser == null) {
            LOGGER.warn("Couldn't finalize giveaway, no users reacted.");
        }
        sendEndMessage((TextChannel) giveawayMsg.getChannel(), randomUser, xpReward);
    }

    private User getRandomUser(Message msg, String emoji, Long xpReward) { // NOSONAR
        if (msg == null) {
            throw new NullPointerException("Message may not be null! (Did the message get deleted?)");
        }
        for (MessageReaction reaction : msg.getReactions()) {
            if (reaction.getReactionEmote().getName().contentEquals(emoji)) {
                List<User> users = reaction.retrieveUsers().complete();
                List<User> nonBotUsers = new ArrayList<>();
                for (User user : users) {
                    if (!user.isBot()) { // NOSONAR
                        nonBotUsers.add(user);
                    }
                }
                int random = ThreadLocalRandom.current().nextInt(0, nonBotUsers.size());
                return nonBotUsers.get(random);
            }
        }
        return null;
    }

    private void sendEndMessage(TextChannel channel, User user, Long xpReward) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setAuthor(user.getName(), null, user.getAvatarUrl());
        if (xpReward > 0) {
            String desc = MessageUtils.getMessageTextFor(channel.getGuild().getMember(user),
                    MessageType.GIVEAWAY_EMBED_WIN_XP, bot);
            desc = desc.replace("USER_MENTION", user.getAsMention());
            desc = desc.replace("XP_REWARD", xpReward.toString());
            builder.setDescription(desc);
            persistXp(user, xpReward, channel.getGuild());
        } else {
            String desc = MessageUtils.getMessageTextFor(channel.getGuild().getMember(user),
                    MessageType.GIVEAWAY_EMBED_WIN_NO_XP, bot);
            desc = desc.replace("USER_MENTION", user.getAsMention());
            builder.setDescription(desc);
        }
        String message = MessageUtils.getMessageTextFor(channel.getGuild().getMember(user),
                MessageType.GIVEAWAY_CONGRATULATION, bot);
        message = message.replace("USER_MENTION", user.getAsMention());
        MessageBuilder msg = new MessageBuilder(message).setEmbed(builder.build());
        channel.sendMessage(msg.build()).queue();
    }

    private void persistXp(User user, Long xpReward, Guild guild) {
        Member member = guild.getMember(user);
        Player player = Player.build(member, bot);
        player.addXp(xpReward);
    }

}