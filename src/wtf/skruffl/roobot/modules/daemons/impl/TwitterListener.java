package wtf.skruffl.roobot.modules.daemons.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import twitter4j.FilterQuery;
import twitter4j.MediaEntity;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.db.pojo.TwitterNotifs;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.Bots;
import wtf.skruffl.roobot.modules.daemons.Daemon;
import wtf.skruffl.roobot.util.MessageUtils;

public class TwitterListener implements Daemon {
    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterListener.class);
    private static TwitterListener instance;
    private Twitter twitter = new TwitterFactory(Bots.getInstance().getTwitterConfig()).getInstance();
    private TwitterStream stream;
    private List<Long> twitterIds = new ArrayList<>();

    private TwitterListener() {
        refresh();
    }

    public static TwitterListener getInstance() {
        if (instance == null) {
            instance = new TwitterListener();
        }
        return instance;
    }

    public void refresh() {
        LOGGER.info("Refreshing twitter list");
        twitterIds = new ArrayList<>();
        FilterQuery filter = new FilterQuery();
        for (Bot bot : Bots.getInstance().getBots()) {
            for (TwitterNotifs notifs : bot.getConnectionManager().getAllTwitterNotifs()) {
                long userId = getUserIdForNotifs(notifs, bot);
                if (!twitterIds.contains(userId)) {
                    twitterIds.add(userId);
                }
            }
        }
        long[] longs = new long[twitterIds.size()];
        for(int i = 0; i<longs.length;i++) {
            longs[i] = twitterIds.get(i);
        }
        filter.follow(longs);
        stream = new TwitterStreamFactory().getInstance(twitter.getAuthorization());
        addListener();
        stream.filter(filter);
    }

    private long getUserIdForNotifs(TwitterNotifs notifs, Bot bot) {
        // Fetches the id from twitter and stores it to the database
        notifs.setTwitterId(Long.toString(getUserId(notifs.getTwitterName())));
        bot.getConnectionManager().persistTwitterNotifs(notifs);
        return Long.parseLong(notifs.getTwitterId());
    }

    private void addListener() {
        stream.addListener(new StatusListener() {

            @Override
            public void onException(Exception arg0) {
                LOGGER.error("general exception", arg0);
            }

            @Override
            public void onTrackLimitationNotice(int arg0) {
                LOGGER.info(Integer.toString(arg0));
            }

            @Override
            public void onStatus(Status arg0) {
                if (!arg0.isRetweet() && twitterIds.contains(arg0.getUser().getId())) {
                    LOGGER.info("Twitter user {} posted a new tweet: \n{}", arg0.getUser().getName(), arg0.getText());
                    newTweet(arg0);
                }
            }

            @Override
            public void onStallWarning(StallWarning arg0) {
                LOGGER.info(arg0.toString());
            }

            @Override
            public void onScrubGeo(long arg0, long arg1) {
                LOGGER.info(Long.toString(arg0));
                LOGGER.info(Long.toString(arg1));
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice arg0) {
                LOGGER.info(arg0.toString());
            }
        });
    }

    private void newTweet(Status tweet) {
        for (Bot bot : Bots.getInstance().getBots()) {
            for (Guild guild : bot.getJDA().getGuilds()) {
                TextChannel tweetChannel = getTweetChannel(bot, guild);
                sendMessageForTweet(tweet, guild, bot, tweetChannel);
            }
        }
    }

    private void sendMessageForTweet(Status tweet, Guild guild, Bot bot, TextChannel tweetChannel) {
        Language guildLanguage = MessageUtils.getGuildLanguage(guild, bot);
        String msg = guildLanguage.getTWITTER_NEW_TWEET().replace("USER", tweet.getUser().getName());
        String permalink = buildLink(tweet);
        EmbedBuilder embedBuilder = new EmbedBuilder().setThumbnail(tweet.getUser().get400x400ProfileImageURL())
                .setTimestamp(tweet.getCreatedAt().toInstant()).addField(msg, tweet.getText(), false)
                .addField("Permalink", permalink, false);
        MessageAction fileMessage = tweetChannel.sendMessage(new MessageBuilder(EmbedBuilder.ZERO_WIDTH_SPACE).build());
        if (tweet.getMediaEntities().length == 1) {
            embedBuilder.setImage(tweet.getMediaEntities()[0].getMediaURLHttps());
        } else {
            for (MediaEntity entity : tweet.getMediaEntities()) {
                if (entity.getType().contentEquals("video")) {
                    embedBuilder.setFooter(guildLanguage.getTWITTER_VIDEO());
                }
                try {
                    fileMessage.addFile(new URI(entity.getMediaURLHttps()).toURL().openStream(), getName(entity));
                } catch (IOException | URISyntaxException e) {
                    LOGGER.error("Error while reading twitter files", e);
                }
            }
        }
        tweetChannel.sendMessage(embedBuilder.build()).complete();
        fileMessage.queue();
    }

    private String getName(MediaEntity entity) {
        switch (entity.getType()) {
        case "photo":
            return entity.getId() + ".jpg";
        case "animated_gif":
            return entity.getId() + ".gif";
        default:
            throw new IllegalArgumentException();
        }
    }

    private String buildLink(Status tweet) {
        StringBuilder builder = new StringBuilder();
        builder.append("https://twitter.com/").append(tweet.getUser().getScreenName()).append("/status/")
                .append(tweet.getId());
        return builder.toString();
    }

    private TextChannel getTweetChannel(Bot bot, Guild guild) {
        String id = bot.getConnectionManager().getGuildSetting(Setting.TWEET_CHANNEL, guild.getIdLong()).get(0)
                .getValue();
        if (id != null) {
            return guild.getTextChannelById(id);
        }
        return null;
    }

    private long getUserId(String user) {
        try {
            return twitter.searchUsers(user, 0).get(0).getId();
        } catch (TwitterException e) {
            LOGGER.error("Error while querying user id", e);
            return 0l;
        }
    }

    @Override
    public Boolean startDaemon() {
        try {
            addListener();
        } catch (Exception e) {
            LOGGER.error("Error while starting daemon", e);
            return false;
        }
        return true;
    }

}