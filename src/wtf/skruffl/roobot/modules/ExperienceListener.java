package wtf.skruffl.roobot.modules;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;

public class ExperienceListener extends ListenerAdapter {

    private Bot bot;

    public ExperienceListener(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot() || MessageUtils.validPrefix(event, bot)) {
            return;
        }
        Player player = Player.build(event.getMember(), bot);
        Integer oldLevel = player.getLevel();
        player.newMessage();
        Integer newLevel = player.getLevel();
        if (newLevel > oldLevel) {
            if (newLevel % 10 == 0) {
                Role newLevelRole = getLevelRole(newLevel, event.getGuild());
                event.getGuild().addRoleToMember(event.getMember(), newLevelRole);
            }
            sendLevelupNotification(player, event);
        }
    }

    public void sendLevelupNotification(Player player, MessageReceivedEvent event) {
        String text = MessageUtils.getMessageTextFor(event.getMember(), MessageType.EXP_LEVELUP, bot);
        text = text.replace("LEVEL", player.getLevel().toString());
        Message msg = new MessageBuilder().setEmbed(new EmbedBuilder().setDescription(text)
                .setAuthor(event.getAuthor().getName()).setThumbnail(event.getAuthor().getAvatarUrl()).build()).build();
        event.getChannel().sendMessage(msg).queue(message -> new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                message.delete().queue();
            }
        }, 3000));
    }

    private Role getLevelRole(Integer level, Guild guild) {
        List<Role> roles = guild.getRolesByName("Level " + level, true);
        if (roles.isEmpty()) {
            Role role = guild.createRole().complete();
            role.getManager().setName("Level " + level).queue();
            return role;
        } else {
            return roles.get(0);
        }
    }
}