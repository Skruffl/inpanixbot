package wtf.skruffl.roobot.modules;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.main.Bot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class InPanixWelcomeListener extends ListenerAdapter {

	private Bot bot;

	public InPanixWelcomeListener(Bot bot) {
		this.bot = bot;
	}

	@Override
	public void onGuildMemberJoin(GuildMemberJoinEvent event) {
		TextChannel welcomeChannel;
		Role joinRole;

		Object returnValue = bot.getManager().getGuildSetting(Setting.JOIN_ROLE_ID, event.getGuild().getIdLong()).get(0)
				.getValue();
		if (returnValue != null) {
			Long joinRoleId = Long.parseLong(returnValue.toString());
			joinRole = event.getGuild().getRoleById(joinRoleId);
			if (joinRole != null) {
				List<Role> roles = new ArrayList<>();
				roles.add(joinRole);
				event.getGuild().modifyMemberRoles(event.getMember(), roles, Collections.emptyList()).queue();
			}
		}

		Long welcomeChannelId = Long.parseLong(bot.getManager()
				.getGuildSetting(Setting.WELCOME_CHANNEL_ID, event.getGuild().getIdLong()).get(0).getValue());

		welcomeChannel = event.getGuild().getTextChannelById(welcomeChannelId);

		StringBuilder description = new StringBuilder().append("Welcome ").append(event.getMember().getAsMention())
				.append(" on the Team InPanix | Community Discord!\nHave a great time!");

		MessageBuilder msgBuilder = new MessageBuilder().setEmbed(new EmbedBuilder()
				.setDescription(description.toString()).setColor(new Color(0x565656))
				.setThumbnail(
						"https://cdn.discordapp.com/attachments/548959726357053453/568105845653766165/InPanix_standard_Logo.png")
				.setAuthor(event.getMember().getUser().getName(), null, event.getMember().getUser().getAvatarUrl())
				.build())
				.setContent(new StringBuilder().append("Welcome ").append(event.getMember().getAsMention())
						.append(" to InPanix").toString());
		if (welcomeChannel == null) {
			welcomeChannel = event.getGuild().getDefaultChannel();
		}
		welcomeChannel.sendMessage(msgBuilder.build()).queue();
	}
}
