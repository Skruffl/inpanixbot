package wtf.skruffl.roobot.modules;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.main.Bots;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.guild.react.GenericGuildMessageReactionEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class SequenceListener extends ListenerAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(SequenceListener.class);
	private Bot bot;

	public SequenceListener(Bot bot) {
		this.bot = bot;
	}

	private void doReaction(GenericGuildMessageReactionEvent event, Boolean removed) {
		if (event.getUser().isBot()) {
			return;
		}
		ConnectionManager manager = bot.getConnectionManager();
		List<LoginSequence> login = manager.getSequence(event.getGuild().getIdLong(), event.getMessageIdLong(),
				removed);
		if (login.isEmpty()) {
			LOGGER.debug("No login sequence was found for this message. Returning.");
			return;
		}

		// Create Lists for the roles to remove/add
		List<Role> rolesToAdd = new ArrayList<>();
		List<Role> rolesToRemove = new ArrayList<>();

		// Get a guild controller for the current guild

		for (LoginSequence row : login) {
			LOGGER.debug("Entering LoginSequence ResultSet");
			if (((Long) event.getMessageIdLong()).equals(row.getMessageId())
					&& event.getReactionEmote().getName().contentEquals((String) row.getUnicodeEmoji())) {

				// Add current role
				if (row.getRoleId() != null) { // NOSONAR
					rolesToAdd.add(event.getGuild().getRoleById(row.getRoleId()));
				}
				if (row.getRemovalRoleId() != null) { // NOSONAR
					rolesToRemove.add(event.getGuild().getRoleById(row.getRemovalRoleId()));
				}
			}
		}
		// Perform action
		event.getGuild().modifyMemberRoles(event.getMember(), rolesToAdd, rolesToRemove).queue();
	}

	@Override
	public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
		Runnable reactionAdded = () -> doReaction(event, false); // NOSONAR
		Bots.getInstance().getScheduler().execute(reactionAdded);
	}

	@Override
	public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event) {
		Runnable reactionRemoved = () -> doReaction(event, true); // NOSONAR
		Bots.getInstance().getScheduler().execute(reactionRemoved);
	}
}
