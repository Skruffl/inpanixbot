package wtf.skruffl.roobot.modules;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;

public class JoinListener extends ListenerAdapter {

    private Bot bot;

    public JoinListener(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        ConnectionManager manager = bot.getConnectionManager();
        List<GuildSettings> settings = manager.getGuildSetting(Setting.JOIN_CHANNEL,
                event.getMember().getGuild().getIdLong());
        if (settings.isEmpty()) {
            return;
        }
        long channelId = Long.parseLong(settings.get(0).getValue());
        TextChannel channel = event.getMember().getGuild().getTextChannelById(channelId);

        settings = manager.getGuildSetting(Setting.JOIN_ROLE_ID, event.getMember().getGuild().getIdLong());
        if (!settings.isEmpty()) {
            Role role = event.getMember().getGuild().getRoleById(settings.get(0).getValue());
            event.getGuild().modifyMemberRoles(event.getMember(), Arrays.asList(role), Collections.emptyList()).queue();
        }

        Language language = MessageUtils.getGuildLanguage(event.getGuild(), bot);
        String message = language.getWELCOME_INITIAL_MESSAGE();
        message = message.replace("GUILD_NAME", event.getGuild().getName());
        message = message.replace("USER_MENTION", event.getMember().getAsMention());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setDescription(message);
        builder.setAuthor(event.getMember().getUser().getName(), null, event.getMember().getUser().getAvatarUrl());
        builder.setThumbnail(event.getGuild().getIconUrl());
        MessageBuilder msgBuilder = new MessageBuilder();
        msgBuilder.setContent(event.getMember().getAsMention());
        msgBuilder.setEmbed(builder.build());

        channel.sendMessage(msgBuilder.build()).queue();
    }
}