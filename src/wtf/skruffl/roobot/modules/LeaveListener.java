package wtf.skruffl.roobot.modules;

import java.util.List;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class LeaveListener extends ListenerAdapter {

    private Bot bot;

    public LeaveListener(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        List<GuildSettings> settings = bot.getConnectionManager().getGuildSetting(Setting.DISCONNECT_CHANNEL,
                event.getGuild().getIdLong());
        if (settings.isEmpty()) {
            return;
        }
        TextChannel leaveNotificationChannel = event.getGuild().getTextChannelById(settings.get(0).getValue());
        Language language = MessageUtils.getGuildLanguage(event.getGuild(), bot);
        String msg = language.getLEAVE_NOTIFICATION();
        msg.replace("USER_NAME", event.getUser().getAsTag());
        leaveNotificationChannel.sendMessage(msg).queue();
    }
}