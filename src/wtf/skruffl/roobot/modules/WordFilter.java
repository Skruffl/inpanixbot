package wtf.skruffl.roobot.modules;

import java.time.Instant;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.Word;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;

/**
 * WordFilter
 */
public class WordFilter extends ListenerAdapter {

    private Bot bot;

    public WordFilter(Bot bot) {
        this.bot = bot;
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Member member = event.getMember();
        if (member.getUser().getIdLong() == bot.getJDA().getSelfUser().getIdLong()) {
            return;
        }
        for (Word word : bot.getConnectionManager().getWordsForGuild(event.getGuild().getIdLong())) {
            if (event.getMessage().getContentRaw().toLowerCase().contains(word.getWord().toLowerCase())) {
                event.getMessage().delete().queue();

                String message = MessageUtils.getMessageTextFor(member, MessageType.WORD_FILTER, bot);
                message = message.replace("USER_MENTION", member.getAsMention());
                event.getChannel().sendMessage(message).queue(msg -> {
                    // Delete message after 3 seconds
                    try {
                        Thread.sleep(3000);
                        msg.delete().queue();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });

                EmbedBuilder builder = new EmbedBuilder().setTimestamp(Instant.now());
                User user = member.getUser();

                builder.setAuthor(member.getEffectiveName(), null, user.getAvatarUrl());
                builder.setDescription("**Member " + member.getAsMention() + " used a blacklisted word!**");
                builder.addField("Message Content", event.getMessage().getContentRaw(), false);
                builder.setFooter("Member ID " + user.getIdLong(), null);
                bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
            }
        }
    }
}