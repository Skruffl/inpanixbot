package wtf.skruffl.roobot.modules;

import java.io.FileInputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.WebhookClientBuilder;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.Webhook;
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;

public class GhostPings extends ListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(GhostPings.class);
    private Bot bot;

    public GhostPings(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        Message msg = bot.getMessagesCache().get(event.getMessageIdLong());
        if (msg == null) {
            MessageHistory hist = event.getChannel().getHistoryAround(event.getMessageId(), 20).complete();
            msg = hist.getMessageById(event.getMessageId());
            if (msg.getMentionedMembers().isEmpty() || MessageUtils.hasAdminPerms(msg.getMember(), bot)) {
                msg = null;
            }
        }
        if (msg == null) {
            LOGGER.debug("Couldn't find message, content, can't recreate it.");
            return;
        }
        Member member = msg.getMember();
        try {
            Icon icon = Icon.from(new URL(member.getUser().getAvatarUrl()).openStream());
            Webhook webhook = event.getChannel().createWebhook(msg.getMember().getNickname())
                    .setName(msg.getMember().getNickname()).setAvatar(icon).complete();
            WebhookClient client = new WebhookClientBuilder(webhook.getIdLong(), webhook.getToken()).build();
            WebhookMessageBuilder builder = new WebhookMessageBuilder().setContent(msg.getContentRaw());
            for (Attachment attachment : msg.getAttachments()) {
                builder.addFile(attachment.getFileName(), new FileInputStream(attachment.downloadToFile().join()));
            }
            client.send(builder.build()).join();
            webhook.delete().queue();
        } catch (Exception e) {
            LOGGER.error("Exception while sending a webhook", e);
        }
    }

}