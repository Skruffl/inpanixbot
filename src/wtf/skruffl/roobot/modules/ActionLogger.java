package wtf.skruffl.roobot.modules;

import java.time.Instant;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.channel.category.CategoryCreateEvent;
import net.dv8tion.jda.api.events.channel.category.CategoryDeleteEvent;
import net.dv8tion.jda.api.events.channel.category.update.CategoryUpdateNameEvent;
import net.dv8tion.jda.api.events.channel.category.update.CategoryUpdatePermissionsEvent;
import net.dv8tion.jda.api.events.channel.category.update.CategoryUpdatePositionEvent;
import net.dv8tion.jda.api.events.channel.text.TextChannelCreateEvent;
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdateNSFWEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdateNameEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdatePermissionsEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdatePositionEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdateSlowmodeEvent;
import net.dv8tion.jda.api.events.channel.text.update.TextChannelUpdateTopicEvent;
import net.dv8tion.jda.api.events.channel.voice.VoiceChannelCreateEvent;
import net.dv8tion.jda.api.events.channel.voice.VoiceChannelDeleteEvent;
import net.dv8tion.jda.api.events.channel.voice.update.VoiceChannelUpdateBitrateEvent;
import net.dv8tion.jda.api.events.channel.voice.update.VoiceChannelUpdateNameEvent;
import net.dv8tion.jda.api.events.channel.voice.update.VoiceChannelUpdatePermissionsEvent;
import net.dv8tion.jda.api.events.channel.voice.update.VoiceChannelUpdatePositionEvent;
import net.dv8tion.jda.api.events.channel.voice.update.VoiceChannelUpdateUserLimitEvent;
import net.dv8tion.jda.api.events.guild.GuildBanEvent;
import net.dv8tion.jda.api.events.guild.GuildUnbanEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateAfkChannelEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateAfkTimeoutEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateIconEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateNameEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateOwnerEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateRegionEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateSplashEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateSystemChannelEvent;
import net.dv8tion.jda.api.events.guild.update.GuildUpdateVerificationLevelEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveAllEvent;
import net.dv8tion.jda.api.events.role.RoleCreateEvent;
import net.dv8tion.jda.api.events.role.RoleDeleteEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdateColorEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdateMentionableEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdateNameEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdatePermissionsEvent;
import net.dv8tion.jda.api.events.role.update.RoleUpdatePositionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.main.Bot;

/**
 * ActionLogger
 */
public class ActionLogger extends ListenerAdapter {

    private Bot bot;

    public ActionLogger(Bot bot) {
        this.bot = bot;
    }

    private EmbedBuilder getBuilder() {
        return new EmbedBuilder().setTimestamp(Instant.now());
    }

    @Override
    public void onGuildMessageUpdate(@Nonnull GuildMessageUpdateEvent event) {
        if (event.getAuthor().isBot()) {
            return;
        }
        EmbedBuilder builder = getBuilder();
        Member member = event.getMember();
        Message message = event.getMessage();
        builder.setAuthor(member.getEffectiveName(), null, member.getUser().getAvatarUrl());
        builder.setDescription("**Message edited in **" + event.getChannel().getAsMention() + "\n[Jump to Message]("
                + message.getJumpUrl() + ")");
        builder.setFooter("Message ID " + event.getMessageId(), null);
        Message msg = bot.getMessagesCache().get(event.getMessageIdLong());
        if (msg != null) {
            builder.addField("Before", msg.getContentDisplay(), false);
            builder.addField("After", message.getContentDisplay(), false);
        }
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildMessageDelete(@Nonnull GuildMessageDeleteEvent event) {
        Message message = bot.getMessagesCache().get(event.getMessageIdLong());
        if (message != null && message.getAuthor().isBot()) {
            return;
        }
        EmbedBuilder builder = getBuilder();
        builder.setDescription("**Message deleted in **" + event.getChannel().getAsMention());
        builder.setFooter("Message ID " + event.getMessageId(), null);
        if (message != null) {
            builder.setAuthor(message.getMember().getEffectiveName(), null, message.getAuthor().getAvatarUrl());
            builder.addField("Content", message.getContentRaw(), false);
        } else {
            builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        }
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildMessageReactionRemoveAll(@Nonnull GuildMessageReactionRemoveAllEvent event) {
        Message message = bot.getMessagesCache().get(event.getMessageIdLong());
        if (message != null && message.getAuthor().isBot()) {
            return;
        }
        EmbedBuilder builder = getBuilder();
        builder.setFooter("Message ID " + event.getMessageId(), null);
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setDescription("**A moderator has cleared the reactions on message [#" + event.getMessageId()
                + "](https://discordapp.com/channels/" + event.getGuild().getId() + "/" + event.getChannel().getId()
                + "/" + event.getMessageId() + ")**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelDelete(@Nonnull TextChannelDeleteEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was deleted!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelUpdateName(@Nonnull TextChannelUpdateNameEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was edited!**");
        builder.addField("Old Name", event.getOldValue(), true);
        builder.addField("New Name", event.getNewValue(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelUpdateTopic(@Nonnull TextChannelUpdateTopicEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was edited!**");
        builder.addField("Old Topic", event.getOldValue(), true);
        builder.addField("New Topic", event.getNewValue(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelUpdatePosition(@Nonnull TextChannelUpdatePositionEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was edited!**");
        builder.addField("Old Position", event.getOldValue().toString(), true);
        builder.addField("New Position", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelUpdatePermissions(@Nonnull TextChannelUpdatePermissionsEvent event) {
        // it was 1:30 am when i wrote this i have no clue how to visualize permissions
        // in an embed
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was edited!**");
        builder.addField("Changed Roles",
                event.getChangedRoles().stream().map(Role::getName).collect(Collectors.joining(", ")), false);
        builder.addField("Changed Members",
                event.getChangedMembers().stream().map(Member::getEffectiveName).collect(Collectors.joining(", ")),
                true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelUpdateNSFW(@Nonnull TextChannelUpdateNSFWEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was edited!**");
        builder.addField("Old NSFW Status", event.getOldValue().toString(), true);
        builder.addField("New NSFW Status", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelUpdateSlowmode(@Nonnull TextChannelUpdateSlowmodeEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was edited!**");
        builder.addField("Old SlowMode Status", event.getOldValue().toString(), true);
        builder.addField("New SlowMode Status", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onTextChannelCreate(@Nonnull TextChannelCreateEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The text channel " + event.getChannel().getAsMention() + " was created!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelDelete(@Nonnull VoiceChannelDeleteEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was edited!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelUpdateName(@Nonnull VoiceChannelUpdateNameEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was edited!**");
        builder.addField("Old Name", event.getOldValue().toString(), true);
        builder.addField("New Name", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelUpdatePosition(@Nonnull VoiceChannelUpdatePositionEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was edited!**");
        builder.addField("Old Position", event.getOldValue().toString(), true);
        builder.addField("New Position", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelUpdateUserLimit(@Nonnull VoiceChannelUpdateUserLimitEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was edited!**");
        builder.addField("Old User Limit", event.getOldValue().toString(), true);
        builder.addField("New User Limit", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelUpdateBitrate(@Nonnull VoiceChannelUpdateBitrateEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was edited!**");
        builder.addField("Old Bitrate", event.getOldValue().toString(), true);
        builder.addField("New Bitrate", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelUpdatePermissions(@Nonnull VoiceChannelUpdatePermissionsEvent event) {
        // it was 1:30 am when i wrote this i have no clue how to visualize permissions
        // in an embed
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was edited!**");
        builder.addField("Changed Roles",
                event.getChangedRoles().stream().map(Role::getName).collect(Collectors.joining(", ")), false);
        builder.addField("Changed Members",
                event.getChangedMembers().stream().map(Member::getEffectiveName).collect(Collectors.joining(", ")),
                true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onVoiceChannelCreate(@Nonnull VoiceChannelCreateEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getChannel().getId(), null);
        builder.setDescription("**The voice channel " + event.getChannel().getName() + " was created!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onCategoryDelete(@Nonnull CategoryDeleteEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Category ID " + event.getCategory().getId(), null);
        builder.setDescription("**The category " + event.getCategory().getName() + " was deleted!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onCategoryUpdateName(@Nonnull CategoryUpdateNameEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getCategory().getId(), null);
        builder.setDescription("**The category " + event.getCategory().getName() + " was edited!**");
        builder.addField("Old Name", event.getOldValue().toString(), true);
        builder.addField("New Name", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onCategoryUpdatePosition(@Nonnull CategoryUpdatePositionEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getCategory().getId(), null);
        builder.setDescription("**The category " + event.getCategory().getName() + " was edited!**");
        builder.addField("Old Position", event.getOldValue().toString(), true);
        builder.addField("New Position", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onCategoryUpdatePermissions(@Nonnull CategoryUpdatePermissionsEvent event) {
        // it was 1:30 am when i wrote this i have no clue how to visualize permissions
        // in an embed
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Channel ID " + event.getCategory().getId(), null);
        builder.setDescription("**The category **" + event.getCategory().getName() + " was edited!");
        builder.addField("Changed Roles",
                event.getChangedRoles().stream().map(Role::getName).collect(Collectors.joining(", ")), false);
        builder.addField("Changed Members",
                event.getChangedMembers().stream().map(Member::getEffectiveName).collect(Collectors.joining(", ")),
                true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onCategoryCreate(@Nonnull CategoryCreateEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Category ID " + event.getCategory().getId(), null);
        builder.setDescription("**The category " + event.getCategory().getName() + " was created!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildBan(@Nonnull GuildBanEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getUser().getAsTag(), null, event.getUser().getAvatarUrl());
        builder.setFooter("User ID " + event.getUser().getId(), null);
        builder.setDescription("**The user " + event.getUser().getAsTag() + " was banned!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUnban(@Nonnull GuildUnbanEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getUser().getAsTag(), null, event.getUser().getAvatarUrl());
        builder.setFooter("User ID " + event.getUser().getId(), null);
        builder.setDescription("**The user " + event.getUser().getAsTag() + " was unbanned**!");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateOwner(@Nonnull GuildUpdateOwnerEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The owner has changed! O_O **");
        builder.addField("Old Owner", event.getOldValue().getAsMention(), true);
        builder.addField("New Owner", event.getNewValue().getAsMention(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateRegion(@Nonnull GuildUpdateRegionEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server region was updated!**");
        builder.addField("Old Region", event.getOldValue().toString(), true);
        builder.addField("New Region", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateSplash(@Nonnull GuildUpdateSplashEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server splash was updated!**");
        builder.addField("Old Splash", event.getOldValue().toString(), true);
        builder.addField("New Splash", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateVerificationLevel(@Nonnull GuildUpdateVerificationLevelEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server verification level was updated!**");
        builder.addField("Old Verification Level", event.getOldValue().toString(), true);
        builder.addField("New Verification Level", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateName(@Nonnull GuildUpdateNameEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server name was updated!**");
        builder.addField("Old Name", event.getOldValue().toString(), true);
        builder.addField("New Name", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateIcon(@Nonnull GuildUpdateIconEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server icon was updated!**");
        builder.addField("Old icon", event.getOldValue().toString(), true);
        builder.addField("New icon", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateAfkChannel(@Nonnull GuildUpdateAfkChannelEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server afk channel was updated!**");
        builder.addField("Old afk channel", event.getOldValue().toString(), true);
        builder.addField("New afk channel", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateSystemChannel(@Nonnull GuildUpdateSystemChannelEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server system channel was updated!**");
        builder.addField("Old system channel", event.getOldValue().toString(), true);
        builder.addField("New system channel", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildUpdateAfkTimeout(@Nonnull GuildUpdateAfkTimeoutEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The server afk timeout was updated!**");
        builder.addField("Old afk timeout", event.getOldValue().toString(), true);
        builder.addField("New afk timeout", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getMember().getEffectiveName(), null, event.getUser().getAvatarUrl());
        builder.setFooter("User ID " + event.getUser().getId(), null);
        builder.setDescription("**New Member " + event.getMember().getAsMention() + " joined!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onGuildMemberLeave(@Nonnull GuildMemberLeaveEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getMember().getEffectiveName(), null, event.getUser().getAvatarUrl());
        builder.setFooter("User ID " + event.getUser().getId(), null);
        builder.setDescription("**Member " + event.getUser().getAsTag() + " left!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());

    }

    @Override
    public void onGuildMemberRoleAdd(@Nonnull GuildMemberRoleAddEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getMember().getEffectiveName(), null, event.getUser().getAvatarUrl());
        builder.setFooter("User ID " + event.getUser().getId(), null);
        builder.setDescription("**" + event.getMember().getAsMention() + " was given the Roles`"
                + event.getRoles().stream().map(Role::getName).collect(Collectors.joining(", ")) + "`!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());

    }

    @Override
    public void onGuildMemberRoleRemove(@Nonnull GuildMemberRoleRemoveEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getMember().getEffectiveName(), null, event.getUser().getAvatarUrl());
        builder.setFooter("User ID " + event.getUser().getId(), null);
        builder.setDescription("**" + event.getMember().getAsMention() + " was removed from the roles`"
                + event.getRoles().stream().map(Role::getName).collect(Collectors.joining(", ")) + "`!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());

    }

    @Override
    public void onGuildMemberUpdateNickname(@Nonnull GuildMemberUpdateNicknameEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Guild ID " + event.getGuild().getId(), null);
        builder.setDescription("**The user " + event.getMember().getAsMention() + " changed their nickname!**");
        builder.addField("Old nickname", event.getOldValue(), true);
        builder.addField("New nickname", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleCreate(@Nonnull RoleCreateEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role " + event.getRole().getName() + " was created!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleDelete(@Nonnull RoleDeleteEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role " + event.getRole().getName() + " was deleted!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleUpdateColor(@Nonnull RoleUpdateColorEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role " + event.getRole().getName() + " was edited!**");
        builder.addField("Old Color", event.getOldValue().toString(), true);
        builder.addField("New Color", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleUpdateMentionable(@Nonnull RoleUpdateMentionableEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role " + event.getRole().getName() + " was edited!**");
        builder.addField("Old Mentionable Status", event.getOldValue().toString(), true);
        builder.addField("New Mentionable Status", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleUpdateName(@Nonnull RoleUpdateNameEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role " + event.getRole().getName() + " was edited!**");
        builder.addField("Old Name", event.getOldValue().toString(), true);
        builder.addField("New Name", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleUpdatePermissions(@Nonnull RoleUpdatePermissionsEvent event) {
        // it was 1:30 am when i wrote this i have no clue how to visualize permissions
        // in an embed
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role permissions for " + event.getRole().getName() + " were updated!**");
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

    @Override
    public void onRoleUpdatePosition(@Nonnull RoleUpdatePositionEvent event) {
        EmbedBuilder builder = getBuilder();
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setFooter("Role ID " + event.getRole().getId(), null);
        builder.setDescription("**The role " + event.getRole().getName() + " was edited!**");
        builder.addField("Old Position", event.getOldValue().toString(), true);
        builder.addField("New Position", event.getNewValue().toString(), true);
        bot.action(event.getGuild(), new MessageBuilder(builder.build()).build());
    }

}