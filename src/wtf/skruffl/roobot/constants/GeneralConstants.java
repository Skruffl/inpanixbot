package wtf.skruffl.roobot.constants;

public class GeneralConstants {
	
	public static final String CLASSPATH = "wtf.skruffl.roobot.messages.";
	public static final String PROPERTIES_PATH = "config.properties";
	public static final String PLATFORMS = "Twitter, Twitch";
	
	private GeneralConstants() {
		//Empty Constructor
	}
	
}
