package wtf.skruffl.roobot.constants;

import lombok.Getter;

/**
 * Language
 */
public enum Language {

    Ping("Pong!");

    @Getter
    private String def;

    private Language(String def) {
        this.def = def;
    }

}