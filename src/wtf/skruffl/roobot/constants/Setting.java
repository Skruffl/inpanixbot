package wtf.skruffl.roobot.constants;

public enum Setting {

    STAR_BOARD("starBoard", Type.TEXT_CHANNEL),

    LANGUAGE("language", Type.STRING),

    ADMIN_ROLE_ID("adminRoleId", Type.ROLE),

    GAMBLING_CHANNEL("gamblingChannel", Type.TEXT_CHANNEL),

    GAMBLING_MIN_AMOUNT("gamblingMinAmount", Type.NUMBER),

    JOIN_CATEGORY("joinCategory", Type.CATEGORY),

    JOIN_CHANNEL("joinChannel", Type.TEXT_CHANNEL),

    STAR_BOARD_EMOJI("starBoardEmoji", Type.STRING),

    PLAYER_COUNT_ID("PlayerCountId", Type.VOICE_CHANNEL),

    JOIN_ROLE_ID("joinRoleId", Type.ROLE),

    DYNAMIC_TWITCH_EMBED("dynamicTwitchEmbed", Type.MESSAGE),

    INFO_CHANNEL_ID("infoChannelId", Type.TEXT_CHANNEL),

    WELCOME_CHANNEL_ID("welcomeChannelId", Type.TEXT_CHANNEL),

    STREAM_GROUP_ID("streamGroupId", Type.ROLE),

    STREAM_ANNOUNCEMENT_CHANNEL("streamAnnouncementChannel", Type.TEXT_CHANNEL),

    ADMIN_GIVEAWAY_CHANNEL("adminGiveawayChannel", Type.TEXT_CHANNEL),

    USER_GIVEAWAY_CHANNEL("userGiveawayChannel", Type.TEXT_CHANNEL),

    TWEET_CHANNEL("twitterChannel", Type.TEXT_CHANNEL),

    DISCONNECT_CHANNEL("disconnectChannel", Type.TEXT_CHANNEL),

    SUPPORT_CATEGORY("supportCategory", Type.CATEGORY),

    SUPPORT_ROLE("supportRole", Type.ROLE),
    
    ACTION_CHANNEL("actionChannel", Type.TEXT_CHANNEL),

    LOG_CHANNEL("logChannel", Type.TEXT_CHANNEL),

    DAILY_WIPE("dailyWipe", Type.TEXT_CHANNEL)

    ;

    private final String databaseName;
    private Type type;

    Setting(final String databaseName, final Type type) {
        this.databaseName = databaseName;
        this.type = type;
    }

    @Override
    public String toString() {
        return databaseName;
    }

    public Type getType() {
        return this.type;
    }

    public enum Type {
        TEXT_CHANNEL,
        VOICE_CHANNEL,
        CATEGORY,
        MESSAGE,
        ROLE,
        STRING,
        NUMBER;
    }
}