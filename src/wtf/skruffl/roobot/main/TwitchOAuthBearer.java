package wtf.skruffl.roobot.main;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.gson.Gson;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import wtf.skruffl.roobot.db.pojo.twitch.AccessTokenReply;
import wtf.skruffl.roobot.util.WebRequestUtils;

@Slf4j
public class TwitchOAuthBearer {

    @Getter
    private static final TwitchOAuthBearer instance = new TwitchOAuthBearer();

    private String token;
    private long expriation;

    private long creation;
    private String twitchToken;
    private String twitchSecret;

    private TwitchOAuthBearer() {
        twitchToken = Bots.getInstance().getTwitchToken();
        twitchSecret = Bots.getInstance().getTwitchSecret();
        request();
    }

    private void request() {
        creation = System.currentTimeMillis();
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost post = new HttpPost(String.format(
                    "https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=client_credentials",
                    twitchToken, twitchSecret));
            CloseableHttpResponse response = client.execute(post);
            AccessTokenReply atr = new Gson().fromJson(WebRequestUtils.readInput(response.getEntity().getContent()),
                    AccessTokenReply.class);
            this.token = atr.getAccess_token();
            this.expriation = atr.getExpires_in() * 1000; // twitch returns in seconds but we're calculating in
                                                          // milliseconds
            log.debug("Received token {} and expiration {}", token, expriation);
        } catch (IOException e) {
            log.error("could not register oauth bearer", e);
        }
    }

    private boolean isValid() {
        long expiration = (this.creation + this.expriation) - 10000L; // add 10 second grace
        return expiration > System.currentTimeMillis();
    }

    public String getToken() {
        if (!isValid()) {
            request();
        }
        return token;
    }

}