package wtf.skruffl.roobot.main;

public class Main {
	private Main() {

	}

	public static void main(String[] args) {
		Bots.getInstance().loadBotsFromConfig();
	}
}
