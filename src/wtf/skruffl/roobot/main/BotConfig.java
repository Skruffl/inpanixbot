package wtf.skruffl.roobot.main;

import lombok.Getter;

@Getter
public class BotConfig {
        private String botName;
        private String botToken;
        private String botPrefix;
        private boolean botLocalmode;
        private String botGameName;
        private String botModules;
        private Integer botXpDelay;
        private Integer botStarCount;
        private int botMessagesCache;

        private String dbMode;
        private String dbAddress;
        private Integer dbPort;
        private String dbUsername;
        private String dbPassword;
        private String dbCatalog;
        private boolean dbSsl;
        private boolean dbLoud;

        private String twitchToken;
        private String twitchSecret;

        private String twitterOAuthKey;
        private String twitterOAuthSec;
        private String twitterToken;
        private String twitterSecret;
}