package wtf.skruffl.roobot.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class Bots {

    private static Bots instance;
    private Logger LOGGER = LoggerFactory.getLogger(Bots.class);
    private Map<String, Bot> bots = new HashMap<>();
    @Getter
    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(5);

    private Bots() {
    }

    public static Bots getInstance() {
        if (instance == null) {
            instance = new Bots();
        }
        return instance;
    }

    public void loadBotsFromConfig() {
        String configFile = "";
        try {
            configFile = Files.readAllLines(Paths.get("config.json")).stream().map(String::valueOf)
                    .collect(Collectors.joining());
        } catch (IOException e) {
            LOGGER.error("Couldn't read config file!", e);
            try {
                new File("config.json").createNewFile();
            } catch (IOException ex) {

            }
            System.exit(1);
        }
        Gson gson = new Gson();
        BotConfig[] bots = gson.fromJson(configFile, BotConfig[].class);
        Arrays.stream(bots).forEach(botConfig -> {
            Bot bot = new Bot(botConfig, scheduler);
            this.bots.put(botConfig.getBotName(), bot);
            bot.loadModules();
        });
        LOGGER.info("Successfully started all bots!");
    }

    public Bot getBot(String name) {
        return bots.get(name);
    }

    public List<String> getBotNames() {
        return bots.keySet().stream().map(obj -> Objects.toString(obj, "")).collect(Collectors.toList());
    }

    public List<Bot> getBots() {
        return bots.values().stream().collect(Collectors.toList());
    }

    public String getTwitchToken() {
        String token = null;
        for (Bot bot : getBots()) {
            String twitchToken = bot.getConfig().getTwitchToken();
            if (twitchToken != null && !twitchToken.isEmpty()) {
                token = twitchToken;
                break;
            }
        }
        return token;
    }

    public String getTwitchSecret() {
        return getBots().stream().map(b -> b.getConfig().getTwitchSecret()).filter(s -> s != null)
                .filter(s -> !s.isEmpty()).distinct().findFirst().orElse(null);
    }

    public Configuration getTwitterConfig() {
        for (Bot bot : getBots()) {
            String twitchToken = bot.getConfig().getTwitterOAuthKey();
            if (twitchToken != null && !twitchToken.isEmpty()) {
                ConfigurationBuilder confBuilder = new ConfigurationBuilder();
                confBuilder.setOAuthConsumerKey(bot.getConfig().getTwitterOAuthKey())
                        .setOAuthConsumerSecret(bot.getConfig().getTwitterOAuthSec())
                        .setOAuthAccessToken(bot.getConfig().getTwitterToken())
                        .setOAuthAccessTokenSecret(bot.getConfig().getTwitterSecret());
                return confBuilder.build();
            }
        }
        return null;
    }

}