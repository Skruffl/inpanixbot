package wtf.skruffl.roobot.main;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.CouchDbConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.modules.ActionLogger;
import wtf.skruffl.roobot.modules.CommandListener;
import wtf.skruffl.roobot.modules.ExperienceListener;
import wtf.skruffl.roobot.modules.GhostPings;
import wtf.skruffl.roobot.modules.InPanixWelcomeListener;
import wtf.skruffl.roobot.modules.JoinListener;
import wtf.skruffl.roobot.modules.LeaveListener;
import wtf.skruffl.roobot.modules.SequenceListener;
import wtf.skruffl.roobot.modules.SpamFilter;
import wtf.skruffl.roobot.modules.StarboardListener;
import wtf.skruffl.roobot.modules.UserCommandListener;
import wtf.skruffl.roobot.modules.WelcomeListener;
import wtf.skruffl.roobot.modules.WordFilter;
import wtf.skruffl.roobot.modules.daemons.impl.ChannelWipe;
import wtf.skruffl.roobot.modules.daemons.impl.GiveawayDaemon;
import wtf.skruffl.roobot.modules.daemons.impl.PlayerCountDaemon;
import wtf.skruffl.roobot.modules.daemons.impl.TwitchDaemon;
import wtf.skruffl.roobot.modules.daemons.impl.TwitterListener;
import wtf.skruffl.roobot.util.GeneralUtils;

/**
 * this is the main bot class
 */
public class Bot extends ListenerAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(Bot.class);
    private JDA jda = null;
    @Getter
    private List<String> modules = new ArrayList<>();
    private List<String> gameNames = new ArrayList<>();
    @Getter
    private ConnectionManager manager;
    @Getter
    private GiveawayDaemon giveaways;
    @Getter
    private LinkedHashMap<Long, Message> messagesCache = new LinkedHashMap<>();

    @Getter
    BotConfig config;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        messagesCache.put(event.getMessageIdLong(), event.getMessage());
        int overflowMessages = messagesCache.size() - config.getBotMessagesCache();
        while (overflowMessages > 0) {
            // remove oldest message
            messagesCache.remove(messagesCache.keySet().toArray()[0]);
            overflowMessages--;
        }
    }

    public Bot(BotConfig config, ScheduledExecutorService service) {
        this.config = config;
        try {
            // Build bot instance
            jda = new JDABuilder(config.getBotToken()).build();
            jda.awaitReady();
            LOGGER.info("Bot successfully initialized!");
            jda.addEventListener(this);

            buildConnectionManager();

            for (String module : config.getBotModules().split(";")) {
                modules.add(module);
            }

            // Set game
            String gameName = config.getBotGameName();
            if (gameName != null && !gameName.contentEquals("")) {
                handleGameName(gameName, service);
            }
        } catch (Exception e) {
            LOGGER.error("Critical error while initializing the bot. Exciting.", e);
            System.exit(1);
        }
    }

    private void handleGameName(String gameName, ScheduledExecutorService service) {
        String[] games = gameName.split(";");
        for (String game : games) {
            gameNames.add(game);
        }

        Runnable run = () -> {
            String game = getGameName();
            jda.getPresence().setPresence(Activity.playing(game), false);
        };
        service.scheduleAtFixedRate(run, 0, 1, TimeUnit.MINUTES);
    }

    private String getGameName() {
        Activity status = jda.getPresence().getActivity();
        String currentGame = status == null ? "" : status.getName();
        String game = currentGame;
        if (gameNames.size() == 1) {
            return gameNames.get(0);
        }
        while (game.contentEquals(currentGame)) {
            game = gameNames.get(ThreadLocalRandom.current().nextInt(0, gameNames.size()));
        }
        return game;
    }

    private void buildConnectionManager() {
        String connectionMode = config.getDbMode();
        switch (connectionMode.trim().toLowerCase()) {
        case "couch":
            this.manager = new CouchDbConnectionManager(config);
            break;
        case "h2":
            LOGGER.warn("H2 database support is planned, but not yet implemented.");
            break;
        default:
            LOGGER.warn("No valid dbMode was set; can't connect to database.");
            break;
        }
    }

    public void loadModules() { // NOSONAR
        for (String s : modules) {
            switch (s.toLowerCase()) {
            case "twitch":
                if (isLocal())
                    return;
                LOGGER.info("Loaded twitch daemon!");
                TwitchDaemon.getInstance();
                break;
            case "command":
                LOGGER.info("Loaded command listener!");
                jda.addEventListener(new CommandListener(this));
                break;
            case "verification":
                LOGGER.info("Loaded verification listener!");
                jda.addEventListener(new SequenceListener(this));
                break;
            case "inpanix_welcome":
                LOGGER.info("Loaded inpanix welcome listener!");
                jda.addEventListener(new InPanixWelcomeListener(this));
                break;
            case "playercount": // NOSONAR
                if (new PlayerCountDaemon(this).startDaemon()) {
                    LOGGER.info("Loaded playercount daemon!");
                } else {
                    LOGGER.error("error while starting player count daemon for bot {}", getConfig().getBotName());
                }
                break;
            case "starboard":
                LOGGER.info("Loaded starboard listener!");
                jda.addEventListener(new StarboardListener(this));
                break;
            case "exp":
                LOGGER.info("Loaded experience listener!");
                jda.addEventListener(new ExperienceListener(this));
                break;
            case "welcome":
                LOGGER.info("Loaded welcome listener!");
                jda.addEventListener(new WelcomeListener(this));
                break;
            case "giveaway":
                LOGGER.info("Loaded giveaway listener!");
                GiveawayDaemon daemon = new GiveawayDaemon(this);
                jda.addEventListener(daemon);
                daemon.startDaemon();
                break;
            case "usercommands":
                LOGGER.info("Loaded user command listener!");
                jda.addEventListener(new UserCommandListener(this));
                break;
            case "leave":
                LOGGER.info("Loaded leave listener!");
                jda.addEventListener(new LeaveListener(this));
                break;
            case "ghostping":
                LOGGER.info("Loaded Ghost Ping listener!");
                jda.addEventListener(new GhostPings(this));
                break;
            case "joinlistener":
                LOGGER.info("Loaded Join listener!");
                jda.addEventListener(new JoinListener(this));
                break;
            case "twitter":
                LOGGER.info("Loaded Twitter listener!");
                TwitterListener.getInstance();
                break;
            case "actionlogger":
                LOGGER.info("Loaded Action Logger!");
                jda.addEventListener(new ActionLogger(this));
                break;
            case "spamfilter":
                LOGGER.info("Loaded Spam Filter!");
                jda.addEventListener(new SpamFilter(this));
                break;
            case "wordfilter":
                LOGGER.info("Loaded Word Filter!");
                jda.addEventListener(new WordFilter(this));
                break;
            case "channelwipe": 
                LOGGER.info("Loaded Channel Wipe!");
                new ChannelWipe().startDaemon();
            default:
                LOGGER.error("Couldn't find module for name {}", s);
                break;
            }
        }
    }

    public ConnectionManager getConnectionManager() {
        if (this.manager == null) {
            buildConnectionManager();
        }
        return this.manager;
    }

    public JDA getJDA() {
        return this.jda;
    }

    public boolean isLocal() {
        return config.isBotLocalmode();
    }

    /**
     * sends a log message to the given guild's log channel
     * 
     * @param guild   the guild to send to
     * @param message the message to send
     * @param stuff   parameters as {} in the message
     */
    public void log(Guild guild, String message, Object... stuff) {
        Bots.getInstance().getScheduler().schedule(() -> {
            String msg = replaceParameters(message, stuff);
            GuildSettings setting = GeneralUtils
                    .firstOrNull(manager.getGuildSetting(Setting.LOG_CHANNEL, guild.getIdLong()));
            TextChannel channel = guild.getTextChannelById(setting.getValue());
            channel.sendMessage(msg).queue();
        }, 0, TimeUnit.SECONDS);
    }

    /**
     * sends an action message to the given guild's action channel
     * 
     * @param guild   the guild to send to
     * @param message the message to send
     * @param stuff   parameters as {} in the message
     */
    public void action(Guild guild, String message, Object... stuff) {
        Bots.getInstance().getScheduler().schedule(() -> {
            String msg = replaceParameters(message, stuff);
            GuildSettings setting = GeneralUtils
                    .firstOrNull(manager.getGuildSetting(Setting.ACTION_CHANNEL, guild.getIdLong()));
            TextChannel channel = guild.getTextChannelById(setting.getValue());
            channel.sendMessage(msg).queue();
        }, 0, TimeUnit.SECONDS);
    }

    /**
     * sends an action message to the given guild's action channel
     * 
     * @param guild   the guild to send to
     * @param message the message to send
     */
    public void action(Guild guild, Message msg) {
        Bots.getInstance().getScheduler().schedule(() -> {
            GuildSettings setting = GeneralUtils
                    .firstOrNull(manager.getGuildSetting(Setting.ACTION_CHANNEL, guild.getIdLong()));
            TextChannel channel = guild.getTextChannelById(setting.getValue());
            channel.sendMessage(msg).queue();
        }, 0, TimeUnit.SECONDS);
    }

    /**
     * replaces all instances of "{}" in the string with the objects in the stuff
     * array
     * 
     * @param message
     * @param stuff
     * @return the filled string
     */
    private String replaceParameters(String message, Object... stuff) {
        String[] msgs = message.split("{}");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < msgs.length; i++) {
            builder.append(msgs[i]);
            builder.append(stuff[i]);
        }
        return builder.toString();
    }

}