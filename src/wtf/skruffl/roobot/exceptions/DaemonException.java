package wtf.skruffl.roobot.exceptions;

public class DaemonException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public DaemonException(String cause) {
        super(cause);
    }

    public DaemonException(String cause, Throwable t) {
        super(cause, t);
    }
}
