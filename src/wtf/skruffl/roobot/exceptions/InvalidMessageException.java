package wtf.skruffl.roobot.exceptions;

public class InvalidMessageException extends IllegalArgumentException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public InvalidMessageException(String message) {
        super(message);
    }

    public InvalidMessageException(Exception e) {
        super(e);
    }

    public InvalidMessageException() {
        super();
    }
}
