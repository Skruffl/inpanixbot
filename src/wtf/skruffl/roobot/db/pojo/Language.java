package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;
import lombok.Getter;

@Getter
public class Language extends Pojo {
    private String STARBOARD_THRESHOLD_SUCCESS;
    private String STARBOARD_THRESHOLD_ERROR;

    private String CONNECT_ADD;
    private String CONNECT_REMOVE;
    private String CONNECT_ERROR;

    private String CONNECTIONS_HEADER;
    private String CONNECTIONS_VISIBLE;
    private String CONNECTIONS_IS_NOT_VISIBLE;

    private String SEQUENCE_ERROR;

    private String SET_GUILD_SETTING_SUCCESS;
    private String SET_GUILD_SETTING_ERROR;

    private String TOGGLE_STREAMER_VISIBLE;
    private String TOGGLE_STREAMER_INVISIBLE;
    private String TOGGLE_STREAMER_ERROR;

    private String CREATE_GIVEAWAY_NOT_ENOUGH_XP;
    private String CREATE_GIVEAWAY_END_TIME;
    private String CREATE_GIVEAWAY_XP_REWARD;
    private String CREATE_GIVEAWAY_NO_GIVEAWAY_CHANNEL;
    private String CREATE_GIVEAWAY_ERROR;

    private String GAMBLE_NOT_ENOUGH_XP;
    private String GAMBLE_INVALID_CHANNEL;
    private String GAMBLE_BELOW_THRESHOLD;
    private String GAMBLE_FIVE_TIMES;
    private String GAMBLE_THREE_TIMES;
    private String GAMBLE_DOUBLED;
    private String GAMBLE_LOSS;
    private String GAMBLE_ERROR;

    private String PING_RESPONSE;
    private String PING_ERROR;

    private String STATS_TOO_MANY_PEOPLE;
    private String STATS_XP;
    private String STATS_LEVEL;
    private String STATS_STARBOARD_MESSAGES;
    private String GIVEAWAY_EMBED_WIN_XP;
    private String GIVEAWAY_EMBED_WIN_NO_XP;
    private String GIVEAWAY_CONGRATULATION;

    private String TWITCH_LIVE;
    private String TWITCH_PLAYING;
    private String TWITCH_STREAM_TITLE;
    private String TWITCH_FOLLOWERS;
    private String TWITCH_TOTAL_VIEWS;

    private String EXP_LEVELUP;

    private String STARBOARD_MESSAGE_LINK;
    private String STARBOARD_MESSAGE_CHANNEL;
    private String STARBOARD_MESSAGE_REACTIONS;
    private String STARBOARD_IMAGE_ATTACHMENT;

    private String WELCOME_INITIAL_MESSAGE;
    private String WELCOME_FINAL_MESSAGE;
    private String WELCOME_ERROR;
    private String WELCOME_SOLVE_CAPTCHA;

    private String MEMBER_COUNT;

    private String LEAVE_NOTIFICATION;

    private String INFO_CARD;
    private String INFO_CREATION_DATE;
    private String INFO_JOIN_DATE;
    private String INFO_MEMBERS;
    private String INFO_BANS;

    private String TICKET_ERROR;

    private String COUNT_AMOUNT;

    private String KICK_LIST;
    private String KICK_INVALID;

    private String BAN_LIST;
    private String BAN_INVALID;

    private String MUTE_ON;
    private String MUTE_OFF;
    private String MUTE_ERROR;

    private String WORD_FILTER;

    private String WORDCTL_LIST;
    private String WORDCTL_ADDED;
    private String WORDCTL_REMOVED;
    private String WORDCTL_ERROR;
    
    private String TWITTER_NEW_TWEET;
    private String TWITTER_VIDEO;
}