package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;

public class LoginSequence extends Pojo {
    private String guildId;
    private String messageId;
    private String roleId;
    private String removalRoleId;
    private String unicodeEmoji;
    private Boolean removal;

    public LoginSequence(Long guildId, Long messageId, Long roleId, Long removalRoleId, String unicodeEmoji,
            Boolean removal) {
        this.guildId = guildId.toString();
        this.messageId = messageId.toString();
        this.roleId = roleId.toString();
        this.removalRoleId = removalRoleId.toString();
        this.unicodeEmoji = unicodeEmoji;
        this.removal = removal;
    }

    public Long getGuildId() {
        return Long.parseLong(this.guildId);
    }

    public Long getMessageId() {
        return Long.parseLong(this.messageId);
    }

    public Long getRoleId() {
        if (this.roleId == null) {
            return null;
        }
        return Long.parseLong(this.roleId);
    }

    public Long getRemovalRoleId() {
        if (this.removalRoleId == null) {
            return null;
        }
        return Long.parseLong(this.removalRoleId);
    }

    public String getUnicodeEmoji() {
        return this.unicodeEmoji;
    }

    public Boolean isRemoval() {
        return this.removal;
    }
}