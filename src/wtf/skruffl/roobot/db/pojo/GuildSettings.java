package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.Pojo;

public class GuildSettings extends Pojo {
    private String guildId;
    private String type;
    private String value;

    public GuildSettings(Long guildId, Setting type, String value) {
        this.guildId = guildId.toString();
        this.type = type.toString();
        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getGuildId(){ 
        return Long.parseLong(this.guildId);
    }

    public String getType() {
        return this.type;
    }

    public String getValue() {
        return this.value;
    }

}