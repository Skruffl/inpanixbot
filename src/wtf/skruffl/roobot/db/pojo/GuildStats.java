package wtf.skruffl.roobot.db.pojo;

import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.Pojo;
import wtf.skruffl.roobot.main.Bot;

public class GuildStats extends Pojo {
    private int memberCount;
    private int onlineMembers;
    private int messageCount;
    private int starboardMessages;
    private static Bot bot;

    private GuildStats(Guild guild) {
        _id = guild.getId();
        poll();
    }

    public static GuildStats build(Guild guild, Bot bot) {
        GuildStats.bot = bot;
        ConnectionManager manager = bot.getConnectionManager();
        GuildStats guildStats = manager.getGuildStats(guild.getIdLong());
        if (guildStats == null) {
            guildStats = new GuildStats(guild);
            manager.persistGuildStats(guildStats);
            guildStats = manager.getGuildStats(guild.getIdLong());
        }
        return guildStats;
    }

    /**
     * <p>
     * reload the member count
     * </p>
     * this won't reload the messages count since this is manually counted by the
     * respective listeners.
     */
    public void poll() {
        Guild guild = bot.getJDA().getGuildById(_id);

        memberCount = guild.getMembers().size();

        onlineMembers = 0;
        for (Member member : guild.getMembers()) {
            if (!member.getOnlineStatus().equals(OnlineStatus.OFFLINE)) {
                onlineMembers++;
            }
        }
    }

    public void newStarboardMessage() {
        starboardMessages++;
    }

    public void newMessage() {
        messageCount++;
    }

    public Integer getMemberCount() {
        poll();
        return this.memberCount;
    }

    public Integer getOnlineMembers() {
        poll();
        return this.onlineMembers;
    }

    public Integer getMessageCount() {
        return this.messageCount;
    }

    public Integer getStarboardMessageCount() {
        return this.starboardMessages;
    }
}