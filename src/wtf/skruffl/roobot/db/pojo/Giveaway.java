package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;

public class Giveaway extends Pojo {
    private String guildId;
    private String channelId;
    private String messageId;
    private String emoji;
    private String endTs;
    private Long xpReward;

    public Giveaway(Long guildId, Long channelId, Long messageId, Long endTs, String emoji, Long xpReward) {
        this.guildId = guildId.toString();
        this.channelId = channelId.toString();
        this.messageId = messageId.toString();
        this.emoji = emoji;
        this.endTs = endTs.toString();
        this.xpReward = xpReward;
    }

    public Long getGuild() {
        return Long.parseLong(guildId);
    }

    public Long getChannel() {
        return Long.parseLong(channelId);
    }

    public Long getMessage() {
        return Long.parseLong(messageId);
    }

    public String getEmoji() {
        return this.emoji;
    }

    public Long getEndTs() {
        return Long.parseLong(endTs);
    }

    public Long getXpReward() {
        return this.xpReward;
    }
}