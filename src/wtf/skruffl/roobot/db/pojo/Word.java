package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;
import lombok.Getter;
import net.dv8tion.jda.api.entities.Guild;

/**
 * Word
 */
@Getter
public class Word extends Pojo {
    private String guildId;
    private String word;

    public Word(Guild guild, String word) {
        this.word = word;
        guildId = guild.getId();
    }
}