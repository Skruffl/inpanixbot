package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;

public class TwitchNotifs extends Pojo {
    private String channelName;
    private Boolean isVisible;
    private Boolean isPingable;
    private String guildId;

    public TwitchNotifs(String channelName, Long guildId, Boolean isVisible) {
        this.channelName = channelName;
        this.guildId = guildId.toString();
        this.isVisible = isVisible;
    }

    public String getChannelName() {
        return this.channelName;
    }

    public Boolean isVisible() {
        return this.isVisible;
    }

    public void setVisible(Boolean b) {
        this.isVisible = b;
    }

    public Long getGuildId() {
        return Long.parseLong(this.guildId);
    }

    public boolean isPingable() {
        if (Boolean.FALSE.equals(isPingable)) {
            return false;
        }
        return true;
    }
}