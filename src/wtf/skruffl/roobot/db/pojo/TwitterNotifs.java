package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;
import lombok.Getter;
import lombok.Setter;

public class TwitterNotifs extends Pojo {
    @Getter
    private String twitterName;
    @Getter
    private String guildId;
    @Getter
    @Setter
    private String twitterId;

    public TwitterNotifs(String twitterName, long guildId) {
        this.twitterName = twitterName;
        this.guildId = Long.toString(guildId);
    }
}