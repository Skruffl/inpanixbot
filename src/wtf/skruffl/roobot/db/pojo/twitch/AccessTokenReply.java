package wtf.skruffl.roobot.db.pojo.twitch;

import lombok.Getter;

@Getter
public class AccessTokenReply {
    private String access_token;
    private String refresh_token;
    private long expires_in;
    private String scope;
    private String token_type;
}