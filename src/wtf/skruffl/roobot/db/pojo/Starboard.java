package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;
import net.dv8tion.jda.api.entities.TextChannel;

public class Starboard extends Pojo {
    private String starBoard;
    private String original;
    private String guildId;
    private String textChannelId;

    public Starboard(Long starboard, Long original, TextChannel channel) {
        this.starBoard = starboard.toString();
        this.original = original.toString();
        this.guildId = channel.getGuild().getId();
        this.textChannelId = channel.getId();
    }


    public Long getStarboard(){
        return Long.parseLong(starBoard);
    }

    public Long getOriginal() {
        return Long.parseLong(original);
    }

    public Long getChannel() {
        return Long.parseLong(textChannelId);
    }

    public Long getGuild() {
        return Long.parseLong(guildId);
    }
}