package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;

public class LoginResponse extends Pojo {
    private String userId;
    private String guildId;
    private String expectedResponse;
    private String channelId;
    private String reactionMessageId;
    private String captcha;

    public LoginResponse(Member member, String expectedResponse, String captcha, Message welcomeMessage) {
        guildId = member.getGuild().getId();
        userId = member.getUser().getId();
        this.expectedResponse = expectedResponse;
        this.channelId = welcomeMessage.getChannel().getId();
        this.reactionMessageId = welcomeMessage.getId();
        this.captcha =  captcha;
    }

    public String getExpectedResponse() {
        return this.expectedResponse;
    }

    public Long getGuildId() {
        return Long.parseLong(guildId);
    }

    public Long getUserId() {
        return Long.parseLong(userId);
    }

    public Long getChannelId() {
        return Long.parseLong(channelId);
    }

    public Long getMessageId() {
        return Long.parseLong(reactionMessageId);
    }

    public String getCaptcha() {
        return this.captcha;
    }
}