package wtf.skruffl.roobot.db.pojo;

import wtf.skruffl.roobot.db.Pojo;

public class StarboardThreshold extends Pojo {
    private int threshold;
    private String guildId;
    private String groupId;

    public StarboardThreshold(int threshold, long guildId, long groupId) {
        this.threshold = threshold;
        this.guildId = Long.toString(guildId);
        this.groupId = Long.toString(groupId);
    }

    public int getThreshold() {
        return this.threshold;
    }

    public long getGuildId() {
        return Long.parseLong(guildId);
    }

    public long getGroupId() {
        return Long.parseLong(groupId);
    }

}