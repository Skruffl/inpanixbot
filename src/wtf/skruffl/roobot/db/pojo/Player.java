package wtf.skruffl.roobot.db.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import lombok.Getter;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.Pojo;
import wtf.skruffl.roobot.main.Bot;

public class Player extends Pojo {

    private static final double curve = 0.311d;

    private String playerId;
    private String guildId;
    private String xpCount;
    private int starboardedMessages;
    private int messages;
    private int stars;
    @Getter
    private int warns;
    private String language;

    private Long lastMessage;
    private static Bot bot;

    /**
     * this is used to calculate the curve
     * 
     * @param args
     */
    public static void main(String[] args) {
        class XpRunner {
            private int level = 0;
            private long xp = 0;
            private int sentMessages = 0;
            private List<Integer> results = new ArrayList<>();

            public void run(int toLvl) {
                while (level <= toLvl) {
                    sentMessages++;
                    xp += ThreadLocalRandom.current().nextInt(5, 20);
                    int tmpLevel = ((Double) ((1 * Math.pow(xp, curve)) - 1)).intValue();
                    if (tmpLevel > level) {
                        level = tmpLevel;
                        results.add(sentMessages);
                    }
                }
            }

            public List<Integer> getResults() {
                return this.results;
            }
        }

        int runs = 30;
        int toLvl = 50;
        List<XpRunner> runners = new ArrayList<>();

        for (int i = 0; i < runs; i++) {
            XpRunner runner = new XpRunner();
            runner.run(toLvl);
            runners.add(runner);
        }

        for (int i = 0; i <= toLvl; i++) {
            long msgs = 0;
            for (XpRunner runner : runners) {
                msgs += runner.getResults().get(i);
            }
            System.out.println(String.format("Level %s with an average of %s messages", i, msgs / runs));
        }
    }

    private Player(Member member) {
        playerId = member.getUser().getId();
        guildId = member.getGuild().getId();
        xpCount = "1";
        starboardedMessages = 0;
        messages = 1;
        warns = 0;
    }

    public static Player build(Member member, Bot bot) {
        Player.bot = bot;
        ConnectionManager manager = bot.getConnectionManager();
        List<Player> list = manager.getPlayer(member.getGuild().getIdLong(), member.getUser().getIdLong());
        if (list.isEmpty()) {
            Player player = new Player(member);
            manager.persistPlayer(player);
            list = manager.getPlayer(member.getGuild().getIdLong(), member.getUser().getIdLong());
            return list.get(0);
        } else {
            return list.get(0);
        }
    }

    public String getLanguage() {
        return this.language;
    }

    public Long getXpCount() {
        return Long.parseLong(xpCount);
    }

    public Integer getLevel() {
        Double levelValue = (1 * Math.pow(getXpCount(), curve)) - 1;
        return levelValue.intValue();
    }

    public int getStars() {
        return stars;
    }

    public Integer getMessages() {
        return this.messages;
    }

    public Integer getStarboardMessages() {
        return this.starboardedMessages;
    }

    public Long getMemberId() {
        return Long.parseLong(playerId);
    }

    public Long getGuildId() {
        return Long.parseLong(guildId);
    }

    public void newStarboardMessage() {
        this.starboardedMessages++;
        bot.getConnectionManager().persistPlayer(this);
    }

    public void addXp(Long xp) {
        xpCount = Long.toString(Long.parseLong(xpCount) + xp);
        bot.getConnectionManager().persistPlayer(this);
    }

    public void removeXp(Long xp) {
        xpCount = Long.toString(Long.parseLong(xpCount) - xp);
        bot.getConnectionManager().persistPlayer(this);
    }

    public void warn() {
        warns++;
        Guild guild = bot.getJDA().getGuildById(guildId);
        Member member = guild.getMemberById(playerId);
        if (warns == 3) {
            guild.kick(member).queue();
        }
        if (warns == 5) {
            guild.ban(member, 7);
        }
    }

    public void newMessage() {
        this.messages++;
        if (lastMessage == null) {
            lastMessage = 0l;
        }
        addXp();
        lastMessage = System.currentTimeMillis();
        bot.getConnectionManager().persistPlayer(this);
    }

    public void addStar(int starCount) {
        // If starCount is starBoardCount, we'll add the full amount, else we just
        // increment it by one
        // this is sufficient, since this method is called every time the starboard
        // reaction count increments by one, except when the message gets into the
        // starboard for the first time

        int starBoardCount = bot.getConfig().getBotStarCount();
        if (starCount == starBoardCount) {
            this.stars += starCount;
        } else {
            this.stars++;
        }
        bot.getConnectionManager().persistPlayer(this);
    }

    private void addXp() {
        Integer delay;
        try {
            delay = bot.getConfig().getBotXpDelay();
        } catch (NumberFormatException e) {
            delay = 60;
        }
        delay *= 1000;
        Long currentTimestamp = System.currentTimeMillis();
        if (currentTimestamp > (lastMessage + delay)) {
            xpCount = ((Number) (getXpCount() + ThreadLocalRandom.current().nextLong(5l, 20l))).toString();
        }
    }

}