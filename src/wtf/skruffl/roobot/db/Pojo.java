package wtf.skruffl.roobot.db;

public abstract class Pojo {
    protected String _id;
    protected String _rev;
    protected String group = getClass().getSimpleName();

    public String getId() {
        return this._id;
    }

    public String getRevision() {
        return this._rev;
    }
}