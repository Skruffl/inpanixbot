package wtf.skruffl.roobot.db;

import java.util.List;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.Giveaway;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.GuildStats;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.db.pojo.LoginResponse;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.db.pojo.Starboard;
import wtf.skruffl.roobot.db.pojo.StarboardThreshold;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.db.pojo.TwitterNotifs;
import wtf.skruffl.roobot.db.pojo.Word;

public interface ConnectionManager {
    
    // CREATE;UPDATE
    public boolean persistGiveaway(Giveaway giveaway);

    public boolean persistGuildSetting(GuildSettings settings);

    public boolean persistGuildStats(GuildStats stats);

    public boolean persistLoginResponse(LoginResponse response);

    public boolean persistLoginSequence(LoginSequence seq);

    public boolean persistPlayer(Player player);

    public boolean persistStarboard(Starboard starboard);

    public boolean persistStarboardThreshold(StarboardThreshold threshold);

    public boolean persistTwitchNotifs(TwitchNotifs notif);

    public boolean persistTwitterNotifs(TwitterNotifs notif);

    public boolean persistWord(Word word);

    // REMOVE
    public boolean deleteGiveaway(Giveaway giveaway);

    public boolean deleteGuildSetting(GuildSettings settings);

    public boolean deleteGuildStats(GuildStats stats);

    public boolean deleteLoginResponse(LoginResponse response);

    public boolean deleteLoginSequence(LoginSequence seq);

    public boolean deletePlayer(Player player);

    public boolean deleteStarboard(Starboard starboard);

    public boolean deleteStarboardThreshold(StarboardThreshold threshold);

    public boolean deleteTwitchNotifs(TwitchNotifs notif);

    public boolean deleteTwitterNotifs(TwitterNotifs notif);

    public boolean deleteWord(Word word);

    // READ
    public List<Giveaway> getAllGiveaways();

    public List<GuildSettings> getGuildSetting(Setting setting, long guildId);

    public GuildStats getGuildStats(long guildId);

    public List<LoginResponse> getLoginResponseByChannel(long guildId, long channelId);

    public List<LoginResponse> getLoginResponseByPlayer(long guildId, long playerId);

    public List<LoginSequence> getSequence(long guildId, long messageId, boolean removed);

    public List<Player> getPlayer(long guildId, long playerId);

    public Player getPlayerById(String id);

    public List<Starboard> getStarboards(long guildId, long original);

    public List<StarboardThreshold> getThresholds(long guildId);

    public List<TwitchNotifs> getVisibleStreamers(long guildId);

    public List<TwitchNotifs> getAllStreamers();

    public List<TwitchNotifs> getStreamer(long guildId, String channelName);

    public List<TwitchNotifs> getGuildStreamers(long guildId);

    public Language getLanguage(String name);

    public List<TwitterNotifs> getAllTwitterNotifs();

    public List<TwitterNotifs> getTwitterNotif(long idLong, String string);
    
    public List<Word> getWordsForGuild(long guildId);


}