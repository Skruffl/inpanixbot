package wtf.skruffl.roobot.db;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.NoDocumentException;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.Giveaway;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.GuildStats;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.db.pojo.LoginResponse;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.db.pojo.Starboard;
import wtf.skruffl.roobot.db.pojo.StarboardThreshold;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.db.pojo.TwitterNotifs;
import wtf.skruffl.roobot.db.pojo.Word;
import wtf.skruffl.roobot.main.BotConfig;

public class CouchDbConnectionManager implements ConnectionManager {
	private CouchDbClient conn;
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CouchDbConnectionManager.class);
	private BotConfig config;

	public CouchDbConnectionManager(BotConfig config) {
		this.config = config;
		createNewConnection();
	}

	/**
	 * Saves the given Object to the underlying database.
	 * 
	 * @param value the value to be saved (As a DAO, please!)
	 * @return true, if the operation succeeded
	 */
	public boolean save(Object value) {
		LOGGER.debug("Saving object {}", value);
		if (value == null) {
			return false;
		}
		try {
			conn.save(value);
			return true;
		} catch (Exception e) {
			LOGGER.error("Error while saving object to database", e);
			return false;
		}
	}

	private void createNewConnection() {
		Logger logger = Logger.getLogger(CouchDbClient.class.getName());
		logger.setLevel(Level.OFF);
		CouchDbProperties props = new CouchDbProperties().setDbName(config.getDbCatalog()).setCreateDbIfNotExist(true)
				.setPort(config.getDbPort()).setHost(config.getDbAddress()).setProtocol("http");
		if (!config.getDbPassword().contentEquals("")) {
			props = props.setPassword(config.getDbPassword());
		}
		if (!config.getDbUsername().contentEquals("")) {
			props = props.setUsername(config.getDbUsername());
		}
		conn = new CouchDbClient(props);
	}

	@Override
	public List<Giveaway> getAllGiveaways() {
		LOGGER.debug("Fetching all giveaways");
		return conn.findDocs("{\"selector\": {\"group\": {\"$eq\": \"Giveaway\"}}}", Giveaway.class);
	}

	@Override
	public List<GuildSettings> getGuildSetting(Setting setting, long guildId) {
		LOGGER.debug("Fetching guild setting for setting {} on guild {}", setting, guildId);
		return conn.findDocs("{\"selector\": {\"group\": {\"$eq\": \"GuildSettings\"},\"guildId\": {\"$eq\": \""
				+ guildId + "\"}, \"type\":{\"$eq\":\"" + setting.toString() + "\"}}}", GuildSettings.class);
	}

	@Override
	public GuildStats getGuildStats(long guildId) {
		LOGGER.debug("Getting guild stats for guild {}", guildId);
		try {
			return conn.find(GuildStats.class, Long.toString(guildId));
		} catch (NoDocumentException e) { // NOSONAR
			return null;
		}
	}

	@Override
	public List<LoginResponse> getLoginResponseByChannel(long guildId, long channelId) {
		LOGGER.debug("Getting login response for channel {} on guild {}", channelId, guildId);
		return conn.findDocs("{\"selector\": {\"group\": {\"$eq\": \"LoginResponse\"}, \"guildId\": {\"$eq\":\""
				+ guildId + "\"}, \"channelId\":{\"$eq\":\"" + channelId + "\"}}}", LoginResponse.class);
	}

	@Override
	public List<LoginResponse> getLoginResponseByPlayer(long guildId, long playerId) {
		LOGGER.debug("Getting login response for player {} on guild {}", playerId, guildId);
		return conn.findDocs("{\"selector\": {\"group\": {\"$eq\": \"LoginResponse\"},\"userId\":{\"$eq\":\"" + playerId
				+ "\"},\"guildId\":{\"$eq\":\"" + guildId + "\"}}}", LoginResponse.class);
	}

	@Override
	public List<LoginSequence> getSequence(long guildId, long messageId, boolean removed) {
		LOGGER.debug("Getting {} login sequence for message {} on guild {}", removed ? "removal" : "", messageId,
				guildId);
		return conn.findDocs(
				"{\"selector\":{\"group\":{\"$eq\":\"LoginSequence\"},\"guildId\":{\"$eq\":\"" + guildId
						+ "\"},\"messageId\":{\"$eq\":\"" + messageId + "\"},\"removal\":{\"$eq\":" + removed + "}}}",
				LoginSequence.class);
	}

	@Override
	public List<Player> getPlayer(long guildId, long playerId) {
		LOGGER.debug("Getting Player {} on guild {}", playerId, guildId);
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"Player\"},\"guildId\":{\"$eq\":\"" + guildId
				+ "\"},\"playerId\":{\"$eq\":\"" + playerId + "\"}}}", Player.class);
	}

	@Override
	public List<Starboard> getStarboards(long guildId, long original) {
		LOGGER.debug("Getting Starboard for message {} on guild {}", original, guildId);
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"Starboard\"},\"guildId\":{\"$eq\":\"" + guildId
				+ "\"}\"original\":{\"$eq\":\"" + original + "\"}}}", Starboard.class);

	}

	@Override
	public List<StarboardThreshold> getThresholds(long guildId) {
		LOGGER.debug("Getting starboard thresholds for guild {}", guildId);
		return conn.findDocs(
				"{\"selector\":{\"group\":{\"$eq\":\"StarboardThreshold\"},\"guildId\":{\"$eq\":\"" + guildId + "\"}}}",
				StarboardThreshold.class);
	}

	@Override
	public List<TwitchNotifs> getVisibleStreamers(long guildId) {
		LOGGER.debug("Getting visible streamers for guild {}", guildId);
		return conn.findDocs(
				"{\"selector\":{\"group\":{\"$eq\":\"TwitchNotifs\"},\"isVisible\":{\"$eq\":true},\"guildId\":{\"$eq\":\""
						+ guildId + "\"}}}",
				TwitchNotifs.class);
	}

	@Override
	public List<TwitchNotifs> getAllStreamers() {
		LOGGER.debug("Getting all streamers");
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"TwitchNotifs\"}}}", TwitchNotifs.class);
	}

	@Override
	public List<TwitchNotifs> getStreamer(long guildId, String channelName) {
		LOGGER.debug("Getting streamer {} for guild {}", channelName, guildId);
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"TwitchNotifs\"},\"guildId\":{\"$eq\":\"" + guildId
				+ "\"},\"channelName\":{\"$eq\":\"" + channelName + "\"}}}", TwitchNotifs.class);
	}

	@Override
	public List<TwitchNotifs> getGuildStreamers(long guildId) {
		LOGGER.debug("Getting all streamers for guild {}", guildId);
		return conn.findDocs(
				"{\"selector\":{\"group\":{\"$eq\":\"TwitchNotifs\"},\"guildId\":{\"$eq\":\"" + guildId + "\"}}}",
				TwitchNotifs.class);
	}

	@Override
	public boolean persistGiveaway(Giveaway giveaway) {
		LOGGER.debug("Persisting giveaway on message {} for guild {}", giveaway.getMessage(), giveaway.getGuild());
		try {
			if (giveaway.getId() == null || giveaway.getId().isEmpty()) {
				conn.save(giveaway);
			} else {
				conn.update(giveaway);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting giveaway", e);
			return false;
		}
	}

	@Override
	public boolean persistGuildSetting(GuildSettings settings) {
		LOGGER.debug("Setting guild setting {} for guild {} to {}", settings.getType(), settings.getGuildId(),
				settings.getValue());
		try {
			if (settings.getId() == null || settings.getId().isEmpty()) {
				conn.save(settings);
			} else {
				conn.update(settings);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting GuildSettings", e);
			return false;
		}
	}

	@Override
	public boolean persistGuildStats(GuildStats stats) {
		LOGGER.debug("Saving guild settings for guild {}", stats.getId());
		try {
			if (stats.getRevision() == null || stats.getRevision().isEmpty()) {
				conn.save(stats);
			} else {
				conn.update(stats);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting GuildStats", e);
			return false;
		}
	}

	@Override
	public boolean persistLoginResponse(LoginResponse response) {
		LOGGER.debug("Saving login response or user {} on guild {}", response.getUserId(), response.getGuildId());
		try {
			if (response.getId() == null || response.getId().isEmpty()) {
				conn.save(response);
			} else {
				conn.update(response);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting LoginResponse", e);
			return false;
		}
	}

	@Override
	public boolean persistLoginSequence(LoginSequence seq) {
		LOGGER.debug("Saving login sequence on message {}, removal {}, on guild {}", seq.getMessageId(),
				seq.isRemoval(), seq.getGuildId());
		try {
			if (seq.getId() == null || seq.getId().isEmpty()) {
				conn.save(seq);
			} else {
				conn.update(seq);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting LoginSequence", e);
			return false;
		}
	}

	@Override
	public boolean persistPlayer(Player player) {
		LOGGER.debug("Saving player {} on guild {}", player.getMemberId(), player.getGuildId());
		try {
			if (player.getId() == null || player.getId().isEmpty()) {
				conn.save(player);
			} else {
				conn.update(player);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting Player", e);
			return false;
		}
	}

	@Override
	public boolean persistStarboard(Starboard starboard) {
		LOGGER.debug("Saving starboard message {} on guild {}", starboard.getStarboard(), starboard.getGuild());
		try {
			if (starboard.getId() == null || starboard.getId().isEmpty()) {
				conn.save(starboard);
			} else {
				conn.update(starboard);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting Starboard", e);
			return false;
		}
	}

	@Override
	public boolean persistStarboardThreshold(StarboardThreshold threshold) {
		LOGGER.debug("Saving starboard threshold for role id {} for {} stars on guild {}", threshold.getGroupId(),
				threshold.getThreshold(), threshold.getGuildId());
		try {
			if (threshold.getId() == null || threshold.getId().isEmpty()) {
				conn.save(threshold);
			} else {
				conn.update(threshold);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting StarboardThreshold", e);
			return false;
		}
	}

	@Override
	public boolean persistTwitchNotifs(TwitchNotifs notif) {
		LOGGER.debug("Persisting twitch notifs for streamer {} on guild {}", notif.getChannelName(),
				notif.getGuildId());
		try {
			if (notif.getId() == null || notif.getId().isEmpty()) {
				conn.save(notif);
			} else {
				conn.update(notif);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting TwitchNotifs", e);
			return false;
		}
	}

	@Override
	public Player getPlayerById(String id) {
		LOGGER.debug("Getting player for id {}", id);
		return conn.find(Player.class, id);
	}

	@Override
	public boolean deleteGiveaway(Giveaway giveaway) {
		LOGGER.debug("Deleting giveaway for message {} on guild {}", giveaway.getMessage(), giveaway.getGuild());
		try {
			conn.remove(giveaway);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting giveaway", e);
			return false;
		}
	}

	@Override
	public boolean deleteGuildSetting(GuildSettings settings) {
		LOGGER.debug("Deleting guild setting {} with value {} on guild {}", settings.getType(), settings.getValue(),
				settings.getGuildId());
		try {
			conn.remove(settings);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting GuildSettings", e);
			return false;
		}
	}

	@Override
	public boolean deleteGuildStats(GuildStats stats) {
		LOGGER.debug("Deleting guild stats for guild {}", stats.getId());
		try {
			conn.remove(stats);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting GuildStats", e);
			return false;
		}
	}

	@Override
	public boolean deleteLoginResponse(LoginResponse response) {
		LOGGER.debug("Deleting login response on message {} on guild {}", response.getMessageId(),
				response.getGuildId());
		try {
			conn.remove(response);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting LoginResponse", e);
			return false;
		}
	}

	@Override
	public boolean deleteLoginSequence(LoginSequence seq) {
		LOGGER.debug("Deleting login sequence on message {} for guild {}", seq.getMessageId(), seq.getGuildId());
		try {
			conn.remove(seq);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting LoginSequence", e);
			return false;
		}
	}

	@Override
	public boolean deletePlayer(Player player) {
		LOGGER.debug("Deleting player {} on guild {}", player.getMemberId(), player.getGuildId());
		try {
			conn.remove(player);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting Player", e);
			return false;
		}
	}

	@Override
	public boolean deleteStarboard(Starboard starboard) {
		LOGGER.debug("Deleting starboard message {} on guild {}", starboard.getOriginal(), starboard.getGuild());
		try {
			conn.remove(starboard);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting Starboard", e);
			return false;
		}
	}

	@Override
	public boolean deleteStarboardThreshold(StarboardThreshold threshold) {
		LOGGER.debug("Deleting threshold on for guild {} on guild {}", threshold.getGroupId(), threshold.getGuildId());
		try {
			conn.remove(threshold);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting StarboardThreshold", e);
			return false;
		}
	}

	@Override
	public boolean deleteTwitchNotifs(TwitchNotifs notif) {
		LOGGER.debug("Deleting twitch notif for streamer {} on guild {}", notif.getChannelName(), notif.getGuildId());
		try {
			conn.remove(notif);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting TwitchNotifs", e);
			return false;
		}
	}

	@Override
	public Language getLanguage(String name) {
		LOGGER.debug("Getting language {}", name);
		return conn.find(Language.class, name);
	}

	@Override
	public boolean persistTwitterNotifs(TwitterNotifs notif) {
		LOGGER.debug("Persisting twitter notifs {} on guild {}", notif.getTwitterName(), notif.getGuildId());
		try {
			if (notif.getId() != null) {
				conn.update(notif);
			} else {
				conn.save(notif);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while saving TwitterNotifs", e);
			return false;
		}
	}

	@Override
	public boolean deleteTwitterNotifs(TwitterNotifs notif) {
		LOGGER.debug("Deleting twitter notifs {} on guild {}", notif.getTwitterName(), notif.getGuildId());
		try {
			conn.remove(notif);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting TwitterNotifs", e);
			return false;
		}
	}

	@Override
	public List<TwitterNotifs> getAllTwitterNotifs() {
		LOGGER.debug("Getting all twitter accounts");
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"TwitterNotifs\"}}}", TwitterNotifs.class);
	}

	@Override
	public List<TwitterNotifs> getTwitterNotif(long guildId, String twitterName) {
		LOGGER.debug("Getting twitternotif {} for guild {}", twitterName, guildId);
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"TwitterNotifs\"},\"guildId\":{\"$eq\":\"" + guildId
				+ "\"},\"twitterName\":{\"$eq\":\"" + twitterName + "\"}}}", TwitterNotifs.class);
	}

	@Override
	public boolean persistWord(Word word) {
		LOGGER.debug("Persisting Word {} for guild {}", word.getWord(), word.getGuildId());
		try {
			conn.save(word);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while persisting word", e);
			return false;
		}
	}

	@Override
	public boolean deleteWord(Word word) {
		LOGGER.debug("Deleting Word {} for guild {}", word.getWord(), word.getGuildId());
		try {
			conn.remove(word);
			return true;
		} catch (Exception e) {
			LOGGER.error("Uncaught exception while deleting word", e);
			return false;
		}
	}

	@Override
	public List<Word> getWordsForGuild(long guildId) {
		LOGGER.debug("Getting all words for guild {}", guildId);
		return conn.findDocs("{\"selector\":{\"group\":{\"$eq\":\"Word\"},\"guildId\":{\"$eq\":\"" + guildId + "\"}}}",
				Word.class);
	}

}
