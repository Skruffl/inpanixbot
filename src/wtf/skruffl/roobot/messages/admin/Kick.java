package wtf.skruffl.roobot.messages.admin;

import java.util.StringJoiner;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * Kick
 */
public class Kick implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        StringJoiner joiner = new StringJoiner(", ");
        for (Member member : event.getMessage().getMentionedMembers()) {
            event.getGuild().kick(member).queue();
            joiner.add(member.getUser().getName());
        }

        String msg = MessageUtils.getMessageTextFor(event.getMember(), MessageType.KICK_LIST, bot);
        msg.replace("KICK_MEMBERS", joiner.toString());

        event.getChannel().sendMessage(msg).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel().sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.KICK_INVALID, bot))
                .queue();
    }

    @Override
    public String getInfo() {
        return "Kicks the given members.";
    }

}