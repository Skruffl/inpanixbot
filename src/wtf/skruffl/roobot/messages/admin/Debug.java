package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.modules.WelcomeListener;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Debug implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {

        new WelcomeListener(bot).onGuildMemberJoin(new GuildMemberJoinEvent(bot.getJDA(), 0l, event.getMember()));
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        //
    }

    @Override
    public String getInfo() {
        return null;
    }

}