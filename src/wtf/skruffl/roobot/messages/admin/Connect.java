package wtf.skruffl.roobot.messages.admin;

import java.util.List;

import wtf.skruffl.roobot.constants.GeneralConstants;
import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.db.pojo.TwitterNotifs;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.modules.daemons.impl.TwitchDaemon;
import wtf.skruffl.roobot.modules.daemons.impl.TwitterListener;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Connect implements MessageHandler {

	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		if (args.length < 3 || args[1].contentEquals("")) {
			throw new InvalidMessageException();
		}

		if (args[1].contentEquals("twitter")) {
			doTwitter(event, args, bot);
		} else if (args[1].contentEquals("twitch")) {
			doTwitch(event, args, bot);
		}
	}

	private void doTwitter(MessageReceivedEvent event, String[] args, Bot bot) {
		List<TwitterNotifs> results = bot.getManager().getTwitterNotif(event.getGuild().getIdLong(), args[2]);

		if (results.isEmpty()) {
			TwitterNotifs insert = new TwitterNotifs(args[2], event.getGuild().getIdLong());
			bot.getManager().persistTwitterNotifs(insert);
			
			String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECT_ADD, bot);
			message = message.replace("STREAMER_NAME", args[2]);
			message = message.replace("PLATFORM", "Twitter");
			event.getChannel().sendMessage(message).queue();
		} else {
			for (TwitterNotifs streamer : results) {
				bot.getManager().deleteTwitterNotifs(streamer);
			}
			String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECT_REMOVE, bot);
			message = message.replace("STREAMER_NAME", args[2]);
			message = message.replace("PLATFORM", "Twitter");
			event.getChannel().sendMessage(message).queue();
		}
		TwitterListener.getInstance().refresh();
	}

	private void doTwitch(MessageReceivedEvent event, String[] args, Bot bot) {
		List<TwitchNotifs> results = bot.getManager().getStreamer(event.getGuild().getIdLong(), args[2]);

		if (results.isEmpty()) {
			TwitchNotifs insert = new TwitchNotifs(args[2], event.getGuild().getIdLong(), false);
			bot.getManager().persistTwitchNotifs(insert);
			TwitchDaemon.getInstance().subscribeToStreamer(args[2], event.getGuild().getIdLong(), bot);

			String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECT_ADD, bot);
			message = message.replace("STREAMER_NAME", args[2]);
			message = message.replace("PLATFORM", "Twitch");
			event.getChannel().sendMessage(message).queue();
		} else {
			for (TwitchNotifs streamer : results) {
				bot.getManager().deleteTwitchNotifs(streamer);
			}
			String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECT_REMOVE, bot);
			message = message.replace("STREAMER_NAME", args[2]);
			message = message.replace("PLATFORM", "Twitch");
			event.getChannel().sendMessage(message).queue();
		}
	}

	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECT_ERROR, bot);
		message = message.replace("PLATFORMS", GeneralConstants.PLATFORMS);
		event.getChannel().sendMessage(message).queue();
	}

	@Override
	public String getInfo() {
		return "Connect a social media channel";
	}

}
