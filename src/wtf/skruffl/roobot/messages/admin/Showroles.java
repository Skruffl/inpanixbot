package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Showroles implements MessageHandler {
	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		if (args.length < 6) {
			throw new InvalidMessageException();
		}

		Role announcementRole;
		Role defaultRole;
		Role removalRole;

		if (event.getMessage().getMentionedRoles().size() == 2) {
			announcementRole = event.getMessage().getMentionedRoles().get(0);
			defaultRole = event.getMessage().getMentionedRoles().get(1);
			removalRole = event.getMessage().getMentionedRoles().get(2);
		} else {
			announcementRole = event.getGuild().getRolesByName(args[3], true).get(0);
			defaultRole = event.getGuild().getRolesByName(args[4], true).get(0);
			removalRole = event.getGuild().getRolesByName(args[5], true).get(0);
		}

		if (announcementRole == null || defaultRole == null || removalRole == null) {
			throw new InvalidMessageException();
		}

		MessageEmbed embed = new EmbedBuilder().setColor(0x565656)
				.setDescription("Du bist beim letzten Schritt angekommen.\nMit der " + announcementRole.getAsMention()
						+ " werden dir in Zukunft die Streams unserer Streamer angekündigt.\nKlicke auf das " + args[1]
						+ ", um die Rolle zu erhalten.\nKlicke auf das " + args[2] + ", wenn kein Interesse besteht.")
				.setFooter("Schritt: (3/3)", null).build();
		Message msg = new MessageBuilder(embed).build();
		event.getChannel().sendMessage(msg).queue(message -> { // NOSONAR
			event.getChannel().addReactionById(event.getChannel().getLatestMessageId(), args[1]).queue();
			event.getChannel().addReactionById(event.getChannel().getLatestMessageId(), args[2]).queue();

			ConnectionManager manager = bot.getConnectionManager();
			// Storing embed message id in case sa new visible streamer is added
			GuildSettings settings = new GuildSettings(event.getGuild().getIdLong(), Setting.DYNAMIC_TWITCH_EMBED,
					event.getChannel().getLatestMessageId());
			manager.persistGuildSetting(settings);

			// Adding the default role for both reactions
			LoginSequence seq1 = new LoginSequence(event.getGuild().getIdLong(), message.getIdLong(),
					defaultRole.getIdLong(), removalRole.getIdLong(), args[2], false);
			LoginSequence seq2 = new LoginSequence(event.getGuild().getIdLong(), message.getIdLong(),
					defaultRole.getIdLong(), removalRole.getIdLong(), args[1], false);
			manager.persistLoginSequence(seq1);
			manager.persistLoginSequence(seq2);

			// Adding additional role for stream announcements
			LoginSequence seq3 = new LoginSequence(event.getGuild().getIdLong(), message.getIdLong(),
					announcementRole.getIdLong(), removalRole.getIdLong(), args[1], false);
			manager.persistLoginSequence(seq3);
		});
	}

	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		event.getChannel()
				.sendMessage("Bitte gib' 2 reaction emojis (ausschliesslich unicode) an und pinge 2 Rollen!\n"
						+ "Alternativ kannst du auch 2 Reaction Emojis und die Namen von 2 Rollen angeben!\n\n"
						+ "Syntax: ```" + bot.getConfig().getBotPrefix() + "Showroles " + "Reaction Add:[Emoji] "
						+ "Reaction No Role:[Emoji] " + "Announcement Role:[Role Name/Role Mention] "
						+ "Player Role:[Role Name/Role Mention] " + "Removal Role:[Role Name/Role Mention]```")
				.queue();
	}

	@Override
	public String getInfo() {
		return "inpanix embed";
	}

}
