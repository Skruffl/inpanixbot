package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Showsupport implements MessageHandler {

	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		if (args.length < 4) {
			throw new InvalidMessageException();
		}

		Role newRole;
		Role removalRole;

		if (event.getMessage().getMentionedRoles().size() == 2) {
			newRole = event.getMessage().getMentionedRoles().get(0);
			removalRole = event.getMessage().getMentionedRoles().get(1);
		} else {
			newRole = event.getGuild().getRolesByName(args[2], true).get(0);
			removalRole = event.getGuild().getRolesByName(args[3], true).get(0);
		}

		if (newRole == null || removalRole == null) {
			throw new InvalidMessageException();
		}

		MessageEmbed embed = MessageUtils.buildSupportEmbed();
		Message msg = new MessageBuilder(embed).build();
		event.getChannel().sendMessage(msg).queue(message -> {
			event.getChannel().addReactionById(event.getChannel().getLatestMessageId(), args[1]).queue();
			ConnectionManager manager = bot.getConnectionManager();
			GuildSettings twitchEmbed = new GuildSettings(event.getGuild().getIdLong(), Setting.DYNAMIC_TWITCH_EMBED,
					message.getId());
			manager.persistGuildSetting(twitchEmbed);

			LoginSequence seq = new LoginSequence(event.getGuild().getIdLong(), message.getIdLong(),
					newRole.getIdLong(), removalRole.getIdLong(), args[1], false);
			manager.persistLoginSequence(seq);

			GuildSettings infoChannelId = new GuildSettings(event.getGuild().getIdLong(), Setting.INFO_CHANNEL_ID,
					event.getChannel().getId());
			manager.deleteGuildSetting(infoChannelId);
		});

	}

	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		event.getChannel().sendMessage("Please enter a reaction emoji (unicode only) and mention 2 roles!\n"
				+ "Alternatively enter a reaction emoji and 2 names of roles!\n\n" + "Syntax: ```"
				+ bot.getConfig().getBotPrefix()
				+ "ShowSupport Reaction:[Emoji] new Role:[Role Name/Role Mention] Removal Role:[Role Name/Role Mention]```")
				.queue();
	}

	@Override
	public String getInfo() {
		return "inpanix embed";
	}

}
