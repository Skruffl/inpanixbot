package wtf.skruffl.roobot.messages.admin;

import java.util.List;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.GeneralUtils;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Setguildsetting implements MessageHandler {

    private MessageReceivedEvent event;

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        this.event = event;
        if (args.length < 3) {
            throw new InvalidMessageException();
        }

        Setting setting = getSetting(args);

        String value = getValue(args);

        if (setting == null) {
            throw new InvalidMessageException();
        }

        String databaseValue = calculateValue(value, setting);
        GuildSettings settings = new GuildSettings(event.getGuild().getIdLong(), setting, databaseValue);

        ConnectionManager connectionManager = bot.getConnectionManager();
        
        //Check if this setting already exists
        List<GuildSettings> databaseSettings = connectionManager.getGuildSetting(setting, event.getGuild().getIdLong());
        if(databaseSettings.isEmpty()) {
            //Just add it if it doesn't
            connectionManager.persistGuildSetting(settings);
        } else {
            //Edit exisiting setting 
            GuildSettings stng = databaseSettings.get(0);
            stng.setValue(databaseValue);
            connectionManager.persistGuildSetting(stng);

            //Delete overflowing settings, since they wouldn't ever be used, anyways
            for(int i = 1; i < databaseSettings.size(); i++) {
                connectionManager.deleteGuildSetting(databaseSettings.get(i));
            }
        }
        
        //Send message
        String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.SET_GUILD_SETTING_SUCCESS, bot);
        message = message.replace("VALUE", getMentionableValue(databaseValue, setting));
        message = message.replace("SETTING", setting.toString());
        event.getChannel().sendMessage(message).queue();
    }

    private String calculateValue(String value, Setting setting) {
        switch (setting.getType()) {
        case CATEGORY:
            return calculateByCategory(value);
        case TEXT_CHANNEL:
            return calculateByTextChannel(value);
        case VOICE_CHANNEL:
            return calculateByVoiceChannel(value);
        case MESSAGE:
            return calculateByMessage(value);
        case ROLE:
            return calculateByRole(value);
        case STRING:
            return value;
        case NUMBER:
            if (!GeneralUtils.isNumeric(value)) {
                throw new InvalidMessageException();
            }
            return value;
        default:
            throw new InvalidMessageException();
        }
    }

    private String getMentionableValue(String value, Setting setting) {
        switch (setting.getType()) {
        case CATEGORY:
            return event.getGuild().getCategoryById(value).getName();
        case TEXT_CHANNEL:
            return new StringBuilder("<#").append(value).append(">").toString();
        case VOICE_CHANNEL:
            return event.getGuild().getVoiceChannelById(value).getName();
        case MESSAGE:
            return value;
        case ROLE:
            return event.getGuild().getRoleById(value).getName();
        case STRING:
        case NUMBER:
            return value;
        default:
            throw new InvalidMessageException();
        }
    }

    private String calculateByRole(String value) {
        if (GeneralUtils.isNumeric(value)) {
            if (event.getGuild().getRoleById(value) != null) {
                return value;
            }
            throw new InvalidMessageException();
        } else if (value.startsWith("<@&") && value.endsWith(">")) {
            return value.replace("<@&", "").replace(">", "");
        } else {
            return event.getGuild().getRolesByName(value, true).get(0).getId();
        }
    }

    private String calculateByVoiceChannel(String value) {
        if (GeneralUtils.isNumeric(value)) {
            if (event.getGuild().getVoiceChannelById(value) != null) {
                return value;
            }
            throw new InvalidMessageException();
        } else if (value.startsWith("<#") && value.endsWith(">")) {
            return value.replace("<#", "").replace(">", "");
        } else {
            return event.getGuild().getVoiceChannelsByName(value, true).get(0).getId();
        }
    }

    private String calculateByMessage(String value) {
        if (GeneralUtils.isNumeric(value)) {
            return value;
        } else if (value.isEmpty()) {
            // Fetches the message before the message calling this command
            return event.getChannel().getHistoryBefore(event.getChannel().getLatestMessageIdLong(), 1).complete()
                    .getRetrievedHistory().get(0).getId();
        } else {
            throw new InvalidMessageException();
        }
    }

    private String calculateByTextChannel(String value) {
        if (GeneralUtils.isNumeric(value)) {
            if (event.getGuild().getTextChannelById(value) != null) {
                return value;
            }
            throw new InvalidMessageException();
        } else if (value.startsWith("<#") && value.endsWith(">")) {
            return value.replace("<#", "").replace(">", "");
        } else {
            return event.getGuild().getTextChannelsByName(value, true).get(0).getId();
        }
    }

    private String calculateByCategory(String value) {
        if (GeneralUtils.isNumeric(value)) {
            if (event.getGuild().getCategoryById(value) != null) {
                return value;
            }
            throw new InvalidMessageException();
        } else {
            return event.getGuild().getCategoriesByName(value, true).get(0).getId();
        }
    }

    private String getValue(String[] args) {
        StringBuilder builder = new StringBuilder();
        for (int i = 2; i < args.length; i++) {
            builder.append(args[i]).append(" ");
        }
        return builder.toString().trim();
    }

    private Setting getSetting(String[] args) {
        for (Setting i : Setting.values()) {
            if (args[1].toLowerCase().trim().contentEquals(i.toString().toLowerCase())) {
                return i;
            }
        }
        for (Setting i : Setting.values()) {
            if (args[1].toLowerCase().trim().contentEquals(i.name().toLowerCase())) {
                return i;
            }
        }
        return null;
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        StringBuilder builder = new StringBuilder();
        for (Setting setting : Setting.values()) {
            builder.append(setting.toString() + "  ");
        }

        String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.SET_GUILD_SETTING_ERROR, bot);
        message = message.replace("PREFIX", bot.getConfig().getBotPrefix());
        message = message.replace("POSSIBLE_SETTINGS", builder.toString());
        event.getChannel().sendMessage(message).queue();
    }

    @Override
    public String getInfo() {
        return "Used for guild configuration";
    }

}