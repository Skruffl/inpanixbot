package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Warn implements MessageHandler {
    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        if(event.getMessage().getMentionedUsers().size() > 1) {
            throw new InvalidMessageException();
        }
        for(Member member : event.getMessage().getMentionedMembers()) {
            Player.build(member, bot).warn();
        }
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {

    }

    @Override
    public String getInfo() {
        return "Warns a member";
    }
}