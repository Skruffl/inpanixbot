package wtf.skruffl.roobot.messages.admin;

import java.util.StringJoiner;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

/**
 * Mute
 */
public class Mute implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        if (args.length > 2) {
            if (args[1].contentEquals("mute")) {
                mute(event, bot);
            } else {
                unmute(event, bot);
            }
        } else {
            throw new InvalidMessageException();
        }
    }

    private void unmute(MessageReceivedEvent event, Bot bot) {
        StringJoiner joiner = new StringJoiner(", ");
        for (Member member : event.getMessage().getMentionedMembers()) {
            for (TextChannel channel : event.getGuild().getTextChannels()) {
                channel.getPermissionOverride(member).delete().queue();
            }
            joiner.add(member.getUser().getAsMention());

        }
        String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.MUTE_OFF, bot);
        message = message.replace("MUTE_MEMBERS", joiner.toString());
        event.getChannel().sendMessage(message).queue();
    }

    private void mute(MessageReceivedEvent event, Bot bot) {
        StringJoiner joiner = new StringJoiner(", ");
        for (Member member : event.getMessage().getMentionedMembers()) {
            for (TextChannel channel : event.getGuild().getTextChannels()) {
                channel.putPermissionOverride(member).setDeny(Permission.MESSAGE_WRITE, Permission.MESSAGE_ADD_REACTION)
                        .queue();
            }
            joiner.add(member.getUser().getAsMention());
        }
        String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.MUTE_ON, bot);
        message = message.replace("MUTE_MEMBERS", joiner.toString());
        event.getChannel().sendMessage(message).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel().sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.MUTE_ERROR, bot))
                .queue();
    }

    @Override
    public String getInfo() {
        return "Mutes or unmutes a member.";
    }

}