package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Wipe implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        event.getChannel().getIterableHistory().stream().forEach(msg -> msg.delete().queue());
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        // not thrown
    }

    @Override
    public String getInfo() {
        return "Clears all messages from a channel";
    }

}