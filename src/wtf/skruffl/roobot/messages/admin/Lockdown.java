package wtf.skruffl.roobot.messages.admin;

import java.util.ArrayList;
import java.util.List;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.PermissionOverride;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;

public class Lockdown implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        // only works in inpanix
        if (!event.getGuild().getId().contentEquals("456877333245198346")) {
            return;
        }

        Role everyone =  event.getGuild().getRoleById("457491616148488192");

        List<TextChannel> channels = new ArrayList<>();
        channels.add(event.getGuild().getTextChannelById("456882525726310401")); //general chat
        channels.add(event.getGuild().getTextChannelById("457498316830277633")); //looking-for-players

        if (args.length > 1 && args[1].equals("on")) {
            for (TextChannel channel : channels) {
                PermissionOverride permissionOverride = channel.getPermissionOverride(everyone);
                permissionOverride.getManager().deny(Permission.MESSAGE_WRITE).queue();
            }
        } else {
            for (TextChannel channel : channels) {
                PermissionOverride permissionOverride = channel.getPermissionOverride(everyone);
                permissionOverride.getManager().grant(Permission.MESSAGE_WRITE).queue();
            }
        }
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel().sendMessage("what").queue();
    }

    @Override
    public String getInfo() {
        return "Puts the given server into lockdown";
    }

}