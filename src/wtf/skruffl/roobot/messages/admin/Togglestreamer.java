package wtf.skruffl.roobot.messages.admin;

import java.util.List;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Togglestreamer implements MessageHandler {

	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		if (args.length > 1) {
			ConnectionManager manager = bot.getConnectionManager();
			List<TwitchNotifs> result = manager.getStreamer(event.getGuild().getIdLong(), args[1]);

			boolean visible = false;

			for (TwitchNotifs notif : result) {
				visible = !notif.isVisible();
				notif.setVisible(visible);
				manager.persistTwitchNotifs(notif);
			}

			List<GuildSettings> twitchEmbed = manager.getGuildSetting(Setting.DYNAMIC_TWITCH_EMBED,
					event.getGuild().getIdLong());
			Long dynamicTwitchEmbed = Long.parseLong(twitchEmbed.get(0).getValue());

			List<GuildSettings> infoChannel = manager.getGuildSetting(Setting.INFO_CHANNEL_ID,
					event.getGuild().getIdLong());
			Long infoChannelId = Long.parseLong(infoChannel.get(0).getValue());

			// Update Message
			event.getGuild().getTextChannelById(infoChannelId).getHistoryAround(dynamicTwitchEmbed, 15)
					.queue(messages -> messages.getMessageById(dynamicTwitchEmbed)
							.editMessage(MessageUtils.buildSupportEmbed()).queue());

			String message;
			if(visible) {
				message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.TOGGLE_STREAMER_VISIBLE, bot);
			} else {
				message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.TOGGLE_STREAMER_INVISIBLE, bot);
			}
			message = message.replace("STREAMER_NAME", args[1]);
			event.getChannel().sendMessage(message).queue();
		} else {
			throw new InvalidMessageException();
		}

	}

	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		event.getChannel()
				.sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.TOGGLE_STREAMER_ERROR, bot))
				.queue();
	}

	@Override
	public String getInfo() {
		return "Enables the steamer in the info embed";
	}

}
