package wtf.skruffl.roobot.messages.admin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Sequence implements MessageHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(Sequence.class);

	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {

		if (args.length > 1) {
			switch (args[1].toLowerCase()) {
			case "add":
				add(event, args, bot);
				break;
			case "remove":
				remove(event, args, bot);
				break;

			default:
				throw new InvalidMessageException();
			}
		} else {
			throw new InvalidMessageException();
		}

	}

	private void add(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		/*
		 * Parameter List: 1 - removalRole 2 - newRole 3 - unicodeEmoji [optional] 4 -
		 * messageId Note: if 4th parameter is omitted, the message calling this will be
		 * deleted and the message before that will be used.
		 */

		Long removalRole = null;
		Long newRole = null;
		String unicodeEmoji = null;
		Long messageId = null;

		try {
			removalRole = Long.parseLong(args[2]);
			newRole = Long.parseLong(args[3]);
			unicodeEmoji = args[4];
			if (args.length >= 6) {
				messageId = Long.parseLong(args[5]);
			} else {
				messageId = MessageUtils.getPreviousMessage(event).getIdLong();
			}

		} catch (Exception e) {
			LOGGER.error("Error while parsing parameters.", e);
			throw new InvalidMessageException();
		}
		LoginSequence seq = new LoginSequence(event.getGuild().getIdLong(), messageId, newRole, removalRole,
				unicodeEmoji, false);
		bot.getManager().persistLoginSequence(seq);
	}

	private void remove(MessageReceivedEvent event, String[] args, Bot bot) {
		Long messageId;
		if (args.length >= 2) {
			messageId = Long.parseLong(args[2]);
		} else {
			messageId = MessageUtils.getPreviousMessage(event).getIdLong();
		}

		List<LoginSequence> list = bot.getManager().getSequence(event.getGuild().getIdLong(), messageId, true);
		for (LoginSequence seq : bot.getManager().getSequence(event.getGuild().getIdLong(), messageId, false)) {
			list.add(seq);
		}

		for (LoginSequence seq : list) {
			bot.getManager().deleteLoginSequence(seq);
		}

	}

	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.SEQUENCE_ERROR, bot);
		message = message.replace("PREFIX", bot.getConfig().getBotPrefix());
		event.getChannel().sendMessage(message).queue();
	}

	@Override
	public String getInfo() {
		return "Adds a role-hook to a message";
	}

}
