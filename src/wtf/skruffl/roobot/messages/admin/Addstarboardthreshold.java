package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.StarboardThreshold;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Addstarboardthreshold implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        if (args.length < 3) {
            throw new InvalidMessageException();
        }
        int starThreshold = Integer.parseInt(args[1]);
        String roleName;
        Role role;
        StringBuilder builder = new StringBuilder();
        for (int i = 2; i < args.length; i++) {
            builder.append(args[i]).append(" ");
        }
        roleName = builder.toString();
        try {
            role = event.getGuild().getRolesByName(roleName.trim(), true).get(0);
        } catch (Exception e) {
            throw new InvalidMessageException(e);
        }
        if (role != null) {
            StarboardThreshold threshold = new StarboardThreshold(starThreshold, event.getGuild().getIdLong(),
                    role.getIdLong());
            bot.getManager().persistStarboardThreshold(threshold);

            String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.STARBOARD_THRESHOLD_SUCCESS, bot);
            message = message.replace("ROLE_NAME", role.getName());
            message = message.replace("STAR_THRESHOLD", Integer.toString(starThreshold));
            event.getChannel().sendMessage(message).queue();
        }
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.STARBOARD_THRESHOLD_ERROR, bot);
        message = message.replace("PREFIX", bot.getConfig().getBotPrefix());
        event.getChannel().sendMessage(message).queue();
    }

    @Override
    public String getInfo() {
        return "Give people a role if they reach a certain level";
    }

}