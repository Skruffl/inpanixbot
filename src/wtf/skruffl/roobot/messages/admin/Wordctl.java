package wtf.skruffl.roobot.messages.admin;

import java.time.Instant;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.Word;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;

/**
 * Wordctl
 */
public class Wordctl implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        switch (args.length > 1 ? args[1].toLowerCase() : "") {
        case "list":
            showList(event, bot);
            break;
        case "add":
            if (args.length < 3) {
                throw new InvalidMessageException();
            }
            add(event, args, bot);
            break;
        case "remove":
            if (args.length < 3) {
                throw new InvalidMessageException();
            }
            remove(event, args, bot);
            break;
        default:
            throw new InvalidMessageException();
        }
    }

    private String getWord(String[] args) {
        StringJoiner builder = new StringJoiner(" ");
        for (int i = 2; i < args.length; i++) {
            builder.add(args[i]);
        }
        return builder.toString();
    }

    private void remove(MessageReceivedEvent event, String[] args, Bot bot) {
        String wordValue = getWord(args);
        for (Word word : bot.getConnectionManager().getWordsForGuild(event.getGuild().getIdLong()).stream()
                .filter(word -> word.getWord().contentEquals(wordValue)).collect(Collectors.toList())) {
            bot.getConnectionManager().deleteWord(word);
        }
        String msg = MessageUtils.getMessageTextFor(event.getMember(), MessageType.WORDCTL_REMOVED, bot);
        msg = msg.replace("VALUE", wordValue);
        event.getChannel().sendMessage(msg).queue();
    }

    private void add(MessageReceivedEvent event, String[] args, Bot bot) {
        String wordValue = getWord(args);
        Word word = new Word(event.getGuild(), wordValue);
        bot.getConnectionManager().persistWord(word);
        String msg = MessageUtils.getMessageTextFor(event.getMember(), MessageType.WORDCTL_ADDED, bot);
        msg = msg.replace("VALUE", wordValue);
        event.getChannel().sendMessage(msg).queue();
    }

    private void showList(MessageReceivedEvent event, Bot bot) {
        EmbedBuilder builder = new EmbedBuilder();
        for (Word word : bot.getConnectionManager().getWordsForGuild(event.getGuild().getIdLong())) {
            builder.addField(word.getWord(), EmbedBuilder.ZERO_WIDTH_SPACE, true);
        }
        builder.setDescription(MessageUtils.getMessageTextFor(event.getMember(), MessageType.WORDCTL_LIST, bot));
        builder.setAuthor(event.getGuild().getName(), null, event.getGuild().getIconUrl());
        builder.setTimestamp(Instant.now());
        event.getChannel().sendMessage(new MessageBuilder(builder.build()).build()).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel()
                .sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.WORDCTL_ERROR, bot)).queue();
    }

    @Override
    public String getInfo() {
        return "Controls filter words";
    }

}