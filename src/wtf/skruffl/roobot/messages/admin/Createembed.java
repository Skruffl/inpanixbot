package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Createembed implements MessageHandler {
    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        switch (args[1]) {
        case "1":
            flexStreamer(event);
            break;
        case "2":
            flexRules(event);
            break;
        case "3":
            flexRole(event);
            break;
        case "4":
            mvpSupport(event);
            break;
        case "5":
            mvpRules(event);
            break;
        case "k1":
            k1(event);
            break;
        case "k2":
            k2(event);
            break;
        default:
            throw new InvalidMessageException();
        }
    }

    private void k2(MessageReceivedEvent event) {
    }

    private void k1(MessageReceivedEvent event) {
    }

    private void mvpSupport(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setDescription("Hier könnt ihr uns unterstützen:")
                .addField("<:instagram:556243679820382219>", "https://www.instagram.com/esport.mvp/", false)
                .addField("<:twitter:556216467700449280>", "https://twitter.com/MvpEsport", false)
                .addField("Lade deine Freunde ein!", "<:discord:591254809311576095> https://discord.gg/avAEc8Y", false)
                .setFooter(
                        "Stelle sicher, dass du dir die Regeln durchliest. Um zu diesen zu gelangen, klicke auf die Reaktion.",
                        null);
        event.getChannel().sendMessage(builder.build()).queue();
    }

    private void mvpRules(MessageReceivedEvent event) {
        event.getChannel().sendMessage(new EmbedBuilder()
                .addField("**(1)** Sei Respektvoll! Keine Hassrede, Rassismus, sexuelle Belästigungen und Trolling.",
                        "Persönliche Angriffe, Bedrohungen oder Missbrauch werden nicht toleriert.", false)
                .addField("**(2)** Bitte beginn kein unnötiges Drama.",
                        "Dies beinhaltet extrem hitzige Auseinandersetzungen über Politik, Religion und andere sensible Themen sowie überhetzende Überreaktionen.\nBringe kein Drama mit Bezug zu anderen Servern.",
                        false)
                .addField("**(3)** > Halte Spam auf ein Minimum.", "(Emoji-Spam, Datei-Spam oder Ping-Spam)", false)
                .addField("(4) > Persönliche Informationen",
                        "Du wirst weder öffentlich noch privat die persönlichen Informationen von Mitgliedern dieses Server offenlegen.",
                        false)
                .addField("(5) Identitätswechsel",
                        "Der Identitätswechsel von Mitgliedern dieses Servers ist NICHT erlaubt.", false)
                .addField("(6) Werbung", "Bewerbe deinen Discord-Server hier nicht.", false)
                .addField("(7) Kanäle blockieren", "Halte die Kanäle immer offen.", false)
                .addField("(8) Discord ToS", "Das brechen der Discord Nutzungsbedingungen führt zu einem Bann.", false)
                .addField("(9) Teamrekrutierung", "Wir erlauben keine Teamrekrutierungen in irgendwelchen Kanälen.",
                        false)
                .addField("Verstöße gegen die Regeln führen zu einer angemessenen Bestrafung.",
                        "Klicke die Reaktion um weiter zu kommen", false)
                .build()).queue();
    }

    private void flexStreamer(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setDescription("Hier könnt ihr uns unterstützen");
        builder.addField("SZero",
                "[YouTube](https://www.youtube.com/channel/UCSbu5gkg8U2WV_XJrvR4LNw)\n[Twitch](https://www.twitch.tv/flexz_szero)",
                false);
        builder.addField("Matrix",
                "[YouTube](https://www.youtube.com/channel/UCimb7yEB8w9QNiDzSTYn4Dw)\n[Twitch](https://www.twitch.tv/flexz_matrix)",
                false);
        builder.addField("Klypax", "[Twitch](https://www.twitch.tv/klypax)", false);
        builder.addField("Rexon", "[Twitch](https://www.twitch.tv/rexoneu)", false);
        builder.addField("Squishy", "[Twitch](https://www.twitch.tv/flexz_squishy)", false);
        builder.addField("Du kannst auch gerne unseren Discord an deine Freunde teilen", "https://discord.gg/UCde2nB",
                false);
        builder.addField("Klicke die Reaktion um zum nächsten Schritt zu gelangen.", "✅", false);
        builder.setFooter("Schritt 1/3", null);

        event.getChannel().sendMessage(builder.build()).queue();
    }

    private void flexRules(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setDescription("☞Auf unserem Discord sind paar Regel einzuhalten☜");
        builder.addField("#1", "Kein Spam in jeglicher Form", false);
        builder.addField("#2", "Keine Eigenwerbung", false);
        builder.addField("#3", "Keine Beleidigungen in jeglicher Form", false);
        builder.addField("#4", "Keine Caps beim schreiben benutzen", false);
        builder.addField("#5", "Kein sinnloses Kanal switchen", false);
        builder.addField("#6", "Keine Teamrekrutierungen in irgendwelche Channel", false);
        builder.addField("Verstöße gegen die Regeln führen zu einer angemessenen Bestrafung!",
                "Sollten dennoch Fragen aufkommen oder gibt es Probleme mit anderen Personen, dann wendet euch bitte an den Owner.",
                false);
        builder.setFooter("Schritt 2/3", null);

        event.getChannel().sendMessage(builder.build()).queue();
    }

    private void flexRole(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setDescription("Wir werden in Zukunft öfter custom games hosten :ok_hand:");
        builder.addField("Du willst mitmachen?", "Klicke auf das :heavy_check_mark:", false);
        builder.addField("Kein Interesse?", "Klicke auf das :x:", false);
        builder.addField("Willst du Stream Benachrichtigungen?", "Klicke auf das 🎦", false);
        builder.setFooter("Schritt 3/3", null);

        event.getChannel().sendMessage(builder.build()).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel().sendMessage("1, 2, 3, 4 or 5 are valid thingies aaaa").queue();
    }

    @Override
    public String getInfo() {
        return "TPPI";
    }
}