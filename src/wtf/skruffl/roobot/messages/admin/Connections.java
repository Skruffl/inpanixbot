package wtf.skruffl.roobot.messages.admin;

import java.util.List;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.TwitchNotifs;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Connections implements MessageHandler {
	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		List<TwitchNotifs> list = bot.getManager().getGuildStreamers(event.getGuild().getIdLong());

		String header = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECTIONS_HEADER, bot);
		String visible = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECTIONS_VISIBLE, bot);
		String notVisible = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CONNECTIONS_IS_NOT_VISIBLE, bot);
		EmbedBuilder builder = new EmbedBuilder().setDescription(header);
		for (TwitchNotifs row : list) {
			builder.addField((String) row.getChannelName(), row.isVisible() ? visible : notVisible, true);
		}
		event.getChannel().sendMessage(builder.build()).queue();
	}

	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		// not thrown
	}

	@Override
	public String getInfo() {
		return "Shows the currently connected social media channels";
	}

}
