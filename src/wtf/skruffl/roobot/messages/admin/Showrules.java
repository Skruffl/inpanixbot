package wtf.skruffl.roobot.messages.admin;

import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.LoginSequence;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.MessageHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Showrules implements MessageHandler {

	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
		if (args.length < 4) {
			throw new InvalidMessageException();
		}

		Role newRole;
		Role removalRole;

		if (event.getMessage().getMentionedRoles().size() == 2) {
			newRole = event.getMessage().getMentionedRoles().get(0);
			removalRole = event.getMessage().getMentionedRoles().get(1);
		} else {
			newRole = event.getGuild().getRolesByName(args[2], true).get(0);
			removalRole = event.getGuild().getRolesByName(args[3], true).get(0);
		}

		if (newRole == null || removalRole == null) {
			throw new InvalidMessageException();
		}

		event.getMessage().delete().queue();

		event.getChannel().sendMessage(new EmbedBuilder()
				.addField("**(1)** Sei Respektvoll! Keine Hassrede, Rassismus, sexuelle Belästigungen und Trolling.",
						"Persönliche Angriffe, Bedrohungen oder Missbrauch werden nicht toleriert.", false)
				.addField("**(2)** Bitte beginn kein unnötiges Drama.",
						"Dies beinhaltet extrem hitzige Auseinandersetzungen über Politik, Religion und andere sensible Themen sowie überhetzende Überreaktionen.\nBringe kein Drama mit Bezug zu anderen Servern.",
						false)
				.addField("**(3)** > Halte Spam auf ein Minimum.", "(Emoji-Spam, Datei-Spam oder Ping-Spam)", false)
				.addField("(4) > Persönliche Informationen",
						"Du wirst weder öffentlich noch privat die persönlichen Informationen von Mitgliedern dieses Server offenlegen.",
						false)
				.addField("(5) Identitätswechsel",
						"Der Identitätswechsel von Mitgliedern dieses Servers ist NICHT erlaubt.", false)
				.addField("(6) Werbung", "Bewerbe deinen Discord-Server hier nicht.", false)
				.addField("(7) Kanäle blockieren", "Halte die Kanäle immer offen.", false)
				.addField("(8) Discord ToS", "Das brechen der Discord Nutzungsbedingungen führt zu einem Bann.", false)
				.addField("(9) Teamrekrutierung", "Wir erlauben keine Teamrekrutierungen in irgendwelchen Kanälen.",
						false)
				.addField("Verstöße gegen die Regeln führen zu einer angemessenen Bestrafung.", "Schritt: (2/3)", false)
				.build()).queue(message -> {
					event.getChannel().addReactionById(event.getChannel().getLatestMessageId(), args[1]).queue();
					ConnectionManager manager = bot.getConnectionManager();
					LoginSequence seq = new LoginSequence(event.getGuild().getIdLong(), message.getIdLong(),
							newRole.getIdLong(), removalRole.getIdLong(), args[1], false);
					manager.persistLoginSequence(seq);

				});

	}

	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		event.getChannel().sendMessage("Bitte gib ein Reaction Emoji (ausschliesslich unicode) und pinge 2 Rollen!\n"
				+ "Alternativ kannst du ein Emoji und die Namen von 2 Rollen angeben!\n\n" + "Syntax: ```"
				+ bot.getConfig().getBotPrefix()
				+ "Showrules Reaction:[Emoji] new Role:[Role Name/Role Mention] Removal Role:[Role Name/Role Mention]```")
				.queue();
	}

	@Override
	public String getInfo() {
		return "inpanix embed";
	}

}
