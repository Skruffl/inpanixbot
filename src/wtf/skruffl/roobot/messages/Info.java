package wtf.skruffl.roobot.messages;

import java.time.Instant;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.Language;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Info implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        Message msg;
        if (args.length <= 1) {
            msg = server(event.getGuild(), bot);
        } else if (args.length == 2) {
            if (args[1].equalsIgnoreCase("user")) {
                msg = user(event.getMember(), bot);
            } else if (args[1].equalsIgnoreCase("server")) {
                msg = server(event.getGuild(), bot);
            } else {
                throw new InvalidMessageException();
            }
        } else if (args[1].equalsIgnoreCase("user")) {
            for (Member member : event.getMessage().getMentionedMembers()) {
                msg = user(member, bot);
                event.getChannel().sendMessage(msg).queue();
            }
            return;
        } else {
            throw new InvalidMessageException();
        }
        event.getChannel().sendMessage(msg).queue();
    }

    private Message server(Guild guild, Bot bot) {
        EmbedBuilder builder = new EmbedBuilder();

        Language lang = MessageUtils.getGuildLanguage(guild, bot);

        String guildInfo = lang.getINFO_CARD();
        guildInfo = guildInfo.replace("NAME", guild.getName());

        builder.setAuthor(guildInfo, null, guild.getIconUrl());
        builder.addField(lang.getINFO_CREATION_DATE(), guild.getTimeCreated().toString(), false);
        builder.addField(lang.getINFO_MEMBERS(), Integer.toString(guild.getMembers().size()), true);
        builder.addField(lang.getINFO_BANS(), Integer.toString(guild.retrieveBanList().complete().size()), true);
        builder.setTimestamp(Instant.now());

        return new MessageBuilder().setEmbed(builder.build()).build();
    }

    private Message user(Member member, Bot bot) {
        User user = member.getUser();

        String userInfo = MessageUtils.getMessageTextFor(member, MessageType.INFO_CARD, bot);
        userInfo = userInfo.replace("NAME", user.getAsTag());

        EmbedBuilder builder = new EmbedBuilder();
        builder.setAuthor(userInfo, null, user.getAvatarUrl());
        builder.addField(MessageUtils.getMessageTextFor(member, MessageType.INFO_CREATION_DATE, bot),
                user.getTimeCreated().toString(), false);
        builder.addField(MessageUtils.getMessageTextFor(member, MessageType.INFO_JOIN_DATE, bot),
                member.getTimeJoined().toString(), false);
        builder.setColor(member.getColor());
        builder.setTimestamp(Instant.now());

        return new MessageBuilder().setEmbed(builder.build()).build();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        // not thrown
    }

    @Override
    public String getInfo() {
        return "Show info about the server or yourself";
    }

}