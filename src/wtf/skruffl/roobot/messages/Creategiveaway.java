package wtf.skruffl.roobot.messages;

import java.time.Instant;
import java.util.List;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.Giveaway;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Creategiveaway implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        if (MessageUtils.hasAdminPerms(event, bot)) {
            adminGiveaway(args, event, bot);
        } else {
            userGiveaway(args, event, bot);
        }
    }

    private void userGiveaway(String[] args, MessageReceivedEvent event, Bot bot) throws InvalidMessageException {
        ConnectionManager manager = bot.getConnectionManager();
        Player player = Player.build(event.getMember(), bot);
        long xp = 0;
        if (args.length < 4) {
            throw new InvalidMessageException();
        }
        try {
            xp = Long.parseLong(args[3]);
        } catch (NumberFormatException e) {
            new InvalidMessageException();
        }

        if (player.getXpCount() < xp) {
            String message = MessageUtils.getMessageTextFor(event.getMember(),
                    MessageType.CREATE_GIVEAWAY_NOT_ENOUGH_XP, bot);
            event.getChannel().sendMessage(message).queue();
            return;
        }

        List<GuildSettings> setting = manager.getGuildSetting(Setting.USER_GIVEAWAY_CHANNEL,
                event.getGuild().getIdLong());
        if (setting.isEmpty()) {
            noChannel("User", event, bot);
            return;
        }

        TextChannel channel = event.getGuild().getTextChannelById(setting.get(0).getValue());
        if (channel == null) {
            noChannel("User", event, bot);
            return;
        }
        long minusXp = giveaway(args, channel, event, bot);
        player.removeXp(minusXp);
    }

    private void adminGiveaway(String[] args, MessageReceivedEvent event, Bot bot) throws InvalidMessageException {
        ConnectionManager manager = bot.getConnectionManager();

        List<GuildSettings> setting = manager.getGuildSetting(Setting.ADMIN_GIVEAWAY_CHANNEL,
                event.getGuild().getIdLong());
        if (setting.isEmpty()) {
            noChannel("Admin", event, bot);
            return;
        }

        TextChannel channel = event.getGuild().getTextChannelById(setting.get(0).getValue());
        if (channel == null) {
            noChannel("Admin", event, bot);
            return;
        }
        giveaway(args, channel, event, bot);
    }

    private long giveaway(String[] args, TextChannel channel, MessageReceivedEvent event, Bot bot) throws InvalidMessageException {
        if (args.length < 4) {
            throw new InvalidMessageException();
        }
        try {
            Long xp = Long.parseLong(args[3]);
            Long endTime = parseTimeString(args[1]);
            String emoji = args[2];
            String giveawayText = "";
            for (int i = 4; i < args.length; i++) {
                giveawayText = giveawayText.concat(" " + args[i]);
            }

            String endTimeText = MessageUtils.getMessageTextFor(event.getMember(),
                    MessageType.CREATE_GIVEAWAY_END_TIME, bot);
            String xpReward = MessageUtils.getMessageTextFor(event.getMember(), MessageType.CREATE_GIVEAWAY_XP_REWARD, bot);
            EmbedBuilder builder = new EmbedBuilder()
                    .setAuthor(event.getAuthor().getName(), null, event.getAuthor().getAvatarUrl())
                    .setTimestamp(Instant.ofEpochMilli(endTime)).setFooter(endTimeText, null)
                    .setDescription(giveawayText);
            if (xp > 0) {
                builder.addField(xpReward, xp.toString(), false);
            }
            channel.sendMessage(builder.build()).queue(msg -> {
                msg.addReaction(emoji).queue();
                Giveaway ga = new Giveaway(event.getGuild().getIdLong(), channel.getIdLong(), msg.getIdLong(), endTime,
                        emoji, xp);
                bot.getGiveaways().addGiveaway(ga);
                bot.getManager().persistGiveaway(ga);
            });
            return xp;
        } catch (NumberFormatException e) {
            throw new InvalidMessageException(e);
        }
    }

    private void noChannel(String type, MessageReceivedEvent event, Bot bot) {
        String message = MessageUtils.getMessageTextFor(event.getMember(),
                MessageType.CREATE_GIVEAWAY_NO_GIVEAWAY_CHANNEL, bot);
        message = message.replace("TYPE", type);
        event.getChannel().sendMessage(message).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel()
                .sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.CREATE_GIVEAWAY_ERROR, bot))
                .queue();
    }

    private Long parseTimeString(String timeString) throws InvalidMessageException {
        Integer timeAmt;
        String unit;
        try {
            unit = timeString.substring(timeString.length() - 1);
            timeAmt = Integer.parseInt(timeString.substring(0, timeString.length() - 1));
            switch (unit.toLowerCase()) {
            case "s":
                return System.currentTimeMillis() + (timeAmt * 1000l);
            case "m":
                return System.currentTimeMillis() + ((timeAmt * 1000l) * 60l);
            case "h":
                return System.currentTimeMillis() + (((timeAmt * 1000l) * 60l) * 60l);
            case "d":
                return System.currentTimeMillis() + ((((timeAmt * 1000l) * 60l) * 60l) * 24l);
            case "w":
                return System.currentTimeMillis() + (((((timeAmt * 1000l) * 60l) * 60l) * 24l) * 7l);
            default:
                throw new InvalidMessageException();
            }
        } catch (NumberFormatException e) {
            throw new InvalidMessageException(e);
        }
    }

    @Override
    public String getInfo() {
        return "Creates a new giveaway";
    }

}