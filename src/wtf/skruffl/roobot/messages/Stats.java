package wtf.skruffl.roobot.messages;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Stats implements MessageHandler {
    private MessageReceivedEvent event;

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        this.event = event;
        if (event.getMessage().getMentionedMembers().isEmpty()) {
            MessageEmbed embed = buildEmbedForMember(event.getMember(), bot);
            event.getChannel().sendMessage(embed).queue();
        } else if (event.getMessage().getMentionedMembers().size() < 5) {
            for (Member member : event.getMessage().getMentionedMembers()) {
                MessageEmbed embed = buildEmbedForMember(member, bot);
                event.getChannel().sendMessage(embed).queue();
            }
        } else {
            throw new InvalidMessageException();
        }
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel()
                .sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.STATS_TOO_MANY_PEOPLE, bot))
                .queue();
    }

    private MessageEmbed buildEmbedForMember(Member member, Bot bot) {
        String xp = MessageUtils.getMessageTextFor(event.getMember(), MessageType.STATS_XP, bot);
        String level = MessageUtils.getMessageTextFor(event.getMember(), MessageType.STATS_LEVEL, bot);
        String starboardMessages = MessageUtils.getMessageTextFor(event.getMember(),
                MessageType.STATS_STARBOARD_MESSAGES, bot);

        Player player = Player.build(member, bot);
        EmbedBuilder builder = new EmbedBuilder();
        builder.setAuthor(member.getUser().getName(), null, member.getUser().getAvatarUrl())
                .addField(xp, player.getXpCount().toString(), true).addField(level, player.getLevel().toString(), true)
                .addField(starboardMessages, player.getStarboardMessages().toString(), true);
        return builder.build();
    }

    @Override
    public String getInfo() {
        return "Shows your XP";
    }

}