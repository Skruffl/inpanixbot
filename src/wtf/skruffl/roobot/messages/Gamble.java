package wtf.skruffl.roobot.messages;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.db.pojo.Player;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Gamble implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        if (args.length < 2) {
            throw new InvalidMessageException();
        }
        if (checkForGamblingChannel(event,bot)) {
            return;
        }

        Member member = event.getMember();
        List<Player> list = bot.getManager().getPlayer(member.getGuild().getIdLong(), member.getUser().getIdLong());
        if (!list.isEmpty()) {
            Player player = list.get(0);
            try {
                Long gambleAmount;
                if (args[1].contains("all")) {
                    gambleAmount = player.getXpCount();
                } else {
                    gambleAmount = Long.parseLong(args[1]);
                }
                if (player.getXpCount() > 1 && player.getXpCount() >= gambleAmount) {
                    long xp = calculateXp(gambleAmount, event, bot);
                    sleep();
                    player = bot.getManager().getPlayerById(player.getId());
                    player.addXp(xp);
                } else {
                    event.getChannel()
                            .sendMessage(MessageUtils.getMessageTextFor(member, MessageType.GAMBLE_NOT_ENOUGH_XP, bot))
                            .queue();
                }
            } catch (NumberFormatException e) {
                throw new InvalidMessageException(e);
            }
        }
    }

    private boolean checkForGamblingChannel(MessageReceivedEvent event, Bot bot) {
        List<GuildSettings> gambleChannels = bot.getManager().getGuildSetting(Setting.GAMBLING_CHANNEL,
                event.getGuild().getIdLong());
        if (!gambleChannels.isEmpty()) {
            String gamblingChannelId = gambleChannels.get(0).getValue();
            if (!event.getChannel().getId().contentEquals(gamblingChannelId)) {
                String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_INVALID_CHANNEL, bot);
                message = message.replace("GAMBLE_CHANNEL", "<#" + gamblingChannelId + ">");
                message = message.replace("USER_MENTION", event.getAuthor().getAsMention());
                event.getChannel().sendMessage(message).queue(msg -> {
                    try {
                        Thread.sleep(3000);
                        msg.delete().queue();
                    } catch (InterruptedException e) { // NOSONAR
                        // nuffin
                    }
                });
                return true;
            }
        }
        return false;
    }

    private long calculateXp(Long gambleAmount, MessageReceivedEvent event, Bot bot) {
        long minXp = 0;
        List<GuildSettings> settings = bot.getManager().getGuildSetting(Setting.GAMBLING_MIN_AMOUNT,
                event.getGuild().getIdLong());
        if (!settings.isEmpty()) {
            minXp = Long.parseLong(settings.get(0).getValue());
        }
        if (gambleAmount <= minXp) {
            String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_BELOW_THRESHOLD, bot);
            message = message.replace("USER_MENTION", event.getMember().getAsMention());
            message = message.replace("THRESHOLD", Long.toString(minXp));
            event.getChannel().sendMessage(message).queue();
            return 0;
        }
        Integer randomNumber = ThreadLocalRandom.current().nextInt(1, 101);
        if (randomNumber <= 2) {
            /*
             * 5x
             */
            String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_FIVE_TIMES, bot);
            sendMessage(gambleAmount, event, message);
            return gambleAmount * 5l;

        } else if (randomNumber <= 10) {
            /**
             * 3x
             */
            String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_THREE_TIMES, bot);
            sendMessage(gambleAmount, event, message);
            return gambleAmount * 3l;

        } else if (randomNumber <= 40) {
            /**
             * 2x
             */
            String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_DOUBLED, bot);
            sendMessage(gambleAmount, event, message);
            return gambleAmount * 2l;

        } else {
            /**
             * LOSS
             */
            String message = MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_LOSS, bot);
            sendMessage(gambleAmount, event, message);
            return gambleAmount * -1l;
        }
    }

    private void sendMessage(Long gambleAmount, MessageReceivedEvent event, String message) {
        message = message.replace("USER_MENTION", event.getMember().getAsMention());
        message = message.replace("AMOUNT", Long.toString(gambleAmount));
        event.getChannel().sendMessage(message).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        event.getChannel().sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.GAMBLE_ERROR, bot))
                .queue();
    }

    private void sleep() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public String getInfo() {
        return "Gamble with our XP";
    }

}