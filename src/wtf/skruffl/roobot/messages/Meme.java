package wtf.skruffl.roobot.messages;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;

/**
 * Meme
 */
@Slf4j
public class Meme implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        String msg = event.getMessage().getContentRaw();
        // remove prefix and class name
        msg = msg.substring(bot.getConfig().getBotPrefix().length() + getClass().getSimpleName().length());
        String[] texts = msg.split("\\|");
        for (int i = 0; i < texts.length; i++) {
            texts[i] = texts[i].trim();
        }
        event.getMessage().getAttachments().get(0).retrieveInputStream().thenAccept(in -> {
            try {
                BufferedImage img = ImageIO.read(in);
                String fileName = event.getMessage().getAttachments().get(0).getFileName();
                String imgType = fileName.substring(fileName.lastIndexOf(".") + 1);

                byte[] byteArray = renderText(texts, img, imgType);
                event.getChannel().sendFile(byteArray, "meme." + imgType).queue();
            } catch (IOException e) {
                log.error("image exception", e);
            }
        });
    }

    private byte[] renderText(String[] texts, BufferedImage img, String imgType) throws IOException {
        Graphics g = img.getGraphics();
        Font font = new Font("impact", Font.PLAIN, (img.getWidth() + img.getHeight()) / 100 * 5);
        FontMetrics metrics = g.getFontMetrics(font);
        int imageWidth = img.getWidth() / 2;
        int padding = (img.getHeight() / 100) * 4;

        Graphics2D g2d = (Graphics2D) g;
        AffineTransform transform = g2d.getTransform();
        g2d.setFont(font);
        FontRenderContext frc = g2d.getFontRenderContext();

        Stroke stroke = new BasicStroke();
        GlyphVector gv = font.createGlyphVector(frc, texts[0]);
        Shape shape = gv.getOutline();
        shape = stroke.createStrokedShape(shape);
        int textHeight = Double.valueOf(shape.getBounds().getHeight() / 100 * 90).intValue();
        g2d.translate(imageWidth - metrics.stringWidth(texts[0]) / 2, textHeight + padding);
        g2d.setColor(Color.black);
        g2d.draw(shape);
        g2d.setColor(Color.white);
        g2d.setTransform(transform);
        g2d.drawString(texts[0], (imageWidth - metrics.stringWidth(texts[0]) / 2) + 1, textHeight + padding + 1);

        stroke = new BasicStroke();
        gv = font.createGlyphVector(frc, texts[1]);
        shape = gv.getOutline();
        shape = stroke.createStrokedShape(shape);
        g2d.translate(imageWidth - metrics.stringWidth(texts[1]) / 2, img.getHeight() - padding);
        g2d.setColor(Color.black);
        g2d.draw(shape);
        g2d.setColor(Color.white);
        g2d.setTransform(transform);
        g2d.drawString(texts[1], (imageWidth - metrics.stringWidth(texts[1]) / 2) + 1, (img.getHeight() - padding) + 1);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(img, imgType, os);
        byte[] byteArray = os.toByteArray();
        return byteArray;
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        //
    }

    @Override
    public String getInfo() {
        return "Create an old impact meme";
    }

}