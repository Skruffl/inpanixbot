package wtf.skruffl.roobot.messages;

import java.util.concurrent.ThreadLocalRandom;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Ping implements MessageHandler {
	
	@Override
	public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) {
		if(ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE) > (Integer.MAX_VALUE - 200)) {
			throw new InvalidMessageException();
		}
		event.getChannel().sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.PING_RESPONSE, bot)).queue();
	}
	
	@Override
	public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
		event.getChannel().sendMessage(MessageUtils.getMessageTextFor(event.getMember(), MessageType.PING_ERROR, bot)).queue();
	}

	@Override
	public String getInfo() {
		return "PONG";
	}

}
