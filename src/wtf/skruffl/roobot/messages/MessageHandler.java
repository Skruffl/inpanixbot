package wtf.skruffl.roobot.messages;

import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface MessageHandler {
	public void handleMessage(MessageReceivedEvent event, final String[] args, Bot bot) throws InvalidMessageException;
	public void invalidSyntax(MessageReceivedEvent event, final String[] args, Bot bot);
	public String getInfo();
}
