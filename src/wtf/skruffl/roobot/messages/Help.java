package wtf.skruffl.roobot.messages;

import java.util.Arrays;
import java.util.List;

import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.messages.admin.Addstarboardthreshold;
import wtf.skruffl.roobot.messages.admin.Ban;
import wtf.skruffl.roobot.messages.admin.Connect;
import wtf.skruffl.roobot.messages.admin.Connections;
import wtf.skruffl.roobot.messages.admin.Createembed;
import wtf.skruffl.roobot.messages.admin.Kick;
import wtf.skruffl.roobot.messages.admin.Mute;
import wtf.skruffl.roobot.messages.admin.Sequence;
import wtf.skruffl.roobot.messages.admin.Setguildsetting;
import wtf.skruffl.roobot.messages.admin.Togglestreamer;
import wtf.skruffl.roobot.messages.admin.Warn;
import wtf.skruffl.roobot.messages.admin.Wipe;
import wtf.skruffl.roobot.messages.admin.Wordctl;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Help implements MessageHandler {
    private static List<MessageHandler> handlers = Arrays.asList(new Addstarboardthreshold(), new Ban(), new Connect(),
            new Connections(), new Createembed(), new Kick(), new Mute(), new Sequence(), new Setguildsetting(),
            new Togglestreamer(), new Warn(), new Wipe(), new Wordctl(), new Count(), new Creategiveaway(), new Gamble(), new Info(),
            new Meme(), new Ping(), new Stats(), new Ticket());

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        EmbedBuilder builder = new EmbedBuilder();
        for(MessageHandler handler : handlers) {
            String msg = handler.getInfo();
            if(handler.getClass().getName().contains("admin")) {
                msg = "[Admin] " + msg;
            }
            builder.addField(handler.getClass().getSimpleName(), msg, true);
        }
        event.getChannel().sendMessage(builder.build()).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        // not thrown
    }

    @Override
    public String getInfo() {
        return "This command";
    }

}