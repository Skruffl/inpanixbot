package wtf.skruffl.roobot.messages;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.constants.Setting;
import wtf.skruffl.roobot.db.ConnectionManager;
import wtf.skruffl.roobot.db.pojo.GuildSettings;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.GeneralUtils;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.PermissionOverride;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Ticket implements MessageHandler {

        @Override
        public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
                ConnectionManager manager = bot.getConnectionManager();
                GuildSettings supportRole = GeneralUtils.firstOrNull(
                                manager.getGuildSetting(Setting.SUPPORT_ROLE, event.getGuild().getIdLong()));
                GuildSettings supportCategory = GeneralUtils.firstOrNull(
                                manager.getGuildSetting(Setting.SUPPORT_CATEGORY, event.getGuild().getIdLong()));
                GuildSettings adminRole = GeneralUtils.firstOrNull(
                                manager.getGuildSetting(Setting.ADMIN_ROLE_ID, event.getGuild().getIdLong()));

                if (adminRole == null || supportRole == null || supportCategory == null
                                || !bot.getModules().contains("ticket")) {
                        throw new InvalidMessageException();
                }

                Category category = event.getGuild().getCategoryById(supportCategory.getValue());
                Role support = event.getGuild().getRoleById(supportRole.getValue());
                Role admin = event.getGuild().getRoleById(adminRole.getValue());

                // Create channel
                TextChannel channel = (TextChannel) category
                                .createTextChannel(event.getAuthor().getName() + event.getMessage().getTimeCreated())
                                .complete();

                // Resend original message
                MessageBuilder builder = new MessageBuilder(event.getMessage());
                builder.setContent(GeneralUtils.shiftJoin(args));
                builder.setEmbed(new EmbedBuilder()
                                .setAuthor(event.getAuthor().getAsTag(), null, event.getAuthor().getAvatarUrl())
                                .addField("Channel", event.getTextChannel().getAsMention(), false).build());
                channel.sendMessage(builder.build()).queue();

                // disable @everyone's permissions
                PermissionOverride everyoneOverride = channel.createPermissionOverride(event.getGuild().getPublicRole())
                                .complete();
                everyoneOverride.getManager().deny(Permission.ALL_PERMISSIONS).queue();

                // Permission overrides for roles
                PermissionOverride suppOverride = channel.createPermissionOverride(support).complete();
                suppOverride.getManager().grant(Permission.ALL_PERMISSIONS).queue();

                PermissionOverride adminOverride = channel.createPermissionOverride(admin).complete();
                adminOverride.getManager().grant(Permission.ALL_PERMISSIONS).queue();

                // Permission overrides for member
                PermissionOverride memberOverride = channel.createPermissionOverride(event.getMember()).complete();
                memberOverride.getManager()
                                .grant(Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MANAGE_CHANNEL)
                                .queue();
        }

        @Override
        public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
                event.getChannel().sendMessage(
                                MessageUtils.getMessageTextFor(event.getMember(), MessageType.TICKET_ERROR, bot));
        }

        @Override
        public String getInfo() {
                return "Creates a support ticket";
        }

}