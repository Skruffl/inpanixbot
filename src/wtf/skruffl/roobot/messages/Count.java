package wtf.skruffl.roobot.messages;

import wtf.skruffl.roobot.constants.MessageType;
import wtf.skruffl.roobot.exceptions.InvalidMessageException;
import wtf.skruffl.roobot.main.Bot;
import wtf.skruffl.roobot.util.MessageUtils;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Count implements MessageHandler {

    @Override
    public void handleMessage(MessageReceivedEvent event, String[] args, Bot bot) throws InvalidMessageException {
        String msg = MessageUtils.getMessageTextFor(event.getMember(), MessageType.COUNT_AMOUNT, bot);
        msg = msg.replace("VALUE", Integer.toString(((TextChannel)event.getChannel()).getMembers().size()));
        event.getChannel().sendMessage(msg).queue();
    }

    @Override
    public void invalidSyntax(MessageReceivedEvent event, String[] args, Bot bot) {
        //not thrown
    }

    @Override
    public String getInfo() {
        return "Shows the amount of members in the current channel";
    }

}