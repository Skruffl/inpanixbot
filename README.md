As of right now, I don't provide precompiled binaries, though building from source is relatively easy, since this is a Maven Project.

1. Getting set up<br>
    1.1 Dependencies<br>
    1.1.1 a jdk installation, anything should work<br>
    1.1.2 maven<br>
    1.1.3 a couchdb install<br>
    1.1.4 git, if you want to check out over CLI<br><br>
    

    1.2 Building from source<br>
    
    `git clone https://gitlab.com/Skruffl/inpanixbot`<br>
    `mvn install -f inpanixbot/pom.xml`<br>
    the install target creates the fat jar and the normal jar in inpanixbot/target<br>
    you'll want the `InPanixBot-0.0.?-SNAPSHOT-jar-with-dependencies.jar`<br><br>
    
    1.3 Using the bot<br>
        1.3.1 First boot!<br>
        Starting the bot is pretty straight forward<br>
        `java -jar inpanixbot/target/InPanixBot-0.0.?-SNAPSHOT-jar-with-dependencies.jar`<br>
        This will exit straight away, since it's not configured. It'll create a default config file in the directory you called the program from<br><br>
        1.3.2 Configuring the bot<br><br>
        In the root directory of the repo, you'll find a configdoc.txt, which lists all config options you can put into the newly created `config.properties` file.<br>
        The bot won't start without a token or a couchdb installation configured, so make sure you set those options.<br>
        1.3.3 Running the bot <br><br>
        After you got everything set up, I highly suggest running the bot through a program like screen<br>
        `screen java -jar inpanixbot/target/InPanixBot-0.0.?-SNAPSHOT-jar-with-dependencies.jar`<br>